unit UControlCajonesEntradaSalida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,System.Math, System.Variants, System.Classes, Vcl.Graphics,StrUtils,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.DBCtrls, Vcl.StdCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.Menus,Dateutils, UControlCajonesProductores,Ubusqueda, System.Character,
  frxClass, frxDBSet, Vcl.ToolWin, Vcl.ImgList, FireDAC.VCLUI.Wait,
  System.ImageList, frxBarcode, Vcl.ExtCtrls, Vcl.Imaging.jpeg;

type
  TControlCajonesEntradaSalida = class(TForm)
    lblfolio: TLabel;
    TxtFolio: TEdit;
    cmbProveedor: TDBLookupComboBox;
    lblProveedor: TLabel;
    CmbTipo: TComboBox;
    LblTipo: TLabel;
    CmbClase: TComboBox;
    LblClase: TLabel;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    LblFecha: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cmbProductor: TDBLookupComboBox;
    LblProductor: TLabel;
    GroupChofer: TGroupBox;
    Label1: TLabel;
    txtPlacas: TEdit;
    Label2: TLabel;
    txtChofer: TEdit;
    DSProveedores: TDataSource;
    QueryProveedores: TFDQuery;
    DSProductores: TDataSource;
    QueryProductores: TFDQuery;
    mnuProductores: TPopupMenu;
    mnuItemAbrir: TMenuItem;
    mnuItemNuevo: TMenuItem;
    txtClaveProductor: TEdit;
    datos: TStringGrid;
    gbTotales: TGroupBox;
    txtTotalCajones: TEdit;
    txtTotalPesoBruto: TEdit;
    txtTotalCosto: TEdit;
    txtTotalTara: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    btnGuardar: TButton;
    cmbAlmacenes: TDBLookupComboBox;
    Label7: TLabel;
    DSAlmacenes: TDataSource;
    QueryAlmacenes: TFDQuery;
    dtpFecha: TDateTimePicker;
    Transaction: TFDTransaction;
    txtTotalPesoNeto: TEdit;
    Label8: TLabel;
    txtPromedioporCajon: TEdit;
    Label9: TLabel;
    Reporte: TfrxReport;
    DSReporte: TfrxDBDataset;
    QueryImprimir: TFDQuery;
    ImageList1: TImageList;
    btnRecibo: TButton;
    btnEtiquetas: TButton;
    Etiquetas: TfrxReport;
    DSEtiquetas: TfrxDBDataset;
    QueryEtiquetas: TFDQuery;
    QueryProductos: TFDQuery;
    grid_datos: TDBGrid;
    DSDetalles: TDataSource;
    QueryDetalles: TFDQuery;
    frxBarCodeObject1: TfrxBarCodeObject;
    qryCajones: TFDQuery;
    btnPoliza: TButton;
    btnRecalcular: TButton;
    cbConta: TCheckBox;
    Panel1: TPanel;
    cbDisponible: TCheckBox;
    btnDevolver: TButton;
    qryEliminar: TFDQuery;
    Image1: TImage;
    Panel2: TPanel;
    Label10: TLabel;
    txtCaracteristica: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure mnuProductoresPopup(Sender: TObject);
    procedure mnuItemAbrirClick(Sender: TObject);
    procedure mnuItemNuevoClick(Sender: TObject);
    procedure actualizar_productores();
    procedure cmbProveedorClick(Sender: TObject);
    procedure cmbProductorClick(Sender: TObject);
    procedure txtClaveProductorExit(Sender: TObject);
    procedure formato_grid();
    procedure datosKeyPress(Sender: TObject; var Key: Char);
    procedure datosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CreaRenglon();
    procedure datosSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure calculaRenglon(row:integer);
    procedure EntradaVacio();
    procedure EntradaManzana();
    procedure SalidaVacio();
    procedure btnGuardarClick(Sender: TObject);
    procedure Totales();
    procedure CmbTipoChange(Sender: TObject);
    procedure LimpiarGrid;
    procedure CmbClaseChange(Sender: TObject);
    procedure EnviarAFormato();
    procedure borrarRenglones();
    procedure DeleteRow(Grid: TStringGrid; ARow: Integer);
    procedure btnReciboClick(Sender: TObject);
    procedure btnEtiquetasClick(Sender: TObject);
    procedure ImprimirEtiquetas();
    procedure btnPolizaClick(Sender: TObject);
    procedure btnRecalcularClick(Sender: TObject);
    procedure datosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure btnDevolverClick(Sender: TObject);
    procedure EliminarRenglon(Grid: TStringGrid; ARow: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    docto_id,docto_cm__id,cancelado:integer;
    liga_in:string;
       eliminar:TStringList;
  end;

var
  ControlCajonesEntradaSalida: TControlCajonesEntradaSalida;

implementation

{$R *.dfm}
{$region 'Funciones'}
function GetSerie(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsLetter(folio[i]))
    then Result := Result+folio[i]
    else exit;
  end;
end;

function GetNumFolio(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsNumber(folio[i]))
    then Result := Result+folio[i];
  end;
end;

procedure TControlCajonesEntradaSalida.ImprimirEtiquetas();
var
  Memo:TfrxMemoView;
  folio_str,page_str,folio_viaje:string;
  folio_int,page,consec,viaje,i:integer;
  ds: TfrxDataSet;
  md: TfrxmasterData;
  query_et:string;
begin
  consec := 1;
  query_et := '';
  if (CmbTipo.Text = 'Entrada') and (CmbClase.text = 'Con Manzana') then
  begin
    folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_cm where docto_cm_id=:did',[docto_id]);
  end
  else
  begin
    folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_in where docto_in_id=:did',[docto_id]);
  end;

  viaje := strtoint(GetNumFolio(folio_viaje));
  QueryProductos.sql.text := ' select  a.nombre,'+
                              ' (select clave_articulo from get_clave_art(a.articulo_id))as clave,'+
                              ' sum(dcd.sic_num_cajones) as cajones'+
                        ' from doctos_cm_det dcd join'+
                        ' articulos a on a.articulo_id = dcd.articulo_id'+
                        ' where dcd.docto_cm_id = '+IntToStr(docto_id)+
                        ' group by a.nombre,a.articulo_id';
  QueryProductos.Open;
  QueryProductos.First;
  while not QueryProductos.Eof do
  begin
    if query_et<>''
    then query_et := query_et +' union ';

    query_et := query_et + ' select a.*,'''+QueryProductos.FieldByName('nombre').AsString+''' as articulo,'+
                            ''''+QueryProductos.FieldByname('clave').asstring+'''as clave,'+
                            ' '+IntToStr(viaje)+' as viaje from'+
                            ' (with recursive n as ('+
                                  ' select '+inttostr(consec)+' as n'+
                                  ' from rdb$database'+
                                  ' union all'+
                                  ' select n.n + 1'+
                                  ' from n'+
                                  ' where n < '+IntToStr(consec+QueryProductos.Fieldbyname('cajones').AsInteger-1)+''+
                                 ' )'+
                            ' select n.n'+
                            ' from n) a';
    consec := consec+QueryProductos.Fieldbyname('cajones').AsInteger;
    QueryProductos.Next;

  end;
  QueryEtiquetas.sql.Text := query_et;

  QueryEtiquetas.open;

  Etiquetas.PrepareReport;
  Memo := Etiquetas.FindObject('txtFecha') as TfrxMemoView;
  Memo.Text := formatdatetime('dddd, dd - mmmm - yyyy',dtpFecha.Date);
  Memo := Etiquetas.FindObject('txtAlmacen') as TfrxMemoView;
  Memo.Text := cmbAlmacenes.Text;
  Memo := Etiquetas.FindObject('txtProductor') as TfrxMemoView;
  Memo.Text := cmbProductor.Text;
  Memo := Etiquetas.FindObject('txtProductor2') as TfrxMemoView;
  Memo.Text := cmbProductor.Text;
  Memo := Etiquetas.FindObject('txtPromedio') as TfrxMemoView;
  Memo.Text := FormatFloat('######.00',(StrToFloat(txtPromedioporCajon.Text)))+' KG';
  Memo := Etiquetas.FindObject('txtPromedio2') as TfrxMemoView;
  Memo.Text := FormatFloat('######.00',(StrToFloat(txtPromedioporCajon.Text)))+' KG';
  Memo := Etiquetas.FindObject('txtProductor3') as TfrxMemoView;
  Memo.Text := cmbProductor.Text;
  Memo := Etiquetas.FindObject('txtAlmacen2') as TfrxMemoView;
  Memo.Text := cmbAlmacenes.Text;

  Etiquetas.ShowReport(true);

end;

procedure TControlCajonesEntradaSalida.borrarRenglones();
var
  i: Integer;
begin
  //
  for i := 1 to datos.RowCount-1 do
  begin
    DeleteRow(datos,i);
  end;
  CreaRenglon;
  datos.Col := 0;
  datos.Row := 1;
  txtTotalCajones.Text := '0';
  txtTotalPesoBruto.Text := '0';
  txtTotalCosto.Text := '0';
  txtTotalTara.Text := '0';
  txtTotalPesoNeto.Text := '0';
  txtPromedioporCajon.Text := '0';
end;

procedure TControlCajonesEntradaSalida.EnviarAFormato;
var
  Memo : TfrxMemoView;
  entrego,recibio : string;
begin
  //
  Memo := Reporte.FindObject('txtTitulo') as TfrxMemoView;
  Memo.Text := cmbTipo.text+' de Cajones ('+CmbClase.Text+')';


  if CmbTipo.Text = 'Entrada' then
  begin
    entrego := txtChofer.Text+' (Placas '+txtPlacas.text+')';
    recibio := '';
    if cmbclase.Text = 'Con Manzana' then
    begin
      QueryImprimir.sql.Text := ' select  p.nombre as proveedor,'+
                                    ' sp.nombre as productor,'+
                                    ' dc.folio,'+
                                    ' dc.fecha,'+
                                    ' dcd.sic_num_cajones,'+
                                    ' a.nombre as nombre_articulo,'+
                                    ' dcd.precio_unitario as costo_unitario,'+
                                    ' dcd.precio_total_neto as total,'+
                                    ' dcd.sic_peso_bruto,'+
                                    ' dcd.sic_tara,'+
                                    ' dcd.unidades as peso_neto,'+
                                    ' dcd.sic_propietario_cajon,'+
                                    ' ''.'' as recibio,'+
                                    ' '','' as entrego'+
                            ' from doctos_cm dc join'+
                            ' proveedores p on p.proveedor_id = dc.proveedor_id join'+
                            ' sic_productores sp on sp.id=dc.sic_productor join'+
                            ' doctos_cm_det dcd on dcd.docto_cm_id = dc.docto_cm_id join'+
                            ' articulos a on a.articulo_id = dcd.articulo_id where dc.docto_cm_id = '+IntToStr(docto_id);
    end
    else
    begin
      recibio := txtChofer.Text+' (Placas '+txtPlacas.text+')';
      entrego := '';
      QueryImprimir.sql.Text := ' select  '''' as proveedor,'+
                                    ' sp.nombre as productor,'+
                                    ' dc.folio,'+
                                    ' dc.fecha,'+
                                    ' dcd.unidades as sic_num_cajones,'+
                                    ' a.nombre as nombre_articulo,'+
                                    ' dcd.costo_unitario,'+
                                    ' dcd.costo_total as total,'+
                                    ' dcd.sic_peso_bruto,'+
                                    ' dcd.sic_tara,'+
                                    ' 0 as peso_neto,'+
                                    ' dcd.sic_propietario_cajon,'+
                                    ' ''.'' as recibio,'+
                                    ' '','' as entrego'+
                            ' from doctos_in dc join'+
                            ' sic_productores sp on sp.id=dc.sic_productor join'+
                            ' doctos_in_det dcd on dcd.docto_in_id = dc.docto_in_id join'+
                            ' articulos a on a.articulo_id = dcd.articulo_id where dc.docto_in_id = '+IntToStr(docto_id);
    end;
  end
  else
  begin
    QueryImprimir.sql.Text := ' select  '''' as proveedor,'+
                                    ' sp.nombre as productor,'+
                                    ' dc.folio,'+
                                    ' dc.fecha,'+
                                    ' dcd.unidades as sic_num_cajones,'+
                                    ' a.nombre as nombre_articulo,'+
                                    ' dcd.costo_unitario,'+
                                    ' dcd.costo_total as total,'+
                                    ' dcd.sic_peso_bruto,'+
                                    ' dcd.sic_tara,'+
                                    ' 0 as peso_neto,'+
                                    ' dcd.sic_propietario_cajon,'+
                                    ' ''.'' as recibio,'+
                                    ' '','' as entrego'+
                            ' from doctos_in dc join'+
                            ' sic_productores sp on sp.id=dc.sic_productor join'+
                            ' doctos_in_det dcd on dcd.docto_in_id = dc.docto_in_id join'+
                            ' articulos a on a.articulo_id = dcd.articulo_id where dc.docto_in_id = '+IntToStr(docto_id);
  end;
  QueryImprimir.Open;
  Memo := Reporte.FindObject('txtEntrego') as TfrxMemoView;
  Memo.Text := entrego;
  Memo := Reporte.FindObject('txtRecibio') as TfrxMemoView;
  Memo.Text := recibio;
  Reporte.PrepareReport;
  Reporte.ShowReport(true);


end;

procedure TControlCajonesEntradaSalida.LimpiarGrid();
var
  i: Integer;
begin
  //
  for i := 1 to datos.RowCount-1 do
  begin
    datos.Rows[i].Clear;
  end;
end;

procedure TControlCajonesEntradaSalida.CreaRenglon();
var
  i:integer;
begin
  //
  datos.RowCount := datos.RowCount + 1;
  for i := 0 to 7 do
  begin
    datos.cells[i,datos.RowCount-1] := '';
  end;

  if (datos.RowCount>2) then
  begin
    datos.cells[0,datos.RowCount-1] := datos.cells[0,datos.RowCount-2];
    datos.cells[1,datos.RowCount-1] := datos.cells[1,datos.RowCount-2];
    datos.cells[5,datos.RowCount-1] := datos.cells[5,datos.RowCount-2];
    datos.cells[7,datos.RowCount-1] := datos.cells[7,datos.RowCount-2];
    datos.cells[8,datos.RowCount-1] := datos.cells[8,datos.RowCount-2];
  end;

  datos.cells[2,datos.RowCount-1] := '0';
  datos.cells[3,datos.RowCount-1] := '0';
  datos.cells[4,datos.RowCount-1] := '0';
  datos.cells[6,datos.RowCount-1] := '0';

end;

procedure TControlCajonesEntradaSalida.DeleteRow(Grid: TStringGrid; ARow: Integer);
var
  i: Integer;
begin
  for i := ARow to Grid.RowCount - 2 do
    Grid.Rows[i].Assign(Grid.Rows[i + 1]);
  Grid.RowCount := Grid.RowCount - 1;
end;

procedure TControlCajonesEntradaSalida.formato_grid();
begin
if cancelado=0 then
 begin
  datos.Row := 1;
  datos.Col := 0;
  //********* TITULO DE COLUMNAS DE GRID************
  datos.Cells[0,0] := 'Cajones';
  datos.Cells[1,0] := 'Articulo';
  datos.Cells[2,0] := 'Costo Unitario';
  datos.Cells[3,0] := 'Total';
  datos.Cells[4,0] := 'Peso Bruto';
  datos.Cells[5,0] := 'Tara';
  datos.Cells[6,0] := 'Peso Neto';
  datos.Cells[7,0] := 'Propietario';
   datos.Cells[9,0] := 'Disponible';
   datos.Cells[10,0] := 'Docto cm id';

  //********* TAMA�O DE COLUMNAS DE GRID************
  datos.ColWidths[0] := 45;
  datos.ColWidths[1] := 350;
  datos.ColWidths[2] := 60;
  datos.ColWidths[3] := 80;
  datos.ColWidths[4] := 80;
  datos.ColWidths[5] := 60;
  datos.ColWidths[6] := 60;
  datos.ColWidths[7] := 140;
  datos.ColWidths[8] := 0;
  datos.ColWidths[9] := 80;
    datos.ColWidths[10] := 0;

  //********* PRIMER RENGLON************
  datos.Cells[2,1] := '0';
  datos.Cells[3,1] := '0';
  datos.Cells[4,1] := '0';
  datos.Cells[5,1] := '0';
  datos.Cells[6,1] := '0';
 end;
end;

procedure TControlCajonesEntradaSalida.Totales();
var
  i,num_cajones: Integer;
  costo_total,peso_bruto,tara,peso_neto : double;
begin
  //
  try
    costo_total := 0;peso_bruto := 0;tara := 0;peso_neto:=0;num_cajones:= 0;
    for i := 1 to datos.RowCount-1 do
    begin
      if (datos.cells[4,i] <> '0') AND (datos.cells[4,i] <> '') then
      begin
        num_cajones := num_cajones + StrToInt(datos.cells[0,i]);
        costo_total := costo_total + StrToFloat(datos.cells[3,i]);
        peso_bruto := peso_bruto + StrToFloat(datos.cells[4,i]);
        tara := tara + StrToFloat(datos.Cells[5,i]);
        peso_neto := peso_neto + StrToFloat(datos.cells[6,i]);
      end;
    end;
    txtTotalCajones.Text := inttostr(num_cajones);
    txtTotalCosto.Text := FloatToStr(costo_total);
    txtTotalTara.Text := FloatToStr(tara);
    txtTotalPesoBruto.Text := floattostr(peso_bruto);
    txtTotalPesoNeto.Text := FloatToStr(peso_neto);
    txtPromedioporCajon.Text := FormatFloat('######.00',peso_neto/num_cajones);
  except
  end;
end;

procedure TControlCajonesEntradaSalida.calcularenglon(row:integer);
begin
   //
  if datos.col <> 0 then
   begin
     try
       datos.Cells[6,row] := floattostr(strtofloat(datos.cells[4,row])-strtofloat(datos.cells[5,row]));
       datos.cells[3,row] := floattostr(StrToFloat(datos.cells[6,row])*strtofloat(datos.cells[2,row]));
     except
     end;
  end;
end;

procedure TControlCajonesEntradaSalida.actualizar_productores();
begin
  QueryProductores.SQL.Text := 'select * from sic_productores where proveedor_id = '+inttostr(cmbProveedor.KeyValue)+' order by nombre';
  QueryProductores.Open;
  cmbProductor.KeyValue := Conexion.ExecSQLScalar('select first 1 id from sic_productores where proveedor_id = '+inttostr(cmbProveedor.KeyValue)+' order by nombre');
  txtClaveProductor.Text := Conexion.ExecSQLScalar('select clave from sic_productores where id=:pid',[cmbProductor.KeyValue]);
end;


{$endregion}

{$REGION 'Funciones Entrada Salida'}
procedure TControlCajonesEntradaSalida.EntradaManzana();
var
  query_str,query_str2,serie,folio_str,clave_proveedor,estatus,clave_articulo,notas,rol_ve,
  propietario_cajon,rol_valor,fecha,folio_viaje:string;
  documento_id,proveedor_id,consecutivo,folio_id,
  dir_cli_id,almacen_id, moneda_id,cond_pago_id, articulo_id,detalle_id,
  num_cajones, productor_id, articulo_discreto_id : integer;
  precio_unitario, precio_total_neto, unidades,pctje_dscto,peso_bruto,tara: double;
  con,j,i,num_discretos: Integer;
  insertar:bool;
  Existe:integer;
begin
  //
  Conexion.StartTransaction;
  try
  serie := Conexion.ExecSQLScalar('select serie from folios_compras where tipo_docto = ''R''');
  except
  end;
    consecutivo := Conexion.ExecSQLScalar('select consecutivo from folios_compras where tipo_docto = ''R''');
    folio_id := Conexion.ExecSQLScalar('select folio_compras_id from folios_compras where tipo_docto = ''R''');
    //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
    if serie <> '@'
    then folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)])
    else folio_str := Format('%.*d',[9,(consecutivo)]);
    Conexion.ExecSQL('update folios_compras set consecutivo=consecutivo+1 where folio_compras_id=:folio_id',[folio_id]);
    //AGREGAR FOLIO NUEVO A LA DESCRIPCION DE LA RECEPCION ANTERIOR
    if Conexion.ExecSQLScalar('Select count(docto_cm_id) from doctos_cm_det where docto_cm_id=:id',[docto_cm__id])=0 then
        begin
        Conexion.ExecSQL('update doctos_cm set descripcion=:desc where docto_cm_id=:id',['Devolucion a la maquila: '+folio_str,docto_cm__id]);
        end;
    //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
    proveedor_id := cmbProveedor.KeyValue;
    clave_proveedor := Conexion.ExecSQLScalar('select coalesce(clave_prov,'''') from get_clave_prov(:id)',[proveedor_id]);
    almacen_id := cmbAlmacenes.KeyValue;
    moneda_id := 1;
    cond_pago_id := Conexion.ExecSQLScalar('select first 1 cond_pago_id from proveedores where proveedor_id = :pid',[proveedor_id]);
    fecha := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);
    productor_id :=  cmbProductor.KeyValue;

    //*******************VERIFICA SI EXISTE EL FOLIO EN DOCTOS_CM**************************//




    //****************************************DA DE ALTA EL ENCABEZADO***********************************

        //MSP 2019
      {query_str := 'insert into doctos_cm (docto_cm_id,tipo_docto,folio,fecha,clave_prov,'+
                   ' proveedor_id,almacen_id,moneda_id,tipo_cambio,'+
                   ' sistema_origen,tipo_dscto,estatus,cond_pago_id,sic_productor,sic_chofer,sic_placas) values (-1,''R'','''+folio_str+''','+
                   ' '''+fecha+''','''+clave_proveedor+''','+inttostr(proveedor_id)+','+
                   ' '+inttostr(almacen_id)+','+inttostr(moneda_id)+',1,''CM'',''P'',''P'','+inttostr(cond_pago_id)+','
                   + IntToStr(productor_id)+','''+txtChofer.Text+''','''+txtPlacas.Text+''') returning docto_cm_id';}

 //MSP 2020
       query_str := 'insert into doctos_cm (docto_cm_id,tipo_docto,sucursal_id,folio,fecha,clave_prov,'+
                   ' proveedor_id,almacen_id,moneda_id,tipo_cambio,'+
                   ' sistema_origen,tipo_dscto,estatus,cond_pago_id,sic_productor,sic_chofer,sic_placas,sic_caracteristica) values (-1,''R'','+inttostr(Conexion.ExecSQLScalar('select sucursal_id from sucursales where upper(nombre)=''MATRIZ'''))+','''+folio_str+''','+
                   ' '''+fecha+''','''+clave_proveedor+''','+inttostr(proveedor_id)+','+
                   ' '+inttostr(almacen_id)+','+inttostr(moneda_id)+',1,''CM'',''P'',''P'','+inttostr(cond_pago_id)+','
                   + IntToStr(productor_id)+','''+txtChofer.Text+''','''+txtPlacas.Text+''','''+txtCaracteristica.Text+''') returning docto_cm_id';

      documento_id := Conexion.ExecSQLScalar(query_str);
      docto_id := documento_id;


     //*******************************DETALLADO DEL DOCUMENTO*********************************************
     for i := 1 to datos.RowCount-1 do
     begin
        insertar := true;
        try
          articulo_id := strtoint(datos.cells[8,i]);
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          unidades := strtofloat(datos.cells[6,i]);
          precio_unitario := StrToFloat(datos.cells[2,i]);
          precio_total_neto := StrToFloat(datos.cells[3,i]);
          num_cajones := StrToInt(datos.cells[0,i]);
          peso_bruto := StrToFloat(datos.cells[4,i]);
          tara := StrToFloat(datos.cells[5,i]);
          propietario_cajon := datos.Cells[7,i];
        except
          insertar := false;
        end;

        if insertar then
        begin
          query_str := 'insert into doctos_cm_det (docto_cm_det_id, docto_cm_id,'+
                       ' clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,notas,pctje_dscto,sic_num_cajones,'+
                       ' sic_peso_bruto,sic_tara,sic_propietario_cajon) values(-1,'+
                       ' '+inttostr(documento_id)+','''+clave_articulo+''','+inttostr(articulo_id)+','+FloatToStr(unidades)+
                       ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+','''',0,'+
                       ' '+IntToStr(num_cajones)+','+FloatToStr(peso_bruto)+','+FloatToStr(tara)+','''+propietario_cajon+''') returning docto_cm_det_id ';
          detalle_id := Conexion.ExecSQLScalar(query_str);

          if Conexion.ExecSQLScalar('select count(*) from articulos_discretos where articulo_id='+inttostr(articulo_id)+' and clave='''+IntToStr(consecutivo)+''' ')>0
          then articulo_discreto_id := Conexion.ExecSQLScalar('select art_discreto_id from articulos_discretos where articulo_id='+inttostr(articulo_id)+' and clave='''+IntToStr(consecutivo)+'''')
          else articulo_discreto_id := Conexion.ExecSQLScalar('insert into articulos_discretos values(-1,'''+IntToStr(consecutivo)+''','+inttostr(articulo_id)+',''L'',current_date) returning art_discreto_id');

          Conexion.ExecSQL('insert into desglose_en_discretos_cm values(-1,'+inttostr(detalle_id)+','+IntToStr(articulo_discreto_id)+','+FloatToStr(unidades)+')');


        end
        else
        begin
          Conexion.Rollback;
        end;
     end;
     Conexion.ExecSQL('execute procedure calc_totales_docto_cm(:did,''N'')',[documento_id]);
     Conexion.ExecSQL('execute procedure aplica_docto_cm(:id)',[documento_id]);
     Conexion.Commit;
     ShowMessage('Recepcion de mercancia con folio: '+folio_str);


                  if (CmbTipo.Text = 'Entrada') and (CmbClase.text = 'Con Manzana') then
  begin
    folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_cm where docto_cm_id=:did',[docto_id]);
  end
  else
  begin
    folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_in where docto_in_id=:did',[docto_id]);
  end;
  i:=1;
  j:=1;
  con:=1;
              //INSERTAR DATOS A TABLA SIC_MAQUILA_DET_ENTRADA
          for i := 1 to datos.RowCount-1 do
             begin
             for j := 1 to strtoint(datos.Cells[0,i]) do
               begin
               qryCajones.sql.Text:='INSERT INTO sic_maquila_det_entrada (maquila_det_id, recepcion_id, cod_barras, peso,articulo_id,costo)'+
                ' VALUES(-1, '''+inttostr(docto_id)+''', '''+AnsiRightStr(folio_viaje,6)+Format('%.*d',[3, con])+''', '''+
                FormatFloat('######.00',(StrToFloat(txtPromedioporCajon.Text)))+''', '''+datos.cells[8,i]+''', '''+datos.cells[2,i]+''')';
                qryCajones.ExecSQL;
                con:=con+1;
               end;
             end;
          ////////////////////////
end;

procedure TControlCajonesEntradaSalida.EntradaVacio();
var
  query_str,serie,folio_str,clave_proveedor,clave_articulo,fecha,folio_nuevo:string;
  documento_id,productor_id,consecutivo,concepto_id,
  almacen_id, moneda_id,cond_pago_id, articulo_id, articulo_discreto_id,almacen_cajones : integer;
  precio_unitario, precio_total_neto, unidades: double;
  i: Integer;
  insertar:bool;
begin
  //
  //Conexion.StartTransaction;
  //try
    if Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''ENTRADA CAJONES''')<>'' then
    begin
    serie := GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''ENTRADA CAJONES'''));
    consecutivo := strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''ENTRADA CAJONES''')));
    concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where upper(nombre) = ''ENTRADA CAJONES''');
    end
    else
    begin
      ShowMessage('Falta concepto ''Entrada Cajones'' en Inventarios.');
    end;
    //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
    folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
    folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
    Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

    //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
    almacen_id := cmbAlmacenes.KeyValue;
    moneda_id := 1;
    fecha := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);
    productor_id :=  cmbProductor.KeyValue;
    //****************************************DA DE ALTA EL ENCABEZADO***********************************

  //MICROSIP 2019
      {query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,'+
                   ' sistema_origen,sic_productor,sic_chofer,sic_placas,sic_proveedor,sic_integ) values (-1,'+inttostr(almacen_id)+','+inttostr(concepto_id)+','+
                   ' '''+folio_str+''',''E'','''+fecha+''','+
                   ' ''IN'','+IntToStr(productor_id)+','''+txtChofer.Text+''','''+txtPlacas.Text+''','+
                   ' '+inttostr(cmbProveedor.KeyValue)+','''+liga_in+''') returning docto_in_id';  }

       //MICROSIP 2020
       almacen_cajones:=Conexion.ExecSQLScalar('select almacen_id from almacenes where upper(nombre)=''ALMACEN CAJONES''');
       query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,sucursal_id,folio,naturaleza_concepto,fecha,'+
                   ' sistema_origen,sic_productor,sic_chofer,sic_placas,sic_proveedor,sic_integ) values (-1,'+inttostr(almacen_cajones)+','+inttostr(concepto_id)+','+
                   ' '+inttostr(Conexion.ExecSQLScalar('select sucursal_id from sucursales where upper(nombre)=''MATRIZ'''))+','''+folio_str+''',''E'','''+fecha+''','+
                   ' ''IN'','+IntToStr(productor_id)+','''+txtChofer.Text+''','''+txtPlacas.Text+''','+
                   ' '+inttostr(cmbProveedor.KeyValue)+','''+liga_in+''') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);
      docto_id := documento_id;

     //*******************************DETALLADO DEL DOCUMENTO*********************************************
          if Conexion.ExecSQLScalar('select articulo_id from articulos where upper(nombre) =''CAJON''')<>0 then
          begin
          articulo_id := Conexion.ExecSQLScalar('select articulo_id from articulos where upper(nombre) =''CAJON''');
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          unidades := StrToInt(txtTotalCajones.Text);
          precio_unitario := 0;
          precio_total_neto := 0;

           query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,'+
                 ' clave_articulo,articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo,sic_num_cajones) values(-1,'+
                 ' '+inttostr(documento_id)+','+IntToStr(almacen_id)+','+IntToStr(concepto_id)+','''+clave_articulo+''','+
                 ' '+inttostr(articulo_id)+',''E'','+
                 ' '+FloatToStr(unidades)+
                 ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+',''C'','+FloatToStr(unidades)+') ';

           Conexion.ExecSQL(query_str);
           Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);
           Conexion.Commit;
          end
          else
          begin
          ShowMessage('Falta el articulo ''Cajon'' No se creo la entrada de cajones vacio.');
          Conexion.Rollback;
          end;
  {except
  Conexion.Rollback;
  end;  }

end;

procedure TControlCajonesEntradaSalida.SalidaVacio();
var
  query_str,serie,folio_str,clave_proveedor,clave_articulo,fecha,folio_nuevo:string;
  documento_id,productor_id,consecutivo,concepto_id,
  almacen_id, moneda_id,cond_pago_id, articulo_id, articulo_discreto_id : integer;
  precio_unitario, precio_total_neto, unidades: double;
begin
  //
  Conexion.StartTransaction;
  try
    serie := GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where nombre = ''SALIDA CAJONES'''));
    consecutivo := strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where nombre = ''SALIDA CAJONES''')));
    concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where nombre = ''SALIDA CAJONES''');
    //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
    folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
    folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
    Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

    //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
    almacen_id := cmbAlmacenes.KeyValue;
    moneda_id := 1;
    fecha := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);
    productor_id :=  cmbProductor.KeyValue;
    //****************************************DA DE ALTA EL ENCABEZADO***********************************

      query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,'+
                   ' sistema_origen,sic_productor,sic_chofer,sic_placas,sic_caracteristica) values (-1,'+inttostr(almacen_id)+','+inttostr(concepto_id)+','+
                   ' '''+folio_str+''',''S'','''+fecha+''','+
                   ' ''IN'','+IntToStr(productor_id)+','''+txtChofer.Text+''','''+txtPlacas.Text+''','''+txtCaracteristica.Text+''') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);
      docto_id := documento_id;





     //*******************************DETALLADO DEL DOCUMENTO*********************************************

          articulo_id := Conexion.ExecSQLScalar('select articulo_id from articulos where nombre =''CAJON''');
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          //validar los campos vacios
          unidades := StrToInt(txtTotalCajones.Text);
          precio_unitario := 0;
          precio_total_neto := 0;

     query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,'+
                 ' clave_articulo,articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo,sic_num_cajones) values(-1,'+
                 ' '+inttostr(documento_id)+','+IntToStr(almacen_id)+','+IntToStr(concepto_id)+','''+clave_articulo+''','+
                 ' '+inttostr(articulo_id)+',''S'','+
                 ' '+FloatToStr(unidades)+
                 ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+',''C'','+FloatToStr(unidades)+') ';

     Conexion.ExecSQL(query_str);
     Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);



     Conexion.Commit;

  except
    Conexion.Rollback;
  end;
end;

{$ENDREGION}

{$REGION 'Combos'}
procedure TControlCajonesEntradaSalida.cmbProductorClick(Sender: TObject);
begin
   txtClaveProductor.Text := Conexion.ExecSQLScalar('select clave from sic_productores where id=:pid',[cmbProductor.KeyValue]);
end;

procedure TControlCajonesEntradaSalida.cmbProveedorClick(Sender: TObject);
begin
  actualizar_productores;
end;
{$ENDREGION}

{$REGION 'Eventos del grid'}

procedure TControlCajonesEntradaSalida.datosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
  const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
  var
   DrawState,viaje,i: Integer;
 DrawRect: TRect;
 S,verificar,folio_viaje,cod_barras: string;
  SavedAlign: word;
  existe:string;
begin
     if (ACol = 9) and (ARow>0) then
   begin
      S := datos.Cells[ACol, ARow]; // cell contents
    SavedAlign := SetTextAlign(datos.Canvas.Handle, TA_CENTER);
    datos.Canvas.TextRect(Rect,
      Rect.Left + (Rect.Right - Rect.Left) div 2, Rect.Top + 2, S);
    SetTextAlign(datos.Canvas.Handle, SavedAlign);
            if (gdSelected in State) {and (qrycargosCobrador.RecordCount > 0)} then
    begin
      with cbDisponible do
      begin
       // Left := Rect.Left + datos.Left + 2;
       // Top := Rect.Top + datos.Top + 2;
       // Height := Rect.Height;
       // Width := Rect.Width;
       // Color := datos.Canvas.Brush.Color;
       // Visible := true;
         verificar:=datos.Cells[9,Arow];
        if verificar='S' then
            begin
            Checked := false;
            end
            else
            begin
           // datos.Cells[9,ARow]:='S';
            Checked:=true;
            end;

      end;
    end
    else
    begin

      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[datos.Cells[Acol,Arow] = 'S'];
     datos.Canvas.FillRect(Rect);
     DrawFrameControl(datos.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
     end;
end;

procedure TControlCajonesEntradaSalida.datosKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
var
  FormBusqueda:TBusquedaArticulos;
begin
  if (Shift = [ssCtrl]) and (Key = VK_DELETE) and (datos.RowCount > 1 ) then
  begin
    DeleteRow(datos,datos.Row);
  end;

  if ((Key = VK_F4) and (datos.Col = 1)) then
  begin
    FormBusqueda := TBusquedaArticulos.Create(nil);
    FormBusqueda.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormBusqueda.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormBusqueda.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
    FormBusqueda.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormBusqueda.txtBusqueda.Text := datos.cells[datos.Col,datos.Row];
    if FormBusqueda.ShowModal = mrOk then
    begin
      datos.Cells[datos.col,datos.row] := FormBusqueda.articulo_nombre;
      datos.Cells[8,datos.row] := IntToStr(FormBusqueda.articulo_id);
      datos.Col := datos.col+1;
    end;
  end;  
end;

procedure TControlCajonesEntradaSalida.datosKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) then
  begin
  calculaRenglon(datos.Row);
    if (datos.col = 7) then
    begin

      CreaRenglon;
      datos.Row := datos.RowCount-1;
      datos.Col := 0;
    end
    else
    begin
      datos.col := datos.Col+1;
      Totales;
    end;


  end;

end;
procedure TControlCajonesEntradaSalida.datosSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
  var
  folio_viaje,cod_barras,existe: string;
  viaje,i:integer;
  verificar,bandera:integer;

begin
  if ((ACol=3) or (ACol = 6) or (ACol=9))  then
  begin
    CanSelect := False;
    if Acol=9 then
       begin
      //   datos.col := Acol;
       end
       else
    datos.col := Acol+1;
    exit;
  end;
      try
       if (ACol=2) then
       begin
       bandera:=0;
         if (CmbTipo.Text = 'Entrada') and (CmbClase.text = 'Con Manzana') then
          begin
            folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_cm where docto_cm_id=:did',[docto_id]);
          end
          else
          begin
            folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_in where docto_in_id=:did',[docto_id]);
         end;
       if folio_viaje<>'' then
       begin
       viaje := strtoint(GetNumFolio(folio_viaje));
       for i := 1 to strtoint(datos.Cells[0,Arow]) do
         begin
           cod_barras:=AnsiRightStr(folio_viaje,6)+Format('%.*d',[3, i]);
             existe:=Conexion.ExecSQLScalar('select cod_barras from claves_cajones where cod_barras=:cod and articulo_id=:aid',[cod_barras,datos.Cells[8,Arow]]);
            if existe<>'' then
            begin
            bandera:=bandera+1;

            end;
          end;
            if bandera>0 then
             begin
             datos.Cells[9,Arow]:='N';
             end
             else
            datos.Cells[9,Arow]:='S';

         end;
      end;

  except

  end;
end;
  


{$ENDREGION}

{$REGION 'Eventos del Formulario'}

procedure TControlCajonesEntradaSalida.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Conexion.Rollback;
  Conexion.Close;
  //Application.Terminate;
end;

procedure TControlCajonesEntradaSalida.FormShow(Sender: TObject);
var
viaje,i,j,bandera:integer;
folio_viaje,cod_barras,existe:string;
begin
Eliminar := TStringList.Create;
  QueryProveedores.Open;
  QueryAlmacenes.Open;
  QueryProductores.Open;
   SetExceptionMask(exAllArithmeticExceptions);

  if docto_id = 0 then
  begin
    cmbProveedor.KeyValue := Conexion.ExecSQLScalar('select first 1 proveedor_id from proveedores order by nombre');
    cmbAlmacenes.KeyValue := Conexion.ExecSQLScalar('select almacen_id from almacenes where es_ppal=''S''');
    actualizar_productores;
    formato_grid;
    datos.ColWidths[9] := 0;
    dtpFecha.Date := Date;
  end;


  try
    Conexion.ExecSQL('grant all on TABLE sic_productores to USUARIO_MICROSIP;');
  except
  end;

  cbConta.Checked:=(Conexion.ExecSQLScalar('select count(docto_co_id) from doctos_co_det where refer=:folio',[TxtFolio.Text])<>0);

   //Llenar campo de disponible
   folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_cm where docto_cm_id=:did',[docto_id]);
   if folio_viaje<>'' then
  begin
       viaje := strtoint(GetNumFolio(folio_viaje));
   for j := 1 to datos.RowCount-1 do
       begin
       bandera:=0;
       if (datos.Cells[0,j]<>'') then
        begin
       for i := 1 to strtoint(datos.Cells[0,j]) do
         begin
           cod_barras:=AnsiRightStr(folio_viaje,6)+Format('%.*d',[3, i]);
             existe:=Conexion.ExecSQLScalar('select cod_barras from claves_cajones where cod_barras=:cod and articulo_id=:aid',[cod_barras,datos.Cells[8,j]]);
            if existe<>'' then
            begin
            bandera:=bandera+1;

            end;
          end;
            if bandera>0 then
             begin
             datos.Cells[9,j]:='N';
             end
             else
            datos.Cells[9,j]:='S';
        end;
       end;

  end;


end;
{$ENDREGION}

{$REGION 'Menu Contextual'}

procedure TControlCajonesEntradaSalida.mnuItemAbrirClick(Sender: TObject);
var
  FormProductores : TControlcajonesproductores;
begin
  FormProductores := TControlCajonesProductores.Create(nil);
  FormProductores.Conexion := Conexion;
  FormProductores.Caption := 'Productor - '+cmbProductor.Text;
  FormProductores.productor_id := cmbProductor.KeyValue;
  FormProductores.ShowModal;
  actualizar_productores;

end;

procedure TControlCajonesEntradaSalida.mnuItemNuevoClick(Sender: TObject);
var
  FormProductores : TControlcajonesproductores;
begin
  FormProductores := TControlCajonesProductores.Create(nil);

  FormProductores.Conexion.Params := Conexion.Params;
  FormProductores.Caption := 'Productor - Nuevo Productor';
  FormProductores.productor_id := 0;
  FormProductores.ShowModal;
  actualizar_productores;
end;

procedure TControlCajonesEntradaSalida.mnuProductoresPopup(Sender: TObject);
begin
  mnuItemAbrir.Enabled := cmbProductor.Text<>'';
end;
{$ENDREGION}

{$REGION 'Clave de Productor'}

procedure TControlCajonesEntradaSalida.txtClaveProductorExit(Sender: TObject);
var
  productor_id :integer;
begin
  productor_id := Conexion.ExecSQLScalar('select id from sic_productores where clave=:cl',[txtClaveProductor.Text]);
  if productor_id <> 0 then
  begin
    cmbProductor.KeyValue := productor_id;
  end
  else
  begin
    ShowMessage('Proveedor no Encontrado');
    txtClaveProductor.selectall;
    txtClaveProductor.setfocus;
  end;
end;
{$ENDREGION}

{$REGION 'Boton Guardar'}

procedure TControlCajonesEntradaSalida.btnDevolverClick(Sender: TObject);
var
   Matriz: array of array of String;
   i, j : integer;
  k,mlongitud,Index: integer;
  Remover:TStringList;
  query_str:string;
begin
Remover := TStringList.Create;
   i:=0;
   j:=0;
   datos.Cells[9,0]:='Disponible';
   //SE CREA LA MATRIZ DEL TAMA�O DEL STRINGRID
  SetLength(Matriz, 10, datos.RowCount);
  //SE GUARDAN LOS DATOS DISPONIBLES DEL STRINGRID EN LA MATRIZ
  for i := 0 to 9 do
    begin
      for j := 0 to datos.RowCount-1 do
        begin
        if datos.Cells[9,j]='S' then
                begin
                Matriz[i,j]:=datos.Cells[i,j];
                //SE GUARDAN EN ARREGLO LOS DISPONIBLES DEL DOCUMENTO PARA ELIMINARLOS AL GUARDAR
                    if not Eliminar.Find(datos.Cells[10,j],Index) then
                    Eliminar.Add(datos.Cells[10,j]);
                end
                   else
                   begin
                   Matriz[i,j]:='';
                   end;
         end;
    end;

   //SE ELIMINAN LOS DATOS DEL STRINGGRID
   with datos do
   begin
   for k := 0 to RowCount-1 do
     begin
     Rows[k].Clear;
     end;
   end;

   //SE LLENA STRINGGRID CON LOS DATOS DEL ARREGLO
   i:=0;
   j:=0;
   Matriz[8,0]:='0';
   for i := 0 to 9 do
    begin

      for j := 1 to datos.RowCount-1 do
          begin
            if Matriz[i,j]='' then
            begin
            if Remover.Find(inttostr(j),Index) then
              begin
              continue;
              end
              else
            Remover.Add(inttostr(j));
            end
            else
            datos.Cells[i,j]:=Matriz[i,j];
          end;
    end;
   i:=0;
    //SE ELIMINAN RENGLONES VACIOS
    for i := Remover.Count-1 Downto 0 do
      begin
      EliminarRenglon(datos,strtoint(Remover[i]));
      end;

    //VACIAR DATOS DE ENTRADA CAJONES
    TxtFolio.Text:='';
    txtChofer.Text:='';
    txtChofer.Enabled:=true;
    txtPlacas.Text:='';
    txtPlacas.Enabled:=true;
    txtTotalCajones.Text:='';
    txtTotalCajones.Enabled:=true;
    txtTotalPesoBruto.Text:='';
    txtTotalPesoBruto.Enabled:=true;
    txtTotalPesoNeto.Text:='';
    txtTotalPesoNeto.Enabled:=true;
    txtTotalCosto.Text:='';
    txtTotalCosto.Enabled:=true;
    txtTotalTara.Text:='';
    txtTotalTara.Enabled:=true;
    dtpfecha.Enabled:=true;
    cmbProductor.Enabled:=true;
    txtClaveProductor.Enabled:=true;
    cmbProveedor.Enabled:=true;
    btnPoliza.Visible:=false;
    txtPromedioporCajon.Text:='';
    txtPromedioporCajon.Enabled:=true;
    cmbAlmacenes.ListFieldIndex:=0;
    cmbAlmacenes.Enabled:=true;
    btnRecibo.visible:=false;
    btnEtiquetas.Visible:=false;
    btnRecalcular.Visible:=false;
    panel1.Visible:=false;

      datos.Cells[0,0] := 'Cajones';
  datos.Cells[1,0] := 'Articulo';
  datos.Cells[2,0] := 'Costo Unitario';
  datos.Cells[3,0] := 'Total';
  datos.Cells[4,0] := 'Peso Bruto';
  datos.Cells[5,0] := 'Tara';
  datos.Cells[6,0] := 'Peso Neto';
  datos.Cells[7,0] := 'Propietario';
   datos.Cells[9,0]:='Disponible';
   docto_cm__id:=docto_id;
   docto_id:=0;
    Totales;
    btnDevolver.Enabled:=false;
end;

procedure TControlCajonesEntradaSalida.EliminarRenglon(Grid: TStringGrid; ARow: Integer);
var
  i: Integer;
begin
  for i := ARow to Grid.RowCount - 2 do
    Grid.Rows[i].Assign(Grid.Rows[i + 1]);
  Grid.RowCount := Grid.RowCount - 1;
end;

procedure TControlCajonesEntradaSalida.btnEtiquetasClick(Sender: TObject);
begin
  ImprimirEtiquetas;
end;

procedure TControlCajonesEntradaSalida.btnGuardarClick(Sender: TObject);
var
num_cajones,docto_cm_id,proveedor_id,almacen_id,moneda_id,cond_pago_id,productor_id,documento_id,Existe,articulo_id,i,j:integer;
             clave_proveedor,fecha,query_str,clave_articulo,propietario_cajon,codigo_barras_cajon:string;
             unidades,tara,precio_unitario,precio_total_neto,peso_bruto:double;
             detalle_id,articulo_discreto_id,consecutivo,entrada_id:integer;
begin

//**********SI EXISTE***********//
           Existe:=Conexion.ExecSQLScalar('select docto_cm_id from doctos_cm where docto_cm_id=:folio',[docto_id]);
    if Existe = 0 then
    begin
    if Eliminar.Count>0 then
       begin
       //CANCELAR SI TODOS LOS DETALLES SE ELIMINAN
        if Conexion.ExecSQLScalar('select count(docto_cm_id) from doctos_cm_det where docto_cm_id=:folio',[docto_cm__id])=Eliminar.Count then
        begin
        Conexion.ExecSQL('update doctos_cm set estatus=''C'' where docto_cm_id=:id',[docto_cm__id]);
        end
        else
        //SI NO, SE ELIMINAN LOS DETALLES
          begin
          try
          i:=0;
          for i := 0 to (Eliminar.Count-1) do
           begin
            qryEliminar.SQL.Text:='delete from doctos_cm_det where docto_cm_det_id=:id';
            qryEliminar.ParamByName('id').Value:=Eliminar[i];
            qryEliminar.ExecSQL;
            end;
            except
            Conexion.Rollback;
            end;
        end;

        //CANCELAR EN CASO DE QUE NO HAYA DETALLES
      {  if Conexion.ExecSQLScalar('Select count(docto_cm_id) from doctos_cm_det where docto_cm_id=:id',[docto_cm__id])=0 then
        begin
        Conexion.ExecSQL('update doctos_cm set estatus=''C'' where docto_cm_id=:id',[docto_cm__id]);
        end;  }
       end;
     //******SI ES ENTRADA**********
      if CmbTipo.ItemIndex = 0 then
      begin
              case CmbClase.ItemIndex of
                   0:begin
                    liga_in := 'S';
                    EntradaVacio;
                    EntradaManzana;
                    LimpiarGrid;
                    EnviarAFormato;
                    ImprimirEtiquetas;

                  end;
                1:begin
                    liga_in := 'N';
                    EntradaVacio;
                    LimpiarGrid;
                    EnviarAFormato;
                end;
              end;
      end



  //******SI ES SALIDA***********
      else
      begin
        SalidaVacio;
        LimpiarGrid;
        txtTotalCajones.Clear;
        txtTotalCajones.SetFocus;
        EnviarAFormato;
        end;
        borrarRenglones;
        self.close;
    end
     else
      begin
      //ELIMINAR LOS DETALLES DEL DOCUMENTO DE COMPRAS ANTERIOR


        //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
        consecutivo := Conexion.ExecSQLScalar('select consecutivo from folios_compras where tipo_docto = ''R''');
        proveedor_id := cmbProveedor.KeyValue;
        clave_proveedor := Conexion.ExecSQLScalar('select coalesce(clave_prov,'''') from get_clave_prov(:id)',[proveedor_id]);
        almacen_id := cmbAlmacenes.KeyValue;
        moneda_id := 1;
        cond_pago_id := Conexion.ExecSQLScalar('select first 1 cond_pago_id from proveedores where proveedor_id = :pid',[proveedor_id]);
        fecha := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);
        productor_id :=  cmbProductor.KeyValue;
        docto_cm_id:=Conexion.ExecSQLScalar('select docto_cm_id from doctos_cm_det where docto_cm_id=:docto',[docto_id]);

        query_str := 'update doctos_cm set fecha='''+fecha+''',clave_prov='''+clave_proveedor+''',proveedor_id='+
        inttostr(proveedor_id)+',almacen_id='+inttostr(almacen_id)+',cond_pago_id='+inttostr(cond_pago_id)+',moneda_id='+inttostr(moneda_id)+',sic_productor='+IntToStr(productor_id)+','+
        'sic_chofer='''+txtChofer.Text+''',sic_placas='''+txtPlacas.Text+''',sic_caracteristica='''+txtCaracteristica.Text+''' where docto_cm_id='+inttostr(docto_id)+'';
        Conexion.ExecSQL(query_str);
        Conexion.ExecSQL('execute procedure calc_totales_docto_cm(:did,''N'')',[documento_id]);
        Conexion.ExecSQL('execute procedure aplica_docto_cm(:id)',[documento_id]);

       //Eliminar detalles si existen
                 if Conexion.ExecSQLScalar('select count(docto_cm_id) from doctos_cm_det where docto_cm_id=:id',[inttostr(docto_cm_id)])>0 then
              begin
              query_str := 'delete from doctos_cm_det where docto_cm_id='+inttostr(docto_cm_id)+'';
              Conexion.ExecSQL(query_str);
              end;
            //*************DETALLADO***************//
                 for i := 1 to datos.RowCount-1 do
         begin
                      articulo_id := strtoint(datos.cells[8,i]);
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          unidades := strtofloat(datos.cells[6,i]);
          precio_unitario := StrToFloat(datos.cells[2,i]);
          precio_total_neto := StrToFloat(datos.cells[3,i]);
          num_cajones := StrToInt(datos.cells[0,i]);
          peso_bruto := StrToFloat(datos.cells[4,i]);
          tara := StrToFloat(datos.cells[5,i]);
          propietario_cajon := datos.Cells[7,i];
          query_str := 'insert into doctos_cm_det (docto_cm_det_id, docto_cm_id,'+
          ' clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,notas,pctje_dscto,sic_num_cajones,'+
          ' sic_peso_bruto,sic_tara,sic_propietario_cajon) values(-1,'+
          ' '+inttostr(docto_cm_id)+','''+clave_articulo+''','+inttostr(articulo_id)+','+FloatToStr(unidades)+
          ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+','''',0,'+
          ' '+IntToStr(num_cajones)+','+FloatToStr(peso_bruto)+','+FloatToStr(tara)+','''+propietario_cajon+''') returning docto_cm_det_id ';
          detalle_id := Conexion.ExecSQLScalar(query_str);
          if Conexion.ExecSQLScalar('select count(*) from articulos_discretos where articulo_id='+inttostr(articulo_id)+' and clave='''+IntToStr(consecutivo)+''' ')>0
             then articulo_discreto_id := Conexion.ExecSQLScalar('select art_discreto_id from articulos_discretos where articulo_id='+inttostr(articulo_id)+' and clave='''+IntToStr(consecutivo)+'''')
             else articulo_discreto_id := Conexion.ExecSQLScalar('insert into articulos_discretos values(-1,'''+IntToStr(consecutivo)+''','+inttostr(articulo_id)+',''L'',current_date) returning art_discreto_id');
          Conexion.ExecSQL('insert into desglose_en_discretos_cm values(-1,'+inttostr(detalle_id)+','+IntToStr(articulo_discreto_id)+','+FloatToStr(unidades)+')');
          end;
      end;

      //ACTUALIZAR ENTRADA
      if Conexion.ExecSQLScalar('select count(recepcion_id) from sic_maquila_det_entrada where recepcion_id=:id',[inttostr(docto_cm_id)])>0 then
      begin
        for i := 1 to datos.RowCount-1 do
         begin
          Conexion.ExecSQL('update sic_maquila_det_entrada set costo=:c,peso=:p,articulo_id=:a where recepcion_id=:r',[StrToFloat(datos.cells[2,i]),StrToFloat(datos.cells[4,i]),strtoint(datos.cells[8,i]),inttostr(docto_cm_id)]);
         end;
      end;

      //ACTUALIZAR DOCUMENTO DE ENTRADA
      if Conexion.ExecSQLScalar('select entrada_id from sic_maquila where recepcion_id=:id',[inttostr(docto_cm_id)])<>'' then
      begin
        for i := 1 to datos.RowCount-1 do
         begin
          entrada_id:=Conexion.ExecSQLScalar('select entrada_id from sic_maquila where recepcion_id=:id',[inttostr(docto_cm_id)]);
          articulo_id := strtoint(datos.cells[8,i]);
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          unidades := strtofloat(datos.cells[6,i]);
          precio_unitario := StrToFloat(datos.cells[2,i]);
          precio_total_neto := StrToFloat(datos.cells[3,i]);
          num_cajones := StrToInt(datos.cells[0,i]);
          peso_bruto := StrToFloat(datos.cells[4,i]);
          Conexion.ExecSQL('update doctos_in_det set clave_articulo=:ca,articulo_id=:a,unidades=:u,costo_unitario=:cu,costo_total=:ct where docto_in_id=:r and articulo_id=a1',[clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,entrada_id,articulo_id]);
         end;
      end;

      //ACTUALIZAR DOCUMENTO DE SALIDA Y ENTRADA EN CASO DE QUE EXISTAN
              //GENERAR CAJONES
               for i := 1 to datos.RowCount-1 do
                 begin
                 num_cajones := StrToInt(datos.cells[0,i]);
                  for j := 1 to num_cajones do
                    begin
                    codigo_barras_cajon:=RightStr(TxtFolio.Text,6)+Format('%.*d',[3, i]);
                    //SALIDA
                    //VERIFICAR SI EXISTE
                    if Conexion.ExecSQLScalar('select modulo_id from claves_cajones where modulo=:m and cod_barras=:c',['SM',codigo_barras_cajon])>0 then
                     //MODIFICAR
                     begin
                      entrada_id:=Conexion.ExecSQLScalar('select modulo_id from claves_cajones where modulo=:m and cod_barras=:c',['SM',codigo_barras_cajon]);
                      articulo_id := strtoint(datos.cells[8,i]);
                      clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
                      unidades := strtofloat(datos.cells[6,i]);
                      precio_unitario := StrToFloat(datos.cells[2,i]);
                      precio_total_neto := StrToFloat(datos.cells[3,i]);
                      num_cajones := StrToInt(datos.cells[0,i]);
                      peso_bruto := StrToFloat(datos.cells[4,i]);
                      Conexion.ExecSQL('update doctos_in_det set clave_articulo=:ca,articulo_id=:a,unidades=:u,costo_unitario=:cu,costo_total=:ct where docto_in_id=:r and articulo_id=:a1',[clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,entrada_id,articulo_id]);
                     end;

                     //ENTRADA
                     //VERIFICAR SI EXISTE
                    if Conexion.ExecSQLScalar('select modulo_id from claves_cajones where modulo=:m and cod_barras=:c',['EM',codigo_barras_cajon])>0 then
                     //MODIFICAR
                     begin
                      entrada_id:=Conexion.ExecSQLScalar('select modulo_id from claves_cajones where modulo=:m and cod_barras=:c',['EM',codigo_barras_cajon]);
                      articulo_id := strtoint(datos.cells[8,i]);
                      clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
                      unidades := strtofloat(datos.cells[6,i]);
                      precio_unitario := StrToFloat(datos.cells[2,i]);
                      precio_total_neto := StrToFloat(datos.cells[3,i]);
                      num_cajones := StrToInt(datos.cells[0,i]);
                      peso_bruto := StrToFloat(datos.cells[4,i]);
                      Conexion.ExecSQL('update doctos_in_det set clave_articulo=:ca,articulo_id=:a,unidades=:u,costo_unitario=:cu,costo_total=:ct where docto_in_id=:r and articulo_id=:a1',[clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,entrada_id,articulo_id]);
                     end;
                    end;
                 end;

               //ACTUALIZAR RECEPCION DE MERCANCIA EN INVENTARIOS
               if TxtFolio.Text<>'' then
               begin
                if Conexion.ExecSQLScalar('select docto_in_id from doctos_in where folio=:f',[TxtFolio.Text])>0 then
                  begin
                     for i := 1 to datos.RowCount-1 do
                        begin
                        entrada_id:=Conexion.ExecSQLScalar('select docto_in_id from doctos_in where folio=:f',[TxtFolio.Text]);
                        articulo_id := strtoint(datos.cells[8,i]);
                        clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
                        unidades := strtofloat(datos.cells[6,i]);
                        precio_unitario := StrToFloat(datos.cells[2,i]);
                        precio_total_neto := StrToFloat(datos.cells[3,i]);
                        num_cajones := StrToInt(datos.cells[0,i]);
                        peso_bruto := StrToFloat(datos.cells[4,i]);
                        Conexion.ExecSQL('update doctos_in_det set clave_articulo=:ca,articulo_id=:a,unidades=:u,costo_unitario=:cu,costo_total=:ct where docto_in_id=:r and articulo_id=:a1',[clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,entrada_id,articulo_id]);
                        end;
                  end;
               end;

              Conexion.Commit;
            ShowMessage('Actualizado correctamente');
            self.Close;
end;

{$ENDREGION}

{$REGION 'Boton Imprimir Recibo'}
procedure TControlCajonesEntradaSalida.btnRecalcularClick(Sender: TObject);
var importe,unidades,precio_unitario,precio_total_neto,peso_bruto,tara:double;
query_str1,query_str2,clave_articulo,propietario_cajon,query_str:string;
det_id1,det_id2,poliza_id,cta_id1,cta_id2,recepcion_id,articulo_id,num_cajones,articulo_discreto_id,detalle_id,i:integer;
insertar:bool;
begin
    cta_id1:=Conexion.ExecSQLScalar('select cco.cuenta_id from almacenes a join cuentas_co cco on a.cuenta_almacen=cco.cuenta_pt where almacen_id=:aid',[cmbAlmacenes.KeyValue]);
    cta_id2:=Conexion.ExecSQLScalar('SELECT CC.cuenta_id FROM cuentas_co CC WHERE CC.cuenta_pt = (SELECT CUENTA_CXP FROM PROVEEDORES WHERE PROVEEDOR_ID = :Pid)',[cmbProveedor.KeyValue]);
    poliza_id:=Conexion.ExecSQLScalar('select count(docto_co_id) from doctos_co_det where refer=:folio',[TxtFolio.Text]);
    if poliza_id=0 then
      begin
        btnPoliza.Click;
      end
      else
      begin
      poliza_id:=Conexion.ExecSQLScalar('select docto_co_id from doctos_co_det where refer=:folio',[TxtFolio.Text]);
      Conexion.ExecSQL('delete from doctos_co_det where docto_co_id=:did',[poliza_id]);
    

        importe:=strtofloat(txtTotalCosto.Text);
        query_str1 := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
        ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
        ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
        ',''C'','+ floattostr(importe)+','+floattostr(importe)+','''+txtFolio.Text+''','''+cmbProveedor.Text+''',-1,1,''S'') returning docto_co_det_id';
        det_id1 := Conexion.ExecSQLScalar(query_str1,[cta_id1]);
        //  Conexion.Commit;
        {**********************************AGREGA DETALLADO A CUENTA PROOVEDORES****************************}
        query_str2 := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
        ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
        ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
        ',''A'','+ floattostr(importe)+','+floattostr(importe)+','''+txtFolio.Text+''','''+cmbProveedor.Text+''',-1,1,''S'') returning docto_co_det_id';
        det_id2 := Conexion.ExecSQLScalar(query_str2,[cta_id2]);

          Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_id1]);
          Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_id2]);
          Conexion.ExecSQL('Update doctos_co set aplicado=''S'' where docto_co_id=:id',[poliza_id]);
          Conexion.ExecSQL('execute procedure aplica_docto_co(:id,''A'')',[poliza_id]);
                       
        Conexion.Commit;

        //ELIMINAR DOCUMENTO DE DETALLE E INSERTAR NUEVO// 
         recepcion_id:=Conexion.ExecSQLScalar('select docto_cm_id from doctos_cm where folio=:folio and tipo_docto=''R''',[txtFolio.Text]);
         Conexion.ExecSQL('delete from doctos_cm_det where docto_cm_id=:did',[recepcion_id]);
         Conexion.Commit;
          //*******************************DETALLADO DEL DOCUMENTO*********************************************
     for i := 1 to datos.RowCount-1 do
         begin
        insertar := true;
           try
          articulo_id := strtoint(datos.cells[8,i]);
          clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
          unidades := strtofloat(datos.cells[6,i]);
          precio_unitario := StrToFloat(datos.cells[2,i]);
          precio_total_neto := StrToFloat(datos.cells[3,i]);
          num_cajones := StrToInt(datos.cells[0,i]);
          peso_bruto := StrToFloat(datos.cells[4,i]);
          tara := StrToFloat(datos.cells[5,i]);
          propietario_cajon := datos.Cells[7,i];
            except
          insertar := false;
           end;

        if insertar then
        begin
          query_str := 'insert into doctos_cm_det (docto_cm_det_id, docto_cm_id,'+
                       ' clave_articulo,articulo_id,unidades,precio_unitario,precio_total_neto,notas,pctje_dscto,sic_num_cajones,'+
                       ' sic_peso_bruto,sic_tara,sic_propietario_cajon) values(-1,'+
                       ' '+inttostr(recepcion_id)+','''+clave_articulo+''','+inttostr(articulo_id)+','+FloatToStr(unidades)+
                       ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+','''',0,'+
                       ' '+IntToStr(num_cajones)+','+FloatToStr(peso_bruto)+','+FloatToStr(tara)+','''+propietario_cajon+''') returning docto_cm_det_id ';
          detalle_id := Conexion.ExecSQLScalar(query_str);
           Conexion.Commit;
        end
        else
        begin
          Conexion.Rollback;
        end;
         end;
       Conexion.ExecSQL('execute procedure calc_totales_docto_cm(:did,''N'')',[recepcion_id]);
       Conexion.Commit;

        ShowMessage('Poliza Recalculada correctamente.');
      end;
end;



procedure TControlCajonesEntradaSalida.btnReciboClick(Sender: TObject);
begin
  EnviarAFormato;
end;
procedure TControlCajonesEntradaSalida.btnPolizaClick(Sender: TObject);
var
query_str,query_str1,query_str2,tipo_cons,folio_poliza,prefijo_poliza,cta_id,cta_id1,cta_id2:string;
det_id,det_id1,det_id2,tipo_poliza_det_id,poliza_id,tipo_poliza_id,mes,anio,consec,newcta,registros:integer;
poliza_existe:bool;
importe:double;
begin
tipo_poliza_id:=Conexion.ExecSQLScalar('Select sic_poliza from almacenes where almacen_id=:aid',[cmbAlmacenes.KeyValue]);

 if Conexion.ExecSQLScalar('select prefijo from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]) = null then
      prefijo_poliza := ''
    else
      prefijo_poliza := Conexion.ExecSQLScalar('select prefijo from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]);


           //OBTIENE EL CONSECUTIVO DE LA POLIZA A GENERAR
    tipo_cons := Conexion.ExecSQLScalar('select tipo_consec from tipos_polizas where tipo_poliza_id = :tpi',[tipo_poliza_id]);
    if tipo_cons = 'M' then
    begin
      mes := Monthof(dtpFecha.Date);
      anio := YearOf(dtpFecha.Date);
      tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                      ' where tipo_poliza_id = :pid and mes=:mes and ano=:a',[tipo_poliza_id,mes,anio]);
      if tipo_poliza_det_id = 0 then
      begin
       tipo_poliza_det_id := Conexion.ExecSQLScalar('insert into tipos_polizas_det values(-1,:pid,:a,:m,1) returning tipo_poliza_det_id',[tipo_poliza_id,anio,mes]);
      end;
    end;
    if tipo_cons = 'E' then
    begin
      anio := YearOf(dtpFecha.Date);
      tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                      ' where tipo_poliza_id = :pid and  ano=:a',[tipo_poliza_id,anio]);
    end;
    if tipo_cons = 'P' then
    begin
      tipo_poliza_det_id := Conexion.ExecSQLScalar('select tipo_poliza_det_id from tipos_polizas_det'+
                      ' where tipo_poliza_id = :pid',[tipo_poliza_id]);
    end;
    consec := Conexion.ExecSQLScalar('select consecutivo from tipos_polizas_det where tipo_poliza_det_id = :tp',[tipo_poliza_det_id]);
    folio_poliza := prefijo_poliza + Format('%.*d',[9-(prefijo_poliza.length), consec]);
    try
    cta_id1:=Conexion.ExecSQLScalar('select cco.cuenta_id from almacenes a join cuentas_co cco on a.cuenta_almacen=cco.cuenta_pt where almacen_id=:aid',[cmbAlmacenes.KeyValue]);
    except
    ShowMessage('Falta cuenta contable de almacen en el almacen: '+Conexion.ExecsqlScalar('select nombre from almacenes where almacen_id=:aid',[cmbAlmacenes.KeyValue]));
    end;
    try
    cta_id2:=Conexion.ExecSQLScalar('SELECT CC.cuenta_id FROM cuentas_co CC WHERE CC.cuenta_pt = (SELECT CUENTA_CXP FROM PROVEEDORES WHERE PROVEEDOR_ID = :Pid)',[cmbProveedor.KeyValue]);
    except
    ShowMessage('Falta cuenta por pagar del proveedor: '+Conexion.ExecsqlScalar('select nombre from proveedores where proveedor_id=:aid',[cmbProveedor.KeyValue]));
    end;
    registros:=Conexion.ExecSQLScalar('Select count(*) from doctos_co_det where refer=:folio',[TxtFolio.Text]);
    if registros=0 then
        begin
             if cta_id1<>'' then
               begin
                  if cta_id2<>'' then
                      begin
                        {***************************INSERTA EL ENCABEZADO DE LA POLIZA*******************************}
                        query_str := 'insert into doctos_co(docto_co_id,tipo_poliza_id,poliza,fecha,moneda_id,tipo_cambio,estatus,sistema_origen,aplicado,descripcion)'+
                                      ' values(-1,'+inttostr(tipo_poliza_id)+','+QuotedStr(folio_poliza)+','''+FormatDateTime('mm/dd/yyyy',dtpFecha.Date)+''',1,1,''N'',''CO'',''N'',''Entrada de Cajones '+TxtFolio.Text+''') returning docto_co_id';
                         poliza_id := Conexion.ExecSQLScalar(query_str);
                        {**************************ACTUALIZA EL DETALLE DEL TIPO DE POLIZA***************************}
                                 {**********************************AGREGA DETALLADO A CUENTA ALMACEN****************************}
                        Conexion.ExecSQL('update tipos_polizas_det set consecutivo = consecutivo+1 where tipo_poliza_det_id = :t',[tipo_poliza_det_id]);
                        poliza_existe := False;
                        importe:=strtofloat(txtTotalCosto.Text);
                        query_str1 := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                              ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                              ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                              ',''C'','+ floattostr(importe)+','+floattostr(importe)+','''+txtFolio.Text+''','''+cmbProveedor.Text+''',-1,1,''S'') returning docto_co_det_id';
                        det_id1 := Conexion.ExecSQLScalar(query_str1,[cta_id1]);
                        //  Conexion.Commit;
                        {**********************************AGREGA DETALLADO A CUENTA PROOVEDORES****************************}
                            query_str2 := 'insert into doctos_co_det(docto_co_det_id,docto_co_id,cuenta_id,depto_co_id,tipo_asiento'+
                              ',importe,importe_mn,refer,descripcion,posicion,moneda_id,aplicado) values(-1,'+inttostr(poliza_id)+','+
                              ':cta_id,(select depto_co_id from deptos_co where upper(nombre) = ''GENERAL'' )'+
                              ',''A'','+ floattostr(importe)+','+floattostr(importe)+','''+txtFolio.Text+''','''+cmbProveedor.Text+''',-1,1,''S'') returning docto_co_det_id';
                              det_id2 := Conexion.ExecSQLScalar(query_str2,[cta_id2]);

                            Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_id1]);
                            Conexion.ExecSQL('execute procedure recalc_saldos_cta_co(:cta_id)',[cta_id2]);
                            Conexion.ExecSQL('Update doctos_co set aplicado=''S'' where docto_co_id=:id',[poliza_id]);
                            Conexion.ExecSQL('execute procedure aplica_docto_co(:id,''A'')',[poliza_id]);

                            Conexion.Commit;
                            btnPoliza.Visible:=false;
                            btnRecalcular.Visible:=True;
                            grid_datos.enabled:=true;
                            datos.enabled:=true;
                           ShowMessage('Poliza creada correctamente.');
                           cbConta.Checked:=True;
                      end
                      else
                      begin
                         ShowMessage('ERROR: No existe cuenta para este proveedor.');
                      end;
               end
               else
               begin
                 ShowMessage('ERROR: No existe cuenta para este amlacen.');
               end;
        end;


end;


{$ENDREGION}

{$REGION 'Combos'}
procedure TControlCajonesEntradaSalida.CmbClaseChange(Sender: TObject);
begin
  //
  case CmbClase.ItemIndex of
    //********* ENTRADA**********
    0:begin
      datos.Visible := true;
      txtTotalCajones.Text := '0';
      txttotalCajones.Enabled := False;
      datos.SetFocus;
      
    end;
    //******* SALIDA ************
    1:begin
      datos.Visible := False;
      txtTotalCajones.Enabled := True;
      txtTotalCajones.SetFocus;
    end;

  end;
  LimpiarGrid;
end;

procedure TControlCajonesEntradaSalida.CmbTipoChange(Sender: TObject);
begin
   case CmbTipo.ItemIndex of
    //********* ENTRADA**********
    0:begin
      datos.Visible := true;
      txtTotalCajones.Text := '0';
      txttotalCajones.Enabled := False;
      datos.SetFocus;
      CmbClase.items.Clear;
      CmbClase.items.Add('Con Manzana');
      CmbClase.items.Add('Vacios');
      CmbClase.ItemIndex := 0;
      CmbClase.Enabled := True;
    end;
    //******* SALIDA ************
    1:begin
      datos.Visible := False;
      txtTotalCajones.Enabled := True;
      txtTotalCajones.SetFocus;
      CmbClase.items.Clear;
      CmbClase.items.Add('Vacios');
      CmbClase.ItemIndex := 0;
      CmbClase.Enabled := False;
    end;

  end;
  LimpiarGrid;
end;


{$ENDREGION}

end.

