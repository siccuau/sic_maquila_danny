object ControlCajonesMaquilaEmbarqueSalida: TControlCajonesMaquilaEmbarqueSalida
  Left = 0
  Top = 0
  Caption = 'Embarque Salida'
  ClientHeight = 256
  ClientWidth = 603
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 237
    Width = 603
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object gridMaterial: TStringGrid
    Left = 8
    Top = 14
    Width = 585
    Height = 171
    ColCount = 3
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 1
  end
  object Button1: TButton
    Left = 304
    Top = 197
    Width = 89
    Height = 34
    Caption = 'Agregar'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 192
    Top = 197
    Width = 89
    Height = 34
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Server=localhost'
      'Protocol=TCPIP'
      'DriverID=FB')
    LoginPrompt = False
    Left = 627
    Top = 302
  end
end
