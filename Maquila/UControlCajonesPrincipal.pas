unit UControlCajonesPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client, Vcl.ImgList,
  Vcl.StdCtrls,UControlCajonesMaquila,UControlCajonesLista,FireDAC.VCLUI.Wait,
  System.ImageList, UControlCajonesConfig, Vcl.Menus,UControlEmbarque,UControlCajonesReportes;

type
  TControlCajonesPrincipal = class(TForm)
    btnMaquila: TButton;
    btnCajones: TButton;
    ImageList1: TImageList;
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    MainMenu1: TMainMenu;
    Config1: TMenuItem;
    Almacenes1: TMenuItem;
    Salir1: TMenuItem;
    btnEmbarque: TButton;
    R1: TMenuItem;
    procedure btnMaquilaClick(Sender: TObject);
    procedure btnCajonesClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure creatablas;
    procedure FormShow(Sender: TObject);
    procedure bntConfigClick(Sender: TObject);
    procedure Salir1Click(Sender: TObject);
    procedure Almacenes1Click(Sender: TObject);
    procedure btnEmbarqueClick(Sender: TObject);
    procedure R1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesPrincipal: TControlCajonesPrincipal;

implementation

{$R *.dfm}


procedure TControlCajonesPrincipal.btnCajonesClick(Sender: TObject);
var
  FormEmbarque:TControlCajonesLista;
begin
  FormEmbarque := TControlCajonesLista.Create(nil);
  FormEmbarque.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormEmbarque.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormEmbarque.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormEmbarque.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormEmbarque.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormEmbarque.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormEmbarque.ShowModal;
end;

procedure TControlCajonesPrincipal.btnEmbarqueClick(Sender: TObject);
var
  FormEmbarque:TFormEmbarqueMaquilas;
begin
  FormEmbarque := TFormEmbarqueMaquilas.Create(nil);
  FormEmbarque.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormEmbarque.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormEmbarque.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormEmbarque.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormEmbarque.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormEmbarque.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormEmbarque.ShowModal;
end;

procedure TControlCajonesPrincipal.btnMaquilaClick(Sender: TObject);
var
  FormMaquila:TControlCajonesMaquila;
begin
  FormMaquila := TControlCajonesMaquila.Create(nil);
  FormMaquila.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormMaquila.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormMaquila.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormMaquila.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormMaquila.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormMaquila.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormMaquila.ShowModal;
end;

procedure TControlCajonesPrincipal.Almacenes1Click(Sender: TObject);
var
  FormConfig:TControlCajonesConfig;
begin
  FormConfig := TControlCajonesConfig.Create(nil);
  FormConfig.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormConfig.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormConfig.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormConfig.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormConfig.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormConfig.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormConfig.ShowModal;
end;

procedure TControlCajonesPrincipal.bntConfigClick(Sender: TObject);
var
  FormConfig:TControlCajonesConfig;
begin
  FormConfig := TControlCajonesConfig.Create(nil);
  FormConfig.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormConfig.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormConfig.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormConfig.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormConfig.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormConfig.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormConfig.ShowModal;
end;

procedure TControlCajonesPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Conexion.Close;
  Application.Terminate;
end;

procedure TControlCajonesPrincipal.FormCreate(Sender: TObject);
begin
  SetWindowLong(Handle, GWL_EXSTYLE, WS_EX_APPWINDOW);
end;

procedure TControlCajonesPrincipal.FormShow(Sender: TObject);
begin
  creatablas;
end;

procedure TControlCajonesPrincipal.R1Click(Sender: TObject);
var
  FormConfig:TControlCajonesReportes;
begin
  FormConfig := TControlCajonesReportes.Create(nil);
  FormConfig.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormConfig.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormConfig.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormConfig.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormConfig.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormConfig.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormConfig.ShowModal;
end;

procedure TControlCajonesPrincipal.Salir1Click(Sender: TObject);
begin
  Conexion.Close;
  Application.Terminate;
end;

procedure TControlCajonesPrincipal.Creatablas;
begin
  //crea las tablas si no existen
  // TABLA SIC_MAQUILA
  conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_MAQUILA'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_MAQUILA ('+
                                ' MAQUILA_ID            INTEGER NOT NULL,'+
                                ' PROVEEDOR_ID          INTEGER,'+
                                ' FECHA                 DATE,'+
                                ' FOLIO                 VARCHAR(6),'+
                                ' ESTATUS               CHAR(1),'+
                                ' FECHAHORA_APERTURA    TIMESTAMP,'+
                                ' FECHAHORA_LIBERACION  TIMESTAMP,'+
                                ' TARIMAS_SALIDA        INTEGER,'+
                                ' ALMACEN_ENTRADA_ID    INTEGER,'+
                                ' ALMACEN_SALIDA_ID     INTEGER,'+
                                ' TRASPASO     CHAR(1)'+
                            ' );'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_MAQUILA'';'+
        ' execute statement ''set GENERATOR SIC_ID_MAQUILA TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_MAQUILA_BI0 FOR SIC_MAQUILA '+
                                ' ACTIVE BEFORE INSERT POSITION 0'+
                                ' AS'+
                                ' begin'+
                                  ' /* Genera un nuevo id */'+
                                  ' if (new.maquila_id = -1) then'+
                                    ' new.maquila_id = gen_id(sic_id_maquila,1);'+
                                ' end'';'+
    ' END end');

    //TABLA SIC_MAQUILA DET_SALIDA
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_MAQUILA_DET_SALIDA'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_MAQUILA_DET_SALIDA ('+
                                ' MAQUILA_DET_ID  INTEGER NOT NULL,'+
                                ' MAQUILA_ID      INTEGER,'+
                                ' NUM_TARIMA      VARCHAR(9),'+
                                ' PESO      INTEGER,'+
                                ' CAJAS      INTEGER,'+
                                 ' ETIQUETA      VARCHAR(100)'+
                            ' );'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_MAQUILA_DET_SALIDA;'';'+
        ' execute statement ''set GENERATOR SIC_ID_MAQUILA_DET_SALIDA TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_MAQUILA_DET_SALIDA_BI0 FOR SIC_MAQUILA_DET_SALIDA'+
                            ' ACTIVE BEFORE INSERT POSITION 0'+
                            ' AS'+
                            ' begin'+
                              ' /* genera un nuevo id */'+
                              ' if (new.maquila_det_id = -1) then'+
                                ' new.maquila_det_id = gen_id(sic_id_maquila_det_salida,1);'+
                            ' end'';'+
    ' END end');

    //TABLA SIC_MAQUILA DET_ENTRADA
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_MAQUILA_DET_ENTRADA'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_MAQUILA_DET_ENTRADA ('+
                                ' MAQUILA_DET_ID  INTEGER NOT NULL,'+
                                ' RECEPCION_ID      VARCHAR(15),'+
                                ' COD_BARRAS      VARCHAR(9),'+
                                ' PESO      DECIMAL(15,2),'+
                                ' ARTICULO_ID      INTEGER'+
                            ' );'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_MAQUILA_DET_ENTRADA;'';'+
        ' execute statement ''set GENERATOR SIC_ID_MAQUILA_DET_ENTRADA TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_MAQUILA_DET_ENTRADA_BI0 FOR SIC_MAQUILA_DET_ENTRADA'+
                            ' ACTIVE BEFORE INSERT POSITION 0'+
                            ' AS'+
                            ' begin'+
                              ' /* genera un nuevo id */'+
                              ' if (new.maquila_det_id = -1) then'+
                                ' new.maquila_det_id = gen_id(sic_id_maquila_det_entrada,1);'+
                            ' end'';'+
    ' END end');

         //TABLA CLAVES_CAJONES
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''CLAVES_CAJONES'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE CLAVES_CAJONES ('+
                                ' RECEPCION_ID  VARCHAR(30),'+
                                ' MAQUILA_ID      INTEGER,'+
                                ' ARTICULO_ID      INTEGER,'+
                                ' COD_BARRAS      VARCHAR(30),'+
                                ' PESO      DECIMAL(15,2),'+
                                'CAJAS_TARIMA INTEGER,'+
                                 'CAJAS INTEGER'+
                            ' );'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_CLAVES_CAJONES;'';'+
        ' execute statement ''set GENERATOR SIC_ID_CLAVES_CAJONES TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_CLAVES_CAJONES_BI0 FOR CLAVES_CAJONES'+
                            ' ACTIVE BEFORE INSERT POSITION 0'+
                            ' AS'+
                            ' begin'+
                              ' /* genera un nuevo id */'+
                              ' if (new.recepcion_id = -1) then'+
                                ' new.recepcion_id = gen_id(SIC_ID_CLAVES_CAJONES,1);'+
                            ' end'';'+
    ' END end');

     //TABLA SIC_MATERIAL_EMPAQUE
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_MATERIAL_EMPAQUE'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_MATERIAL_EMPAQUE ('+
                              'ID           INTEGER NOT NULL,'+
                              'MAQUILA_ID   INTEGER,'+
                              'ARTICULO_ID  INTEGER,'+
                              'UNIDADES     INTEGER'+
                                ');'';'+
          ' execute statement ''ALTER TABLE SIC_MATERIAL_EMPAQUE ADD CONSTRAINT PK_SIC_MATERIAL_EMPAQUE PRIMARY KEY (ID);'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_MATERIAL_EMPAQUE;'';'+
        ' execute statement ''set GENERATOR SIC_ID_MATERIAL_EMPAQUE TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_MATERIAL_EMPAQUE_BI0 FOR SIC_MATERIAL_EMPAQUE'+
                              ' ACTIVE BEFORE INSERT POSITION 0'+
    ' AS begin /* Genera un nuevo id */ if (new.id = -1) then new.id = gen_id(sic_id_material_empaque,1); end'';'+
    ' END end');

         //TABLA SIC_CHOFERES
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_CHOFERES'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_CHOFERES ('+
                              'ID      INTEGER NOT NULL,'+
                              'NOMBRE  VARCHAR(50),'+
                              'PLACAS  VARCHAR(50),'+
                              'MARCA   VARCHAR(50),'+
                              'MODELO  VARCHAR(50)'+
                               ');'';'+
          ' execute statement ''ALTER TABLE SIC_CHOFERES ADD CONSTRAINT PK_SIC_CHOFERES PRIMARY KEY (ID);'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_CHOFER;'';'+
        ' execute statement ''set GENERATOR SIC_ID_CHOFER TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_CHOFERES_BI0 FOR SIC_CHOFERES'+
                              ' ACTIVE BEFORE INSERT POSITION 0'+
      ' AS begin /* Genera un nuevo id */ if (new.id = -1) then new.id = gen_id(sic_id_chofer,1); end'';'+
    ' END end');

    //TABLA ALMACENES AGREGAR CAMPO SIC_POLIZA
    Conexion.ExecSQL('EXECUTE block as BEGIN if (not exists( select 1 from RDB$RELATION_FIELDS rf'+
                    ' where rf.RDB$RELATION_NAME = ''ALMACENES'' and rf.RDB$FIELD_NAME = ''SIC_POLIZA'')) then'+
                    ' execute statement ''ALTER TABLE ALMACENES ADD SIC_POLIZA INTEGER''; END');

                     //TABLA SIC MAQUILA ARTICULOS TARIMA
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_MAQUILA_ARTICULOS_TARIMA'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_MAQUILA_ARTICULOS_TARIMA ('+
                                ' ID  INTEGER NOT NULL,'+
                                ' MAQUILA_DET_ID      INTEGER,'+
                                ' ARTICULO_ID      INTEGER,'+
                                ' UNIDADES      NUMERIC(15,2),'+
                                ' CLAVE_ARTICULO      VARCHAR(20),'+
                                ' SURTIDAS      INTEGER,'+
                                ' PESO      NUMERIC(15,2)'+
                            ' );'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_MAQUILA_ARTICULOS_TARIMA;'';'+
        ' execute statement ''set GENERATOR SIC_ID_MAQUILA_ARTICULOS_TARIMA TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_MAQUILA_TARIMA_BI0 FOR SIC_MAQUILA_ARTICULOS_TARIMA'+
                            ' ACTIVE BEFORE INSERT POSITION 0'+
                            ' AS'+
                            ' begin'+
                              ' /* genera un nuevo id */'+
                              ' if (new.ID = -1) then'+
                                ' new.ID = gen_id(sic_id_maquila_articulos_tarima,1);'+
                            ' end'';'+
    ' END end');

      //TABLA SIC GASTOS CONCEPTOS
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_GASTOS_CONCEPTOS'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_GASTOS_CONCEPTOS ('+
                              'ID_GASTO      INTEGER NOT NULL,'+
                              'CLAVE_GASTO   VARCHAR(20),'+
                              'NOMBRE_GASTO  VARCHAR(200),'+
                              'COSTO         NUMERIC(15,2)'+
                              ');'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_GASTOS_CONCEPTOS;'';'+
        ' execute statement ''set GENERATOR SIC_ID_GASTOS_CONCEPTOS TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_GASTOS_CONCEPTOS_BI0 FOR SIC_GASTOS_CONCEPTOS'+
                              ' ACTIVE BEFORE INSERT POSITION 0'+
                              ' AS begin /* genera un nuevo id */ if (new.id_gasto = -1) then new.id_gasto ='+
                              ' gen_id(sic_id_gastos_conceptos,1); end'';'+
                               ' END end');

                                     //TABLA SIC MAQUILA GASTOS
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_MAQUILA_GASTOS'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_MAQUILA_GASTOS ('+
                              'ID          INTEGER NOT NULL,'+
                              'MAQUILA_ID  INTEGER,'+
                              'ID_GASTO    INTEGER,'+
                              'COSTO       NUMERIC(15,2)'+
                                 ');'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_MAQUILA_GASTOS;'';'+
        ' execute statement ''set GENERATOR SIC_ID_MAQUILA_GASTOS TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_MAQUILA_GASTOS_BI0 FOR SIC_MAQUILA_GASTOS'+
                              ' ACTIVE BEFORE INSERT POSITION 0'+
                              ' AS begin /* genera un nuevo id */ if (new.id = -1) then new.id ='+
                              ' gen_id(sic_id_maquila_gastos,1); end'';'+
                               ' END end');

                                                           //TABLA SIC_DET_EMBARQUE
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_EMBARQUE'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_EMBARQUE ('+
                              'EMBARQUE_ID       INTEGER NOT NULL,'+
                              'FOLIO             VARCHAR(20),'+
                              'ALMACEN_ID        INTEGER,'+
                              'CLIENTE_ID        INTEGER,'+
                              'FECHA             DATE,'+
                              'CHOFER            VARCHAR(50),'+
                             'PLACAS            VARCHAR(20),'+
                              'POLIZA_DE_SEGURO  VARCHAR(20)'+
                              ');'';'+
                             ' execute statement ''CREATE GENERATOR SIC_ID_EMBARQUE;'';'+
        ' execute statement ''set GENERATOR SIC_ID_EMBARQUE TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_EMBARQUE_BI0 FOR SIC_EMBARQUE'+
                              ' ACTIVE BEFORE INSERT POSITION 0'+
                              ' AS begin /* genera un nuevo id */ if (new.embarque_id = -1) then new.embarque_id ='+
                              ' gen_id(sic_id_embarque,1); end'';'+
                               ' END end');

                                    //TABLA SIC_DET_EMBARQUE
    Conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_DET_EMBARQUE'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_DET_EMBARQUE ('+
                            'EMBARQUE_ID   INTEGER,'+
                            'ETIQUETA      VARCHAR(100),'+
                            'LOCALIZACION  VARCHAR(11),'+
                            'SURTIDAS      INTEGER'+
                          ');'';'+
                ' END end');

                //SE CREAN CONCEPTOS USADOS
                //INVENTARIOS - ENTRADA CAJONES
    if(Conexion.ExecSQLScalar('select nombre from conceptos_in where nombre=''Entrada Cajones''')='') then
    begin
    Conexion.ExecSQL('INSERT INTO CONCEPTOS_IN (CONCEPTO_IN_ID, NOMBRE, NOMBRE_ABREV, NATURALEZA, FOLIO_AUTOM, SIG_FOLIO,OCULTO,TIPO,TIPO_CALCULO) VALUES (-1, ''Entrada Cajones'', ''Entrada Cajones'', ''E'', ''S'', ''000000001'',''N'',''C'',''R'');');
    end;

                    //INVENTARIOS - ENTRADA A MAQUILA
    if(Conexion.ExecSQLScalar('select nombre from conceptos_in where nombre=''Entrada a Maquila''')='') then
    begin
    Conexion.ExecSQL('INSERT INTO CONCEPTOS_IN (CONCEPTO_IN_ID, NOMBRE, NOMBRE_ABREV, NATURALEZA, FOLIO_AUTOM, SIG_FOLIO,OCULTO,TIPO,TIPO_CALCULO) VALUES (-1, ''Entrada a Maquila'', ''Entrada a Maquila'', ''E'', ''S'', ''E00000001'',''N'',''C'',''R'');');
    end;

                        //INVENTARIOS - SALIDA A MAQUILA
    if(Conexion.ExecSQLScalar('select nombre from conceptos_in where nombre=''Salida a Maquila''')='') then
    begin
    Conexion.ExecSQL('INSERT INTO CONCEPTOS_IN (CONCEPTO_IN_ID, NOMBRE, NOMBRE_ABREV, NATURALEZA, FOLIO_AUTOM, SIG_FOLIO,OCULTO,TIPO,TIPO_CALCULO,CENTROS_COSTO) VALUES (-1, ''Salida a Maquila'', ''Salida a Maquila'', ''S'', ''S'', ''S00000001'',''N'',''V'',''C'',''S'');');
    end;

                            //INVENTARIOS - SALIDA DE MATERIAL
    if(Conexion.ExecSQLScalar('select nombre from conceptos_in where nombre=''Salida de Material''')='') then
    begin
    Conexion.ExecSQL('INSERT INTO CONCEPTOS_IN (CONCEPTO_IN_ID, NOMBRE, NOMBRE_ABREV, NATURALEZA, FOLIO_AUTOM, SIG_FOLIO,OCULTO,TIPO,TIPO_CALCULO) VALUES (-1, ''Salida de Material'', ''Salida de Material'', ''S'', ''S'', ''000000001'',''N'',''V'',''C'');');
    end;

                               //SE CREA EL ALMACEN INTERMEDIARIO (MAQUILA)
    if(Conexion.ExecSQLScalar('select nombre from almacenes where nombre=''Maquila''')='') then
    begin
    Conexion.ExecSQL('INSERT INTO ALMACENES (ALMACEN_ID,NOMBRE,NOMBRE_ABREV,OCULTO) VALUES (-1, ''Maquila'', ''Maquila'', ''N'');');
    end;

       if(Conexion.ExecSQLScalar('select nombre from almacenes where nombre=''Almacen Cajones''')='') then
    begin
    Conexion.ExecSQL('INSERT INTO ALMACENES (ALMACEN_ID,NOMBRE,NOMBRE_ABREV,OCULTO) VALUES (-1, ''Almacen Cajones'', ''Almacen Cajones'', ''N'');');
    end;
end;



end.
