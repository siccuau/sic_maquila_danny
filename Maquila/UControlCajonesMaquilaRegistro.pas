unit UControlCajonesMaquilaRegistro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls,UControlCajonesMaquilaDetalleEntrada,UControlCajonesMaquilaMaterial, UControlCajonesMaquilaDetalleSalida,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,System.Character,
  FireDAC.Comp.DataSet, Vcl.DBCtrls, FireDAC.VCLUI.Wait,
  UControlCajonesMaquilaEmbarque,UcontrolCajonesGastosExtras,frxClass, frxDBSet,frxBarcode,strutils;

type
  TControlCajonesMaquilaRegistro = class(TForm)
    txtFolioMaquila: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    lblStatus: TLabel;
    btnLiberar: TButton;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    txtNumCajones: TEdit;
    txtTotalKGMaq: TEdit;
    txtTotalKG: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    txtTarimasSalida: TEdit;
    txtTotalCajas: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    txtPesoPromporCaja: TEdit;
    btnDetalleSalida: TButton;
    btnDetalleEntrada: TButton;
    Label13: TLabel;
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    Button1: TButton;
    btnGuardar: TButton;
    QueryProveedores: TFDQuery;
    DSProveedores: TDataSource;
    cbProveedores: TDBLookupComboBox;
    dtpFecha: TDateTimePicker;
    QueryAlmacenEntrada: TFDQuery;
    DSAlmacenEntrada: TDataSource;
    QueryAlmacenSalida: TFDQuery;
    DSAlmacenSalida: TDataSource;
    cbAlmacenesSalida: TDBLookupComboBox;
    cbAlmacenesEntrada: TDBLookupComboBox;
    btnCrearTraspaso: TButton;
    DSLlenadoEntrada: TDataSource;
    qryConsulta: TFDQuery;
    Button2: TButton;
    Button3: TButton;
    qryLlenadoGastos: TFDQuery;
    dsLlenadoGastos: TDataSource;
    QueryProductos: TFDQuery;
    QueryEtiquetas: TFDQuery;
    DSEtiquetas: TfrxDBDataset;
    Etiquetas: TfrxReport;
    frxBarCodeObject1: TfrxBarCodeObject;
    qryLlenadoEntrada: TFDQuery;
    QueryProductos1: TFDQuery;
    DSEtiquetas1: TfrxDBDataset;
    procedure btnDetalleEntradaClick(Sender: TObject);
    procedure btnDetalleSalidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnLiberarClick(Sender: TObject);
    procedure btnCrearTraspasoClick(Sender: TObject);
    procedure ActualizarEntradaSalida;
    procedure checar_liberacion;
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    operacion, maquila_id : integer;
  end;

var
  ControlCajonesMaquilaRegistro: TControlCajonesMaquilaRegistro;

implementation

{$R *.dfm}
function GetSerie(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsLetter(folio[i]))
    then Result := Result+folio[i]
    else exit;
  end;
end;

function GetNumFolio(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsNumber(folio[i]))
    then Result := Result+folio[i];
  end;
end;

procedure TControlCajonesMaquilaRegistro.btnCrearTraspasoClick(Sender: TObject);
var
query_str,folio_nuevo,serie,fecha,folio_str,clave_articulo:string;
consecutivo,almacen_id,documento_id,concepto_id,articulo_id,i:integer;
almacen_maquila:integer;
precio_unitario,precio_total_neto:double;
unidades:double;
begin

  //****************************************DA DE ALTA EL ENCABEZADO***********************************
   if Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''SALIDA A MAQUILA''')<>'' then
    begin
      serie :=GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''SALIDA A MAQUILA'''));
      consecutivo :=strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''SALIDA A MAQUILA''')));
      concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where upper(nombre) = ''SALIDA A MAQUILA''');
    end
        else
    begin
      ShowMessage('Falta concepto ''Salida a Maquila'' en Inventarios.');
      Conexion.Rollback;
    end;
      //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
      folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
      folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
      Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

  //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
  almacen_id := cbAlmacenesEntrada.KeyValue;
  //    moneda_id := 1;
  fecha := FormatDateTime('mm/dd/yyyy', Now);
 //****************************************DA DE ALTA EL ENCABEZADO***********************************
  if Conexion.ExecSQLScalar('select maquila_id from claves_cajones where maquila_id='+inttostr(maquila_id)+'')>0 then
  begin
      //SALIDA DEL ALMACEN ORIGEN
      //MSP 2019
      { query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,descripcion,'+
      ' sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_id)+','+inttostr(concepto_id)+','+
      ' '''+folio_str+''',''S'','''+fecha+''',''Salida de la maquila: '+txtFolioMaquila.Text+''','+
      ' ''IN'','+
      ' '+inttostr(cbProveedores.KeyValue)+') returning docto_in_id';}

      //MSP 2020
             query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,sucursal_id,folio,naturaleza_concepto,fecha,descripcion,'+
      ' sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_id)+','+inttostr(concepto_id)+','+
      ' '+inttostr(Conexion.ExecSQLScalar('select sucursal_id from sucursales where upper(nombre)=''MATRIZ'''))+','''+folio_str+''',''S'','''+fecha+''',''Salida de la maquila: '+txtFolioMaquila.Text+''','+
      ' ''IN'','+
      ' '+inttostr(cbProveedores.KeyValue)+') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);
      //A�ADIR SALIDA ID A TABLA SIC_MAQUILA
     // Conexion.ExecSQL('update sic_maquila set salida_id=:s where maquila_id=:m',[inttostr(documento_id),inttostr(maquila_id)]);
     //A�ADIR MODULO_ID A TABLA CLAVES_CAJONES
       // Conexion.ExecSQL('update claves_cajones set modulo_id=:m where maquila_id=:m',[inttostr(documento_id),inttostr(maquila_id)]);
    //*******************************DETALLADO DEL DOCUMENTO*********************************************

    qryConsulta.SQL.Text :='select * from claves_cajones where maquila_id=:maquilaid';
    qryConsulta.ParamByName('maquilaid').Value:=maquila_id;
    qryConsulta.Open;
    qryConsulta.First;
    while not qryConsulta.Eof do
    begin
      articulo_id := qryConsulta.FieldByName('articulo_id').Value;
      clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
      //validar los campos vacios

      unidades := qryConsulta.FieldByName('peso').Value;
      precio_unitario := Conexion.ExecSQLScalar('Select precio_unitario FROM DOCTOS_CM_DET where docto_cm_id=:d1',[qryConsulta.FieldByName('RECEPCION_ID').Value]);
      precio_total_neto := precio_unitario*unidades;

      query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,clave_articulo,'+
      ' articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo,sic_num_cajones) values(-1,'+
      ' '+inttostr(documento_id)+','+IntToStr(almacen_id)+','+IntToStr(concepto_id)+','+
      ' '''+clave_articulo+''','+inttostr(articulo_id)+',''S'','+
      ' '+FloatToStr(unidades)+
      ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+',''C'','+FloatToStr(unidades)+') ';

      Conexion.ExecSQL(query_str);
      Conexion.Commit;
      //A�ADIR MODULO_ID A TABLA CLAVES_CAJONES
        Conexion.ExecSQL('insert into claves_cajones values(null,:mid,:artid,:cod,:p,null,null,:m,:mo,:tip)',[inttostr(maquila_id),inttostr(articulo_id),qryConsulta.FieldByName('COD_BARRAS').Value,floattostr(unidades),inttostr(documento_id),'SM','S']);
      Conexion.Commit;
      qryConsulta.Next;
    end;
          Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);
          Conexion.Commit;

      //****************************************DA DE ALTA EL ENCABEZADO***********************************
         if Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''ENTRADA A MAQUILA''')<>'' then
    begin
      serie :=GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''ENTRADA A MAQUILA'''));
      consecutivo :=strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''ENTRADA A MAQUILA''')));
      concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where upper(nombre) = ''ENTRADA A MAQUILA''');
    end
         else
    begin
      ShowMessage('Falta concepto ''Entrada a Maquila'' en Inventarios.');
      Conexion.Rollback;
    end;
      //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
      folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
      folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
      Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

  //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
  fecha := FormatDateTime('mm/dd/yyyy', Now);

  almacen_maquila:=Conexion.ExecSQLScalar('select almacen_id from almacenes where nombre=''Maquila''');
 //****************************************DA DE ALTA EL ENCABEZADO***********************************
    //ENTRADA AL ALMACEN DE MAQUILA
    //MSP 2019
       {query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,descripcion,'+
      ' sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_maquila)+','+inttostr(concepto_id)+','+
      ' '''+folio_str+''',''E'','''+fecha+''',''Entrada al almacen maquila'','+
      ' ''IN'','+
      ' '+inttostr(cbProveedores.KeyValue)+') returning docto_in_id';}
      //MSP 2020
       query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,sucursal_id,folio,naturaleza_concepto,fecha,descripcion,'+
      ' sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_maquila)+','+inttostr(concepto_id)+','+
      ' '+inttostr(Conexion.ExecSQLScalar('select sucursal_id from sucursales where upper(nombre)=''MATRIZ'''))+','''+folio_str+''',''E'','''+fecha+''',''Entrada al almacen maquila'','+
      ' ''IN'','+
      ' '+inttostr(cbProveedores.KeyValue)+') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);
    //A�ADIR ENTADA ID A TABLA SIC_MAQUILA
      Conexion.ExecSQL('update sic_maquila set entrada_id=:e where maquila_id=:m',[inttostr(documento_id),inttostr(maquila_id)]);

    //*******************************DETALLADO DEL DOCUMENTO*********************************************

    qryConsulta.SQL.Text :='select * from claves_cajones where maquila_id=:maquilaid and recepcion_id is not null';
    qryConsulta.ParamByName('maquilaid').Value:=maquila_id;
    qryConsulta.Open;
    qryConsulta.First;
    while not qryConsulta.Eof do
    begin
      articulo_id := qryConsulta.FieldByName('articulo_id').Value;
      clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
      //validar los campos vacios

      unidades := qryConsulta.FieldByName('peso').Value;
      //precio_unitario := Conexion.ExecSQLScalar('Select precio_unitario FROM GET_PRECIO_ART(:articulo_id, (SELECT precio_empresa_id FROM precios_empresa WHERE UPPER(nombre)=''PRECIO DE LISTA''),1, CURRENT_DATE,0)',[articulo_id]);
      precio_unitario:=Conexion.ExecSQLScalar('select costo_ultima_compra from get_ultcom_art(:articulo_id)',[articulo_id]);
      precio_total_neto := precio_unitario*unidades;

      query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,clave_articulo,'+
      ' articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo,sic_num_cajones) values(-1,'+
      ' '+inttostr(documento_id)+','+IntToStr(almacen_maquila)+','+IntToStr(concepto_id)+','+
      ' '''+clave_articulo+''','+inttostr(articulo_id)+',''E'','+
      ' '+FloatToStr(unidades)+
      ' ,'+FloatToStr(precio_unitario)+','+FloatToStr(precio_total_neto)+',''C'','+FloatToStr(unidades)+') ';

      Conexion.ExecSQL(query_str);
            Conexion.Commit;
       //A�ADIR MODULO_ID A TABLA CLAVES_CAJONES
        Conexion.ExecSQL('insert into claves_cajones values(null,:mid,:artid,:cod,:p,null,null,:m,:mo,:tip)',[inttostr(maquila_id),inttostr(articulo_id),qryConsulta.FieldByName('COD_BARRAS').Value,floattostr(unidades),inttostr(documento_id),'EM','E']);
      Conexion.Commit;
      qryConsulta.Next;
    end;
          Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);
          Conexion.Commit;
    Conexion.ExecSQL('update sic_maquila set traspaso=''S'' where maquila_id=:maquila',[maquila_id]);
    ShowMessage('Traspaso creado correctamente.');
    Conexion.Commit;
    btnCrearTraspaso.Enabled:=false;
    checar_liberacion;
    //ACTUALIZAR DATOS DE ENTADA
          txtNumCajones.Text:=Conexion.ExecSQLScalar('select count(cod_barras) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
      txtTotalKG.Text:=Conexion.ExecSQLScalar('select sum(peso) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
      txtTotalKGMaq.Text:= Conexion.ExecSQLScalar('select sum(peso) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
  end
  else
  begin
    ShowMessage('No se puede crear traspaso sin detalles de entrada.');
    Conexion.Rollback;
  end;
end;

procedure TControlCajonesMaquilaRegistro.checar_liberacion;
begin
 if (Conexion.ExecSQLScalar('select traspaso from sic_maquila where maquila_id=:mid',[maquila_id])='S') then
 begin
   if (Conexion.ExecSQLScalar('select count(maquila_id) from sic_maquila_det_salida where maquila_id=:mid',[maquila_id])>0) then
     begin
     btnliberar.Enabled:=true;
     end;
 end;
end;

procedure TControlCajonesMaquilaRegistro.btnDetalleEntradaClick(
  Sender: TObject);
var
  FormEntrada : TControlCajonesMaquilaDetalleEntrada;
  i,docto_id,viaje,consec:integer;
  query_et,folio_viaje:string;
begin
  if (Conexion.ExecSQLScalar('select traspaso from sic_maquila where maquila_id=:mid',[maquila_id])='S') then
  begin
    FormEntrada := TControlCajonesMaquilaDetalleEntrada.create(nil);
    FormEntrada.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormEntrada.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormEntrada.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
    FormEntrada.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormEntrada.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
    FormEntrada.maquila_id := maquila_id;
    FormEntrada.txtFolioMaquila.Text:=txtFolioMaquila.Text;
    FormEntrada.txtFolioMaquila.Enabled:=false;
    FormEntrada.txtProveedor.Text:=cbProveedores.Text;
    FormEntrada.txtProveedor.Enabled:=false;
    FormEntrada.txtFecha.Text:=DateToStr(dtpFecha.Date);
    FormEntrada.txtFecha.Enabled:=false;
    FormEntrada.txtAlmacen.Text := cbAlmacenesEntrada.Text;
    FormEntrada.txtAlmacen.Enabled:=false;
    FormEntrada.txtMaquila.Text:=inttostr(maquila_id);
    FormEntrada.txtMaquila.Enabled:=false;
    FormEntrada.StringGrid1.enabled:=false;
    FormEntrada.btnGuardar.Caption:='Salir';

    docto_id:=Conexion.ExecSQLScalar('select recepcion_id from claves_cajones where maquila_id=:mid',[maquila_id]);
    consec := 1;
    query_et := '';
    folio_viaje := Conexion.ExecSQLScalar('select folio from doctos_cm where docto_cm_id=:did',[docto_id]);
    viaje := strtoint(GetNumFolio(folio_viaje));
    QueryProductos.sql.text := ' select  a.nombre,'+
                              ' (select clave_articulo from get_clave_art(a.articulo_id))as clave,'+
                              ' sum(dcd.sic_num_cajones) as cajones,(select sum(unidades)/sum(sic_num_cajones) from doctos_cm_det where docto_cm_id = 24828) as peso'+
                        ' from doctos_cm_det dcd join'+
                        ' articulos a on a.articulo_id = dcd.articulo_id'+
                        ' where dcd.docto_cm_id = '+IntToStr(docto_id)+
                        ' group by a.nombre,a.articulo_id,dcd.unidades';
  QueryProductos.Open;
  QueryProductos.First;
  while not QueryProductos.Eof do
  begin
    if query_et<>''
    then query_et := query_et +' union ';

    query_et := query_et + ' select a.*,'''+QueryProductos.FieldByName('nombre').AsString+''' as articulo,'+
                            ''''+QueryProductos.FieldByname('clave').asstring+'''as clave,'+
                            ' '+IntToStr(viaje)+' as viaje,'+
                            ''''+QueryProductos.FieldByname('peso').asstring+'''as peso from'+
                            ' (with recursive n as ('+
                                  ' select '+inttostr(consec)+' as n'+
                                  ' from rdb$database'+
                                  ' union all'+
                                  ' select n.n + 1'+
                                  ' from n'+
                                  ' where n < '+IntToStr(consec+QueryProductos.Fieldbyname('cajones').AsInteger-1)+''+
                                 ' )'+
                            ' select n.n'+
                            ' from n) a';
    consec := consec+QueryProductos.Fieldbyname('cajones').AsInteger;
    QueryProductos.Next;

  end;

    qryllenadoEntrada.SQL.Clear;
    qryLlenadoEntrada.SQL.Text:='select cod_barras,(select nombre from articulos where articulo_id=cc.articulo_id),peso'+
                                ' from claves_cajones cc where cc.maquila_id=:maquila_id and tipo=''E''';
    qryLlenadoEntrada.ParamByName('maquila_id').Value:=maquila_id;
    qryLlenadoEntrada.Open;
    qryLlenadoEntrada.First;
    i:=1;

    while not qryLlenadoEntrada.Eof do
    begin
      FormEntrada.StringGrid1.Cells[0,i]:= qryLlenadoEntrada.FieldByName('cod_barras').AsString;
      FormEntrada.StringGrid1.Cells[1,i]:= qryLlenadoEntrada.FieldByName('nombre').AsString;
      FormEntrada.StringGrid1.Cells[2,i]:= qryLlenadoEntrada.FieldByName('peso').AsString;
    //  FormEntrada.StringGrid1.Cells[3,i]:= qryLlenadoEntrada.FieldByName('viaje').AsString;
      FormEntrada.StringGrid1.RowCount:=FormEntrada.StringGrid1.RowCount+1;
      qryLlenadoEntrada.Next;
      i:=i+1;
    end;

    FormEntrada.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
    FormEntrada.ShowModal;
    //LLENADO DE CAMPOS ENRADA CON CONSULTA
    if Conexion.ExecSQLScalar('select count(cod_barras) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''')<> 0 then
    begin
      txtNumCajones.Text:=Conexion.ExecSQLScalar('select count(cod_barras) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
      txtTotalKG.Text:=Conexion.ExecSQLScalar('select sum(peso) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
      txtTotalKGMaq.Text:= Conexion.ExecSQLScalar('select sum(peso) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
    end;
  end
  else
  begin
    FormEntrada := TControlCajonesMaquilaDetalleEntrada.create(nil);
    FormEntrada.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormEntrada.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormEntrada.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
    FormEntrada.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormEntrada.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
    FormEntrada.maquila_id := maquila_id;
    FormEntrada.txtFolioMaquila.Text:=txtFolioMaquila.Text;
    FormEntrada.txtProveedor.Text:=cbProveedores.Text;
    FormEntrada.txtFecha.Text:=DateToStr(dtpFecha.Date);
    FormEntrada.txtAlmacen.Text := cbAlmacenesEntrada.Text;
    FormEntrada.txtMaquila.Text:=inttostr(maquila_id);

    FormEntrada.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
    FormEntrada.ShowModal;
    //LLENADO DE CAMPOS ENTRADA CON CONSULTA
    if Conexion.ExecSQLScalar('select count(cod_barras) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''')<> 0 then
    begin
      txtNumCajones.Text:=Conexion.ExecSQLScalar('select count(cod_barras) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
      txtTotalKG.Text:=Conexion.ExecSQLScalar('select sum(peso) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
      txtTotalKGMaq.Text:= Conexion.ExecSQLScalar('select sum(peso) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
    end;
    btnCrearTraspaso.Click;
  end;
end;

procedure TControlCajonesMaquilaRegistro.btnDetalleSalidaClick(Sender: TObject);
var
  FormSalida : TControlCajonesMaquilaDetalleSalida;
  total_cajas,tarimas:integer;

begin
  FormSalida := TControlCajonesMaquilaDetalleSalida.create(nil);
  FormSalida.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormSalida.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormSalida.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormSalida.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormSalida.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormSalida.txtFolioMaquila.Text:=txtFolioMaquila.Text;
  FormSalida.txtProveedor.Text:=cbProveedores.Text;
  FormSalida.txtFecha.Text:=DateToStr(dtpFecha.Date);
  FormSalida.txtAlmacen.Text := cbAlmacenesSalida.Text;
  FormSalida.txtCajas.Text := txtTotalCajas.Text;


  FormSalida.num_tarimas := conexion.ExecSQLScalar('select count(maquila_id) from sic_maquila_det_salida where maquila_id=:mid',[maquila_id]);
  FormSalida.maquila_id := maquila_id;
  FormSalida.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormSalida.ShowModal;
  ActualizarEntradaSalida;
  checar_liberacion;
end;

procedure TControlCajonesMaquilaRegistro.btnGuardarClick(Sender: TObject);
var
  fecha_str, folio_str,num_tarima,qs:string;
  i,tarimas_salida:integer;
begin
  fecha_str := FormatDateTime('mm/dd/yyyy', dtpFecha.Date);

  if operacion = 0 then
  begin
    // DA DE ALTA LA MAQUILA NUEVA (ENCABEZADO Y LAS TARIMAS ESTIMADAS EN DETALLE DE SALIDA)***************

    qs:= 'insert into sic_maquila values(-1,'+vartostr(cbProveedores.KeyValue)+','''+fecha_str+''','+
          'null,''A'',current_timestamp,null,'+'0'+','+vartostr(cbAlmacenesEntrada.KeyValue)+','+vartostr(cbAlmacenesSalida.KeyValue)+',''N'',null,null,null)'+
                      ' returning maquila_id';

    maquila_id := Conexion.ExecSQLScalar(qs);

    folio_str := Format('%.*d',[6,(maquila_id)]);
    Conexion.ExecSQL('update sic_maquila set folio = :folio where maquila_id = :mid',[folio_str,maquila_id]);
    btnDetalleSalida.Enabled := True;
    btnDetalleEntrada.Enabled := True;
    txtFolioMaquila.Text := folio_str;
    operacion := 1;
    ShowMessage('Maquila creada correctamente.');
    btnDetalleEntrada.Enabled:=true;
    btnDetalleSalida.Enabled:=true;
    //ActualizarEntradaSalida;
  end
  else
  begin
    //EDITA LA MAQUILA CORRESPONDIENTE
    Conexion.ExecSQL('update sic_maquila set proveedor_id=:proveedor,almacen_entrada_id=:almacenentrada,almacen_salida_id=:almacensalida where maquila_id=:mid',[cbProveedores.KeyValue,cbAlmacenesEntrada.KeyValue,cbAlmacenesSalida.KeyValue,maquila_id]);
    showmessage('La maquila se edito correctamente.');
    self.Close;
  end;
end;


procedure TControlCajonesMaquilaRegistro.ActualizarEntradaSalida;
var
  traspaso : string;
begin
  //
  //LLENADO DE CAMPOS ENRADA CON CONSULTA
  txtNumCajones.Text:=Conexion.ExecSQLScalar('select count(cod_barras) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
  txtTotalKG.Text:=Conexion.ExecSQLScalar('select sum(peso) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');
  txtTotalKGMaq.Text:= Conexion.ExecSQLScalar('select sum(peso) from claves_cajones where maquila_id ='+inttostr(maquila_id)+' and tipo=''E''');

  //LLENADO DE CAMPOS SALIDA CON CONSULTA
  txtTarimasSalida.Text:=Conexion.ExecSQLScalar('select count(*) from sic_maquila_det_salida where maquila_id ='+inttostr(maquila_id)+'');
  txtPesoPromporCaja.Text:=Conexion.ExecSQLScalar('select coalesce((sum(peso)/sum(cajas)),0) from sic_maquila_det_salida where maquila_id ='+inttostr(maquila_id)+'');
  txtTotalCajas.Text:= Conexion.ExecSQLScalar('select coalesce(sum(cajas),0) from sic_maquila_det_salida where maquila_id ='+inttostr(maquila_id)+'');

  //BLOQUEAR BOTON TRASPASO
  traspaso := Conexion.ExecSQLScalar('select traspaso from sic_maquila where maquila_id ='+inttostr(maquila_id)+'');
 { if traspaso='S' then
  begin
    btnCrearTraspaso.Enabled:=false;
  end
  else
  begin
    btnCrearTraspaso.Enabled:=true;
  end; }
end;

procedure TControlCajonesMaquilaRegistro.btnLiberarClick(Sender: TObject);
var
  FormMaterial : TControlCajonesMaquilaMaterial;
  status:string;
begin
  FormMaterial := TControlCajonesMaquilaMaterial.create(nil);
  FormMaterial.maquila_id := maquila_id;
  FormMaterial.folio_maquila:=txtFolioMaquila.Text;
  FormMaterial.almacen_salida_id := strtoint(cbAlmacenesSalida.KeyValue);
  //FormMaterial.almacen_entrada_id := strtoint(cbAlmacenesEntrada.KeyValue);
  FormMaterial.almacen_entrada_id := Conexion.ExecSQLScalar('select almacen_id from almacenes where upper(nombre)=''MAQUILA''');
  FormMaterial.total_kg:=strtofloat(txtTotalKG.Text);
  FormMaterial.total_cajas:=strtofloat(txtTotalCajas.Text);
  FormMaterial.proveedor_id:=strtoint(cbProveedores.KeyValue);
  FormMaterial.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormMaterial.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormMaterial.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormMaterial.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormMaterial.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormMaterial.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormMaterial.ShowModal;
  status := Conexion.ExecSQLScalar('select estatus from sic_maquila where maquila_id = '''+inttostr(maquila_id)+'''');
  if status = 'C' then
  begin
    lblStatus.Caption:='Cerrada';
    btnLiberar.Enabled:=false;
  end
  else
  begin
    lblStatus.Caption:='Abierta';
  end;
  //CREAR TRASPASO
 { if FormMaterial.liberar=1 then
  begin
    btnCrearTraspaso.Click;
  end; }
end;


procedure TControlCajonesMaquilaRegistro.Button1Click(Sender: TObject);
var
  Memo:TfrxMemoView;
  folio_str,page_str,folio_viaje:string;
  folio_int,page,consec,viaje,i:integer;
  ds: TfrxDataSet;
  md: TfrxmasterData;
  query_et:string;
begin
 consec := 1;
  query_et := '';

    folio_viaje := Conexion.ExecSQLScalar('select folio from sic_maquila where maquila_id=:mid',[maquila_id]);
    viaje := strtoint(GetNumFolio(folio_viaje));


    QueryProductos1.sql.text := 'SELECT sma.maquila_det_id, NUM_TARIMA,sma.surtidas as cajas,a.nombre as ARTICULO,ca.clave_articulo as CLAVE'+
                              ' from sic_maquila_det_salida smd join sic_maquila_articulos_tarima sma'+
                              ' on smd.maquila_det_id=sma.maquila_det_id '+
                              ' join articulos a on a.articulo_id=sma.articulo_id'+
                              ' join claves_articulos ca on a.articulo_id=ca.articulo_id'+
                              ' where maquila_id= '+IntToStr(maquila_id)+
                               ' order by num_tarima';
    QueryProductos1.Open;

        QueryProductos.sql.text := 'SELECT max(sma.maquila_det_id) as maquila_det_id, NUM_TARIMA,max(smd.cajas) as cajas,max(a.nombre) as nombre,max(ca.clave_articulo) as clave_articulo'+
      ' from sic_maquila_det_salida smd join sic_maquila_articulos_tarima sma'+
      ' on smd.maquila_det_id=sma.maquila_det_id '+
      ' join articulos a on a.articulo_id=sma.articulo_id'+
      ' join claves_articulos ca on a.articulo_id=ca.articulo_id'+
      ' where maquila_id= '+IntToStr(maquila_id)+
      ' group by num_tarima';
  QueryProductos.Open;
  QueryProductos.First;
  while not QueryProductos.Eof do
  begin
    if query_et<>''
    then query_et := query_et +' union ';

    query_et := query_et + ' select a.*,'''+QueryProductos.FieldByName('nombre').AsString+''' as articulo,'+
                            ''''+QueryProductos.FieldByname('clave_articulo').asstring+'''as clave,'+
                            ''''+QueryProductos.FieldByname('cajas').asstring+'''as cajas,'+
                            ' '+IntToStr(viaje)+' as viaje from'+
                            ' (with recursive n as ('+
                                  ' select '+inttostr(consec)+' as n'+
                                  ' from rdb$database'+
                                  ' union all'+
                                  ' select n.n+1'+
                                  ' from n'+
                                  ' where n <  '+IntToStr(consec-1)+''+
                                 ' )'+
                            ' select n.n'+
                            ' from n) a';
    consec := consec+1;
    QueryProductos.Next;

  end;
  QueryEtiquetas.sql.Text := query_et;

  QueryEtiquetas.open;

  Etiquetas.PrepareReport;
  Memo := Etiquetas.FindObject('txtFecha') as TfrxMemoView;
  Memo.Text := formatdatetime('dddd, dd - mmmm - yyyy',dtpFecha.Date);
  Memo := Etiquetas.FindObject('txtAlmacen') as TfrxMemoView;
  Memo.Text := cbAlmacenesSalida.Text;
  Memo := Etiquetas.FindObject('txtProductor') as TfrxMemoView;
  Memo.Text := cbProveedores.Text;
  Memo := Etiquetas.FindObject('txtProductor2') as TfrxMemoView;
  Memo.Text := cbProveedores.Text;
 // Memo := Etiquetas.FindObject('txtPromedio') as TfrxMemoView;
 // Memo.Text := FormatFloat('######.00',(StrToFloat(txtPesoPromporCaja.Text)))+' KG';
 // Memo := Etiquetas.FindObject('txtPromedio2') as TfrxMemoView;
 // Memo.Text := FormatFloat('######.00',(StrToFloat(txtPesoPromporCaja.Text)))+' KG';
  Memo := Etiquetas.FindObject('txtProductor3') as TfrxMemoView;
  Memo.Text := cbProveedores.Text;
  Memo := Etiquetas.FindObject('txtAlmacen2') as TfrxMemoView;
  Memo.Text := cbAlmacenesSalida.Text;

  Etiquetas.ShowReport(true);

end;

procedure TControlCajonesMaquilaRegistro.Button2Click(Sender: TObject);
var
  FormEmbarque : TControlCajonesMaquilaEmbarque;
begin
  if btnLiberar.Enabled=true then
  begin
    FormEmbarque := TControlCajonesMaquilaEmbarque.create(nil);
    FormEmbarque.maquila_id := maquila_id;
    FormEmbarque.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormEmbarque.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormEmbarque.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
    FormEmbarque.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormEmbarque.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
    FormEmbarque.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
    FormEmbarque.ShowModal;
  end
  else
  begin
    FormEmbarque := TControlCajonesMaquilaEmbarque.create(nil);
    FormEmbarque.maquila_id := maquila_id;
    FormEmbarque.gridTarima.Enabled:=false;
    FormEmbarque.btnGuardar.Caption:='Salir';
    FormEmbarque.DBGrid1.OnDblClick:=nil;
    FormEmbarque.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormEmbarque.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormEmbarque.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
    FormEmbarque.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormEmbarque.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
    FormEmbarque.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
    FormEmbarque.ShowModal;
  end;
end;

procedure TControlCajonesMaquilaRegistro.Button3Click(Sender: TObject);
var
  FormGastos : TControlCajonesGastosExtra;
  maquila_gastos,i:integer;
begin
maquila_gastos:=Conexion.ExecSQLScalar('select count(maquila_id) from sic_maquila_gastos where maquila_id=:mid',[maquila_id]);
  if maquila_gastos>0 then
    begin
         FormGastos := TControlCajonesGastosExtra.create(nil);
          FormGastos.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
          FormGastos.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
          FormGastos.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
          FormGastos.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
          FormGastos.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
          FormGastos.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
          FormGastos.maquila_id:=maquila_id;
          FormGastos.btnGuardar.Caption:='Salir';
      qryllenadoGastos.SQL.Clear;
      qryllenadoGastos.SQL.Text:='select smg.id_gasto,sgc.clave_gasto,sgc.nombre_gasto,smg.costo'+
                        ' from sic_gastos_conceptos sgc join sic_maquila_gastos smg'+
                        ' on sgc.id_gasto=smg.id_gasto'+
                         ' where smg.maquila_id=:maquila';
      qryllenadoGastos.ParamByName('maquila').Value:=maquila_id;
      qryllenadoGastos.Open;
      qryllenadoGastos.First;
      i:=1;

    while not qryllenadoGastos.Eof do
        begin
          FormGastos.gridGastos.Cells[0,i]:= qryllenadoGastos.FieldByName('clave_gasto').AsString;
          FormGastos.gridGastos.Cells[1,i]:= qryllenadoGastos.FieldByName('nombre_gasto').AsString;
          FormGastos.gridGastos.Cells[2,i]:= qryllenadoGastos.FieldByName('costo').AsString;
          FormGastos.gridGastos.Cells[3,i]:= qryllenadoGastos.FieldByName('id_gasto').AsString;
          FormGastos.gridGastos.RowCount:=FormGastos.gridGastos.RowCount+1;
          qryllenadoGastos.Next;
          i:=i+1;
        end;
        FormGastos.ShowModal;
    end
        else
          begin
          FormGastos := TControlCajonesGastosExtra.create(nil);
          FormGastos.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
          FormGastos.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
          FormGastos.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
          FormGastos.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
          FormGastos.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
          FormGastos.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
          FormGastos.maquila_id:=maquila_id;
          FormGastos.ShowModal;
          end;
end;

procedure TControlCajonesMaquilaRegistro.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if Conexion.ExecSQLScalar('select count(maquila_id) from claves_cajones where maquila_id=:mid',[maquila_id])>0 then
    begin
    Self.Close;
    end
    else
    begin
    ShowMessage('No se puede guardar maquila sin articulos de entrada.');
    Conexion.ExecSQL('delete from sic_maquila where maquila_id=:mid',[maquila_id]);
    Self.Close;
    end;
end;

procedure TControlCajonesMaquilaRegistro.FormShow(Sender: TObject);
var
  status : string;
begin
if Conexion.ExecSQLScalar('select count(maquila_id) from sic_maquila_det_salida where maquila_id=:mid',[maquila_id])=0 then
   begin
   Button1.Enabled:=False;
   end;
  dtpFecha.Date := Now;
  QueryProveedores.open;
  cbProveedores.KeyValue := Conexion.ExecSQLScalar('select first 1 proveedor_id from proveedores order by nombre');
  QueryAlmacenEntrada.open;
  QueryAlmacenSalida.open;
  if maquila_id=0 then
  begin
  cbAlmacenesSalida.KeyValue := Conexion.ExecSQLScalar('select first 1 almacen_id from almacenes order by nombre');
  cbAlmacenesEntrada.KeyValue := Conexion.ExecSQLScalar('select first 1 almacen_id from almacenes order by nombre');
  end;
  status := Conexion.ExecSQLScalar('select estatus from sic_maquila where folio = '''+txtFolioMaquila.Text+'''');

  if status = 'C' then
  begin
    lblStatus.Caption := 'Cerrada';
  end
  else
  begin
   lblStatus.Caption := 'Abierta';
  end;

  if maquila_id <> 0 then
     begin
     ActualizarEntradaSalida;
     end;

end;

end.
