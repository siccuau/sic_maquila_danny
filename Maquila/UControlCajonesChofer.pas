unit UControlCajonesChofer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.StdCtrls, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TControlCajonesChofer = class(TForm)
    Conexion: TFDConnection;
    Label1: TLabel;
    txtNombre: TEdit;
    Label2: TLabel;
    txtPlacas: TEdit;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    txtMarca: TEdit;
    txtModelo: TEdit;
    btnGuardar: TButton;
    btnCancelar: TButton;
    qryDatos: TFDQuery;
    qryDatosID: TIntegerField;
    qryDatosNOMBRE: TStringField;
    qryDatosPLACAS: TStringField;
    qryDatosMARCA: TStringField;
    qryDatosMODELO: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    chofer_id:integer;
  end;

var
  ControlCajonesChofer: TControlCajonesChofer;

implementation

{$R *.dfm}

procedure TControlCajonesChofer.btnCancelarClick(Sender: TObject);
begin
self.Close;
end;

procedure TControlCajonesChofer.btnGuardarClick(Sender: TObject);
begin
 if chofer_id < 1 then
  begin
    Conexion.ExecSQL('insert into sic_choferes values(-1,:nombre,:placas,:marca,:modelo)',[txtNombre.Text,txtPlacas.Text,txtMarca.Text,txtModelo.Text]);
  end
  else
  begin
    Conexion.ExecSQL('update sic_choferes set nombre=:nombre,placas=:placas,marca=:marca,modelo=:modelo where id=:id',[txtNombre.Text,txtPlacas.Text,txtMarca.Text,txtModelo.Text,inttostr(chofer_id)]);
  end;
  Conexion.Commit;
    ModalResult := mrOk;
end;

procedure TControlCajonesChofer.FormShow(Sender: TObject);
begin
qryDatos.SQL.Text:='select * from sic_choferes where id=:cid';
qryDatos.ParamByName('cid').Value:=inttostr(chofer_id);
qryDatos.Open();
txtNombre.Text:=qryDatosNOMBRE.Value;
txtPlacas.Text:=qryDatosPLACAS.Value;
txtMarca.Text:=qryDatosMARCA.Value;
txtModelo.Text:=qryDatosMODELO.Value;
end;

end.
