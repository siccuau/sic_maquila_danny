unit UControlCajonesGastosExtras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client,UBusquedaGastos, Vcl.StdCtrls, Vcl.Grids, Vcl.Menus,UGastosExtra;

type
  TControlCajonesGastosExtra = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    gridGastos: TStringGrid;
    btnAgregar: TButton;
    lblUnidades: TLabel;
    lblGastoExtra: TLabel;
    lblClaveArticulo: TLabel;
    txtCostoGastos: TEdit;
    txtNombreGastos: TEdit;
    txtClaveGastos: TEdit;
    mnuGastos: TPopupMenu;
    mnuItemAbrir: TMenuItem;
    mnuItemNuevo: TMenuItem;
    btnGuardar: TButton;
    procedure txtClaveGastosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure txtNombreGastosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure mnuItemAbrirClick(Sender: TObject);
    procedure mnuItemNuevoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    maquila_id:integer;
    gasto_id:string;
  end;

var
  ControlCajonesGastosExtra: TControlCajonesGastosExtra;

implementation

{$R *.dfm}

procedure TControlCajonesGastosExtra.btnAgregarClick(Sender: TObject);
begin
gridGastos.Cells[0,gridGastos.RowCount-1]:=txtClaveGastos.Text;
gridGastos.Cells[1,gridGastos.RowCount-1]:=txtNombreGastos.Text;
gridGastos.Cells[2,gridGastos.RowCount-1]:=txtCostoGastos.Text;
gridGastos.Cells[3,gridGastos.RowCount-1]:=gasto_id;
gridGastos.RowCount:=gridGastos.RowCount+1;
end;

procedure TControlCajonesGastosExtra.btnGuardarClick(Sender: TObject);
var
  i: Integer;
begin
if btnGuardar.Caption='Guardar' then
  begin
      gridGastos.RowCount:=gridGastos.RowCount-1;

        for i := 1 to gridGastos.RowCount-1 do
            begin
            Conexion.ExecSQL('insert into sic_maquila_gastos values (-1,:maquila_id,:idgasto,:costo)',[maquila_id,gridGastos.Cells[3,i],gridGastos.Cells[2,i]])
            end;

      ShowMessage('Gastos guardados correctamente.');
      self.close
  end
      else
        begin
        self.Close;
        end;
end;

procedure TControlCajonesGastosExtra.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Conexion.Close;
end;

procedure TControlCajonesGastosExtra.FormShow(Sender: TObject);
begin
gridGastos.Cells[0,0]:='Clave';
gridGastos.Cells[1,0]:='Gasto';
gridGastos.Cells[2,0]:='Costo';
gridGastos.Cells[3,0]:='ID';
gridGastos.ColWidths[0]:=80;
gridGastos.ColWidths[1]:=200;
gridGastos.ColWidths[2]:=80;
gridGastos.ColWidths[3]:=0;
end;

procedure TControlCajonesGastosExtra.mnuItemAbrirClick(Sender: TObject);
var
  FormGasto : TGastosExtra;
begin
  FormGasto := TGastosExtra.Create(nil);
            FormGasto.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
            FormGasto.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
            FormGasto.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
            FormGasto.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
            FormGasto.Conexion.Open;
            FormGasto.txtNombre.Visible:=false;
            FormGasto.txtClave_Nuevo.Visible:=false;
            FormGasto.txtCosto_Nuevo.Visible:=false;
            FormGasto.ShowModal;
end;

procedure TControlCajonesGastosExtra.mnuItemNuevoClick(Sender: TObject);
var
  FormGasto : TGastosExtra;
begin
  FormGasto := TGastosExtra.Create(nil);
            FormGasto.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
            FormGasto.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
            FormGasto.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
            FormGasto.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
            FormGasto.Conexion.Open;
            FormGasto.cbNombre.Visible:=false;
            FormGasto.txtClave_Nuevo.visible:=true;
            FormGasto.txtCosto_Nuevo.visible:=true;
            FormGasto.btnGuardar.Caption:='Nuevo';
            FormGasto.btnEliminar.visible:=false;
            FormGasto.ShowModal;
end;

procedure TControlCajonesGastosExtra.txtClaveGastosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
    FormBusqueda : TBusquedaGastos;
        begin
          if key = VK_F4 then
          begin
            FormBusqueda := TBusquedaGastos.Create(nil);
            FormBusqueda.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
            FormBusqueda.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
            FormBusqueda.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
            FormBusqueda.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
            FormBusqueda.Conexion.Open;
            Formbusqueda.rb_clave.Checked := true;
            FormBusqueda.txtBusqueda.Text := txtClaveGastos.Text;

                  if FormBusqueda.ShowModal = mrOk then
                  begin
                    txtClaveGastos.Text := FormBusqueda.articulo_clave;
                    txtNombreGastos.Text := FormBusqueda.articulo_nombre;
                    gasto_id := IntToStr(FormBusqueda.articulo_id);
                    txtCostoGastos.Text:=FloatToStr(FormBusqueda.costo_gasto);
                    btnAgregar.SetFocus;
                  end;
          end;

      end;
procedure TControlCajonesGastosExtra.txtNombreGastosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
   var
    FormBusqueda : TBusquedaGastos;
begin
 if key = VK_F4 then
  begin
    FormBusqueda := TBusquedaGastos.Create(nil);
    FormBusqueda.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormBusqueda.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormBusqueda.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
    FormBusqueda.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormBusqueda.Conexion.Open;
    FormBusqueda.txtBusqueda.Text := txtNombreGastos.Text;

    if FormBusqueda.ShowModal = mrOk then
    begin
      txtClaveGastos.Text := FormBusqueda.articulo_clave;
      txtNombreGastos.Text := FormBusqueda.articulo_nombre;
      gasto_id := IntToStr(FormBusqueda.articulo_id);
      txtCostoGastos.Text:=FloatToStr(FormBusqueda.costo_gasto);
      btnAgregar.SetFocus;
    end;
  end;
end;

end.
