unit UControlCajonesMaquilaDetalleSalida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Data.DB, FireDAC.Comp.Client, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Grids, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.DBGrids, UControlCajonesMaquilaTarima;

type
  TControlCajonesMaquilaDetalleSalida = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    Label1: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    txtFolioMaquila: TEdit;
    txtProveedor: TEdit;
    txtFecha: TEdit;
    txtAlmacen: TEdit;
    txtCajas: TEdit;
    txtNumCajas: TEdit;
    GroupBox2: TGroupBox;
    DSSalida: TDataSource;
    btnGuardar: TButton;
    btnAgregarTarima: TButton;
    grdTarimasSalida: TDBGrid;
    lblCajas: TLabel;
    qrySalida: TFDQuery;
    qrySalidaMAQUILA_DET_ID: TIntegerField;
    qrySalidaNUM_TARIMA: TStringField;
    qrySalidaCAJAS: TIntegerField;
    qrySalidaPESO: TBCDField;
    procedure btnAgregarTarimaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure grdTarimasSalidaDblClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    tarima : TTarima;
    num_tarimas, maquila_id : integer;
  end;

var
  ControlCajonesMaquilaDetalleSalida: TControlCajonesMaquilaDetalleSalida;

implementation

{$R *.dfm}

function removerCeros(const Value: string): string;
var
  i: Integer;
begin
  for i := 1 to Length(Value) do
    if Value[i] <> '0' then
    begin
      Result := Copy(Value, i, MaxInt);
      exit;
    end;
  Result := '';
end;

procedure TControlCajonesMaquilaDetalleSalida.btnAgregarTarimaClick(
  Sender: TObject);
  var etiqueta:string;
begin
  tarima := TTarima.Create(nil);
  tarima.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  tarima.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  tarima.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
  tarima.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  tarima.Conexion.Open;
  num_tarimas:=conexion.ExecSQLScalar('select count(maquila_id) from sic_maquila_det_salida where maquila_id=:mid',[maquila_id]);
  tarima.num_tarima := num_tarimas+1;
  tarima.maquila_id := maquila_id;
  tarima.lblNumTarima.Caption := 'Tarima # '+inttostr(num_tarimas+1);
  etiqueta:='8'+Format('%.*d',[5, maquila_id])+Format('%.*d',[3, tarima.num_tarima]);
  tarima.maquila_det_id := Conexion.ExecSQLScalar('insert into sic_maquila_det_salida(maquila_det_id,maquila_id,num_tarima,peso,cajas,etiqueta) values(-1,'+inttostr(maquila_id)+','+inttostr(tarima.num_tarima)+',0,0,'+etiqueta+') returning maquila_det_id');
 // tarima.ShowModal;
 //ELIMINAR TARIMA AL DARLE CANCELAR
  if tarima.ShowModal = mrOk then
    begin
     if tarima.qryDetallesTarima.RecordCount=0 then
     begin
     Conexion.ExecSQL('delete from sic_maquila_det_salida where maquila_det_id=:mid',[tarima.maquila_det_id]);
     Conexion.ExecSQL('delete from sic_maquila_articulos_tarima where maquila_det_id=:mid',[tarima.maquila_det_id]);
     Conexion.Commit;
     end;
    end
    else
    begin
    Conexion.ExecSQL('delete from sic_maquila_det_salida where maquila_det_id=:mid',[tarima.maquila_det_id]);
     Conexion.ExecSQL('delete from sic_maquila_articulos_tarima where maquila_det_id=:mid',[tarima.maquila_det_id]);
     Conexion.Commit;
    end;
  qrySalida.Refresh;
  txtNumCajas.Text := Conexion.ExecSQLScalar('select coalesce(sum(cajas),0) from sic_maquila_det_salida where maquila_id=:mid',[maquila_id]);
end;

procedure TControlCajonesMaquilaDetalleSalida.btnGuardarClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TControlCajonesMaquilaDetalleSalida.FormShow(Sender: TObject);
begin
  qrySalida.ParamByName('maquila_id').Value := maquila_id;
  qrySalida.Open;
  txtNumCajas.Text := Conexion.ExecSQLScalar('select coalesce(sum(cajas),0) from sic_maquila_det_salida where maquila_id=:mid',[maquila_id]);
end;

procedure TControlCajonesMaquilaDetalleSalida.grdTarimasSalidaDblClick(
  Sender: TObject);
begin
  tarima := TTarima.Create(nil);
  tarima.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  tarima.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  tarima.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
  tarima.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  tarima.Conexion.Open;

  tarima.num_tarima := qrySalida.FieldByName('num_tarima').AsInteger;
  tarima.maquila_id := maquila_id;
  tarima.lblNumTarima.Caption := 'Tarima # '+inttostr(tarima.num_tarima);
  tarima.maquila_det_id := qrySalida.FieldByName('maquila_det_id').AsInteger;
  tarima.ShowModal;
  qrySalida.Refresh;
  txtNumCajas.Text := Conexion.ExecSQLScalar('select cajas from sic_maquila_det_salida where maquila_id=:mid',[maquila_id]);
end;

end.
