object ControlCajonesMaquilaEmbarqueTarima: TControlCajonesMaquilaEmbarqueTarima
  Left = 0
  Top = 0
  Caption = 'Tarima'
  ClientHeight = 242
  ClientWidth = 432
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 223
    Width = 432
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object gridTarima: TStringGrid
    Left = 8
    Top = 8
    Width = 415
    Height = 153
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 1
    OnKeyPress = gridTarimaKeyPress
    OnSelectCell = gridTarimaSelectCell
  end
  object btnGuardar: TButton
    Left = 192
    Top = 176
    Width = 97
    Height = 33
    Caption = 'Guardar'
    TabOrder = 2
    OnClick = btnGuardarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Server=localhost'
      'Protocol=TCPIP'
      'DriverID=FB')
    LoginPrompt = False
    Left = 483
    Top = 182
  end
end
