unit UControlCajonesMaquilaMaterial;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,UControlCajonesMaquilaEmbarqueSalida,
  FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids,System.Character,UControlCajonesMaquilaChofer,
  frxClass, frxDBSet;

type
  TControlCajonesMaquilaMaterial = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    gridEntrada: TDBGrid;
    Label1: TLabel;
    gridSalida: TDBGrid;
    Label2: TLabel;
    Label3: TLabel;
    btnImprimir: TButton;
    btnCerrar: TButton;
    DSLlenadoEntrada: TDataSource;
    gridMaterial: TStringGrid;
    txtTotal: TEdit;
    Label4: TLabel;
    qryLlenadoSalida: TFDQuery;
    DSLlenadoSalida: TDataSource;
    qryMaterial: TFDQuery;
    txtTotalCajas: TEdit;
    Label5: TLabel;
    DSReporte: TfrxDBDataset;
    Reporte: TfrxReport;
    qryImprimir: TFDQuery;
    qryLlenadoSalidaARTICULO: TStringField;
    DBGrid3: TDBGrid;
    DSMaterial: TDataSource;
    qryLlenadoSalidaCAJAS: TBCDField;
    qryLlenadoSalidaARTICULO_ID: TIntegerField;
    qryMaterialUNIDADES: TFMTBCDField;
    qryMaterialARTICULO: TStringField;
    qryMaterialARTICULO_ID: TIntegerField;
    qryImprimirARTICULO: TStringField;
    qryImprimirUNIDADES: TFMTBCDField;
    qryImprimirARTICULO_ID: TIntegerField;
    qryLlenadoEntrada: TFDQuery;
    qryLlenadoEntradaKG: TBCDField;
    qryLlenadoEntradaNOMBRE: TStringField;
    qryLlenadoEntradaARTICULO_ID: TIntegerField;
    qryImprimirFOLIO: TStringField;
    qryMaterialCOSTO_ULTIMA_COMPRA: TFMTBCDField;
    txtTotalCosto: TEdit;
    Label6: TLabel;
    qryImprimirCOSTO_ULTIMA_COMPRA: TFMTBCDField;
    procedure FormShow(Sender: TObject);
    procedure gridSalidaDblClick(Sender: TObject);
    procedure btnCerrarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    maquila_id,almacen_salida_id,almacen_entrada_id,proveedor_id,liberar:integer;
    total_kg,total_cajas:double;
    folio_str,folio_maquila:string;
  end;

var
  ControlCajonesMaquilaMaterial: TControlCajonesMaquilaMaterial;

implementation

{$R *.dfm}

function GetSerie(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsLetter(folio[i]))
    then Result := Result+folio[i]
    else exit;
  end;
end;

function GetNumFolio(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsNumber(folio[i]))
    then Result := Result+folio[i];
  end;
end;

procedure TControlCajonesMaquilaMaterial.btnImprimirClick(Sender: TObject);
begin
    qryImprimir.ParamByName('mid').Value:=inttostr(maquila_id);
    qryImprimir.Open;
    reporte.PrepareReport(True);
    Reporte.Print;
    Reporte.ShowReport(true);
end;

procedure TControlCajonesMaquilaMaterial.btnCerrarClick(Sender: TObject);
var
  FormChofer : TControlCajonesMaquilaChofer;
  status : string;buttonSelected:integer;
  query_str,folio_nuevo,serie,fecha,clave_articulo:string;
  consecutivo,documento_id,concepto_id,articulo_id,i,unidades:integer;
  precio_unitario,precio_total_neto,unidades_entrada,unidades_material,ultimo_costo:double;
begin
  if qryMaterial.RecordCount<>0 then
  begin
  //////////////////////////SALIDA A INVENTARIOS DE MATERIAL/////////////////////////////////////////////
  //****************************************DA DE ALTA EL ENCABEZADO***********************************
    try
    if Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''SALIDA DE MATERIAL''')='' then
      begin
      ShowMessage('No existe el concepto ''Salida de Material'' en inventarios.');
      end
      else
      begin
      serie :=GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''SALIDA DE MATERIAL'''));
      consecutivo :=strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''SALIDA DE MATERIAL''')));
      concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where upper(nombre) = ''SALIDA DE MATERIAL''');
      //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
      folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
      folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
      Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

      //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
      fecha := FormatDateTime('mm/dd/yyyy', Now);

      //MSP 2019
      {query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,descripcion,'+
      'sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_salida_id)+','+inttostr(concepto_id)+','+
      ''''+folio_str+''',''S'','''+fecha+''',''Salida de material de la maquila: '+folio_maquila+''','+
      '''IN'','+
      ''+inttostr(proveedor_id)+') returning docto_in_id';}
      //MSP 2020
      query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,sucursal_id,folio,naturaleza_concepto,fecha,descripcion,'+
      'sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_salida_id)+','+inttostr(concepto_id)+','+
      ' '+inttostr(Conexion.ExecSQLScalar('select sucursal_id from sucursales where upper(nombre)=''MATRIZ'''))+','''+folio_str+''',''S'','''+fecha+''',''Salida de material de la maquila: '+folio_maquila+''','+
      '''IN'','+
      ''+inttostr(proveedor_id)+') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);


      //*******************************DETALLADO DEL DOCUMENTO*********************************************
      qryMaterial.First;
      while not qryMaterial.Eof do
      begin
        articulo_id:=qryMaterialARTICULO_ID.Value;
        clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
        unidades_material:=double(qryMaterialUNIDADES.Value);
        precio_unitario :=  Conexion.ExecSQLScalar('select costo_ultima_compra from get_ultcom_art(:aid)',[articulo_id]);
        precio_total_neto := precio_unitario*unidades_material;
        query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,clave_articulo,'+
        ' articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo) values(-1,'+
        inttostr(documento_id)+','+IntToStr(almacen_salida_id)+','+IntToStr(concepto_id)+','''+clave_articulo+''','+
        inttostr(articulo_id)+',''S'','+
        floattostr(unidades_material)+
        ','+floattostr(precio_unitario)+','+floattostr(precio_total_neto)+',''C'') ';
        Conexion.ExecSQL(query_str);

        query_str := 'insert into sic_material_empaque(id,maquila_id,articulo_id,unidades) values (-1,'+inttostr(maquila_id)+','+inttostr(articulo_id)+','+floattostr(unidades_material)+')';
        Conexion.ExecSQL(query_str);
        qryMaterial.Next;
      end;
      Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);
      Conexion.Commit;
     /////////////////////////////////////

     //////////////////////////SALIDA A INVENTARIOS DE GRID DE ENTRADA/////////////////////////////////////////////
  //****************************************DA DE ALTA EL ENCABEZADO***********************************
      serie :=GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''SALIDA A MAQUILA'''));
      consecutivo :=strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''SALIDA A MAQUILA''')));
      concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where upper(nombre) = ''SALIDA A MAQUILA''');
      //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
      folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
      folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
      Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

      //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
      fecha := FormatDateTime('mm/dd/yyyy', Now);

      //MSP 2019
      {query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,descripcion,'+
      ' sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_entrada_id)+','+inttostr(concepto_id)+','+
      ' '''+folio_str+''',''S'','''+fecha+''',''Entrada a maquila: '+folio_maquila+''','+
      ' ''IN'','+
      ' '+inttostr(proveedor_id)+') returning docto_in_id';}
      //MSP 2020
      query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,sucursal_id,folio,naturaleza_concepto,fecha,descripcion,'+
      ' sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_entrada_id)+','+inttostr(concepto_id)+','+
      ' '+inttostr(Conexion.ExecSQLScalar('select sucursal_id from sucursales where upper(nombre)=''MATRIZ'''))+','''+folio_str+''',''S'','''+fecha+''',''Entrada a maquila: '+folio_maquila+''','+
      ' ''IN'','+
      ' '+inttostr(proveedor_id)+') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);


      //*******************************DETALLADO DEL DOCUMENTO*********************************************
      qryLLenadoEntrada.First;
      while not qryLlenadoEntrada.eof do
      begin

        articulo_id:=qryLlenadoEntradaARTICULO_ID.Value;
        clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
        unidades_entrada:=qryLlenadoEntradaKG.Value;
        precio_unitario :=  Conexion.ExecSQLScalar('select costo_ultima_compra from get_ultcom_art(:aid)',[articulo_id]);
        precio_total_neto := precio_unitario*unidades_entrada;
        query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,clave_articulo,'+
        ' articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo) values(-1,'+
        inttostr(documento_id)+','+IntToStr(almacen_entrada_id)+','+IntToStr(concepto_id)+','''+clave_articulo+''','+
        inttostr(articulo_id)+',''S'','+
        floattostr(unidades_entrada)+
        ','+floattostr(precio_unitario)+','+floattostr(precio_total_neto)+',''C'') ';
        Conexion.ExecSQL(query_str);
      qryLlenadoEntrada.Next;
      end;

      Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);
      Conexion.Commit;
     /////////////////////////////////////

      //////////////////////////ENTRADA A INVENTARIOS DE GRID DE SALIDA/////////////////////////////////////////////
  //****************************************DA DE ALTA EL ENCABEZADO***********************************
      serie :=GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''ENTRADA A MAQUILA'''));
      consecutivo :=strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_in where upper(nombre) = ''ENTRADA A MAQUILA''')));
      concepto_id := Conexion.ExecSQLScalar('select concepto_in_id from conceptos_in where upper(nombre) = ''ENTRADA A MAQUILA''');
      //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
      folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
      folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
      Conexion.ExecSQL('update conceptos_in set sig_folio=:sig where concepto_in_id=:concepto_id',[folio_nuevo,concepto_id]);

      //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
      fecha := FormatDateTime('mm/dd/yyyy', Now);

      //MSP 2019
      {query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,folio,naturaleza_concepto,fecha,descripcion,'+
      ' sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_salida_id)+','+inttostr(concepto_id)+','+
      ' '''+folio_str+''',''E'','''+fecha+''',''Salida a maquila: '+folio_maquila+''','+
      ' ''IN'','+
      ' '+inttostr(proveedor_id)+') returning docto_in_id';}

      //MSP 2020
      query_str := 'insert into doctos_in (docto_in_id,almacen_id,concepto_in_id,sucursal_id,folio,naturaleza_concepto,fecha,descripcion,'+
      ' sistema_origen,sic_proveedor) values (-1,'+inttostr(almacen_salida_id)+','+inttostr(concepto_id)+','+
      ' '+inttostr(Conexion.ExecSQLScalar('select sucursal_id from sucursales where upper(nombre)=''MATRIZ'''))+','''+folio_str+''',''E'','''+fecha+''',''Salida a maquila: '+folio_maquila+''','+
      ' ''IN'','+
      ' '+inttostr(proveedor_id)+') returning docto_in_id';
      documento_id := Conexion.ExecSQLScalar(query_str);

      //*******************************DETALLADO DEL DOCUMENTO*********************************************
      qryLlenadoSalida.First;
      while not qryLlenadoSalida.eof do
        begin
        if Conexion.ExecSQLScalar('select count(componente_id) from juegos_det where articulo_id=:c',[qryLlenadoSalidaARTICULO_ID.Value])=0 then
        begin
        articulo_id:=qryLlenadoSalidaARTICULO_ID.Value;
        clave_articulo := Conexion.ExecSQLScalar('select coalesce(clave_articulo,'''') from get_clave_art(:aid)',[articulo_id]);
        unidades_entrada:=qryLlenadoSalidaCAJAS.Value;
        ultimo_costo:=Conexion.ExecSQLScalar('select costo_ultima_compra from get_ultcom_art(:a)',[articulo_id]);
        precio_unitario:=(strtofloat(txtTotal.Text)*ultimo_costo+strtofloat(txtTotalCosto.Text))/strtofloat(txtTotalCajas.Text);
        //precio_unitario :=  Conexion.ExecSQLScalar('select coalesce(precio,0) from articulos a join precios_articulos pa on a.articulo_id=pa.articulo_id where a.articulo_id=:aid',[articulo_id]);
        precio_total_neto := precio_unitario*unidades_entrada;
        query_str := 'insert into doctos_in_det (docto_in_det_id, docto_in_id,almacen_id,concepto_in_id,clave_articulo,'+
        ' articulo_id,tipo_movto,unidades,costo_unitario,costo_total,metodo_costeo) values(-1,'+
        inttostr(documento_id)+','+IntToStr(almacen_salida_id)+','+IntToStr(concepto_id)+','''+clave_articulo+''','+
        inttostr(articulo_id)+',''S'','+
        floattostr(unidades_entrada)+
        ','+floattostr(precio_unitario)+','+floattostr(precio_total_neto)+',''C'') ';
        Conexion.ExecSQL(query_str);
        end;
       qryLlenadoSalida.Next;
       end;

      Conexion.ExecSQL('execute procedure aplica_docto_in(:did)',[documento_id]);
      Conexion.Commit;
     /////////////////////////////////////

    {  FormChofer := TControlCajonesMaquilaChofer.create(nil);
      FormChofer.proveedor_id := proveedor_id;
      FormChofer.almacen_id := almacen_entrada_id;
      Formchofer.maquila_id:=maquila_id;
      FormChofer.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
      FormChofer.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
      FormChofer.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
      FormChofer.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
      FormChofer.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
      FormChofer.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
      FormChofer.ShowModal;  }

      //LIBERAR MAQUILA//
      status := Conexion.ExecSQLScalar('select estatus from sic_maquila where maquila_id = '''+inttostr(maquila_id)+'''');
      if status = 'A' then
      begin
        Conexion.ExecSQL('update sic_maquila set estatus=''C'',fechahora_liberacion=:fecha where maquila_id=:folio',[Now,inttostr(maquila_id)]);
        btnCerrar.Enabled:=false;
        gridEntrada.Enabled:=false;
        gridSalida.Enabled:=false;
        gridMaterial.Enabled:=false;
        ShowMessage('Maquila Cerrada.');
        liberar:=1;
      end
      else
      begin
        Conexion.ExecSQL('update sic_maquila set estatus=''A'',fechahora_liberacion=:fecha where maquila_id=:folio',[Now,inttostr(maquila_id)]);
      end;
      btnImprimir.Enabled:=true;
        end;
    except
      ShowMessage('Error no se pudo generar el documento de inventarios.');
      Conexion.Rollback;
    end;
  end
  else
  begin
    ShowMessage('No se puede cerrar la maquila si no hay material.');
  end;

end;

procedure TControlCajonesMaquilaMaterial.gridSalidaDblClick(Sender: TObject);
var
  i,j,k:integer;
  encontrado:bool;
  FormEmbarqueSalida : TControlCajonesMaquilaEmbarqueSalida;
begin
  FormEmbarqueSalida := TControlCajonesMaquilaEmbarqueSalida.create(nil);
  FormEmbarqueSalida.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormEmbarqueSalida.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormEmbarqueSalida.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormEmbarqueSalida.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormEmbarqueSalida.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormEmbarqueSalida.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;


  qryMaterial.SQL.Clear;
  qryMaterial.SQL.Text:='select nombre as Articulo,unidades as Unidades,articulo_id from'+
  ' (select componente_id,unidades from articulos a join juegos_det jd on'+
  ' a.articulo_id=jd.articulo_id where a.articulo_id=:aid) f join articulos e on'+
  ' e.articulo_id=f.componente_id';
  qryMaterial.ParamByName('aid').Value:=qryLlenadoSalidaARTICULO_ID.Value;
  qryMaterial.Open;

  FormEmbarqueSalida.gridMaterial.Cols[0].Clear;
  FormEmbarqueSalida.gridMaterial.Cols[1].Clear;
  FormEmbarqueSalida.gridMaterial.Cells[0,0]:='Unidades';
  FormEmbarqueSalida.gridMaterial.Cells[1,0]:='Articulo';
  FormEmbarqueSalida.gridMaterial.RowCount:=1;

  //LLENAR STRINGGRID EMBARQUE SALIDA
  qryMaterial.First;
  j:=1;
  while not qryMaterial.Eof do
  begin
    FormEmbarqueSalida.GridMaterial.RowCount:=FormEmbarqueSalida.GridMaterial.RowCount+1;
    for i:=0 to qryMaterial.FieldCount - 1 do begin
      if i=1 then
      begin
        FormEmbarqueSalida.GridMaterial.Cells[i,j] := '0';

      end
      else
        FormEmbarqueSalida.GridMaterial.Cells[i,j] := qryMaterial.Fields[i].AsString;
    end;{for i}

    qryMaterial.Next;
    Inc(j);

  end;{while}

  FormEmbarqueSalida.ShowModal;
  j:=GridMaterial.RowCount;
  //LLENAR STRINGGRID MATERIAL
  if FormEmbarqueSalida.guardar then
  begin
    for i:=1 to FormEmbarqueSalida.GridMaterial.RowCount-1 do begin
      for k := 0 to  GridMaterial.RowCount-1  do
      begin
        if FormEmbarqueSalida.GridMaterial.Cells[0,i]=GridMaterial.Cells[1,k] then
        begin
          GridMaterial.Cells[0,k]:=inttostr(strtoint(GridMaterial.Cells[0,k])+strtoint(FormEmbarqueSalida.GridMaterial.Cells[1,i]));
          encontrado:=true;
        end;
      end;
      if not encontrado then
      begin
        GridMaterial.Cells[1,j] := FormEmbarqueSalida.GridMaterial.Cells[0,i];
        GridMaterial.Cells[0,j] := FormEmbarqueSalida.GridMaterial.Cells[1,i];
        GridMaterial.Cells[2,j] := FormEmbarqueSalida.GridMaterial.Cells[2,i];
        GridMaterial.RowCount:=GridMaterial.RowCount+1;
      end;

      Inc(j);
    end;{for i}

  ShowMessage('Material agregado.');
  end;

end;

procedure TControlCajonesMaquilaMaterial.FormShow(Sender: TObject);
var
consulta_material,i:integer;
begin
  txtTotal.Text:=floattostr(total_kg);
  txtTotalCajas.Text:=floattostr(total_cajas);
  txtTotalCosto.Text:=Format('%.2f', [strtofloat(Conexion.ExecSQLScalar('select sum((unidades)*(costo_ultima_compra)) as CostoTotal'+
' from (select (select costo_ultima_compra from get_ultcom_art(com.articulo_id)),sat.unidades as cajas,'+
       ' com.nombre,(sat.unidades*jd.unidades) as unidades,com.articulo_id'+
' from sic_maquila_articulos_tarima sat join'+
' sic_maquila_det_salida ds on ds.maquila_det_id = sat.maquila_det_id join'+
' articulos a on a.articulo_id = sat.articulo_id  join'+
' juegos_det jd on jd.articulo_id = a.articulo_id join'+
' articulos com on com.articulo_id=jd.componente_id'+
' where ds.maquila_id = :maquila_id )',[inttostr(maquila_id)]))]);
  //LLENADO DE ENTRADA
  qryllenadoEntrada.SQL.Clear;
  qryLlenadoEntrada.SQL.Text:=' select sum(peso) as KG,(select nombre from articulos where articulo_id=cc.articulo_id),articulo_id'+
                              ' from claves_cajones cc where cc.maquila_id=:maquila_id and tipo=''E'''+
                              ' group by articulo_id,peso';
  qryLlenadoEntrada.ParamByName('maquila_id').Value:=maquila_id;
  qryLlenadoEntrada.Open;

  //LLENADO DE SALIDA

  qryLlenadoSalida.ParamByName('maquila_id').AsInteger := maquila_id;
  qryllenadoSalida.Open;
  //BLOQUEAR BOTON LIBERAR EN CASO DE QUE LA SALIDA ESTE VACIA
  if qryLlenadoSalida.Recordcount=0 then
   begin
   btnCerrar.Enabled:=False;
   end;

  //TITULOS DEL STRINGGRID MATERIAL
  gridMaterial.Cells[1,0]:='Articulo';
  gridMaterial.Cells[0,0]:='Unidades';
  gridMaterial.ColWidths[1]:=450;
  gridMaterial.ColWidths[0]:=70;

  consulta_material:=Conexion.ExecSQLScalar('select count(articulo_id) from sic_material_empaque where maquila_id=:mid',[inttostr(maquila_id)]);
  if consulta_material>0 then
  begin
    btnImprimir.Enabled:=true;
  end;
  qryMaterial.ParamByName('maquila_id').AsInteger := maquila_id;
  qryMaterial.Open;

    //LLENAR SRTINGGRID MATERIAL
    i:=1;
  qryMaterial.First;
  while not qryMaterial.Eof do
  begin
  i:=i+1;
  gridMaterial.Cells[0,i]:=string(qryMaterialUNIDADES.Value);
  gridMaterial.Cells[1,i]:=string(qryMaterialARTICULO.Value);
  qryMaterial.Next;
  end;


end;

end.
