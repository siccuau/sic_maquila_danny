unit UControlCajonesMaquilaTarima;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, UBusqueda, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet;

type
  TTarima = class(TForm)
    btnGuardar: TButton;
    grdTarima: TDBGrid;
    lblUnidades: TLabel;
    txtUnidades: TEdit;
    lblArticulo: TLabel;
    txtNombreArticulo: TEdit;
    txtClaveArticulo: TEdit;
    lblClaveArticulo: TLabel;
    btnAgregar: TButton;
    txtSumaCajasTarima: TEdit;
    lblCajas: TLabel;
    lblNumTarima: TLabel;
    Conexion: TFDConnection;
    qryDetallesTarima: TFDQuery;
    dsDetallesTarima: TDataSource;
    qryAgregar: TFDQuery;
    btnCancelar: TButton;
    qryDetallesTarimaUNIDADES: TBCDField;
    qryDetallesTarimaCLAVE_ARTICULO: TStringField;
    qryDetallesTarimaARTICULO: TStringField;
    qryDetallesTarimaPESO: TBCDField;
    txtPesoTotal: TEdit;
    Label1: TLabel;
    procedure txtClaveArticuloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnAgregarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure txtNombreArticuloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    art_id:string;
    num_tarima : integer;
    maquila_id, maquila_det_id : integer;
  end;

var
  Tarima: TTarima;

implementation

{$R *.dfm}

procedure TTarima.btnAgregarClick(Sender: TObject);
var
ultimo_costo,peso:double;
begin
  ultimo_costo:=Conexion.ExecSQLScalar('select costo_ultima_compra from get_ultcom_art(:art)',[art_id]);
  peso:=strtoint(txtUnidades.Text)*ultimo_costo;
  Conexion.ExecSQL('insert into sic_maquila_articulos_tarima values(-1,:mid,:aid,:un,:cve,:surt,:peso)'
                ,[maquila_det_id,art_id,strtoint(txtUnidades.Text),txtClaveArticulo.Text,strtoint(txtUnidades.Text),peso]);
  qryDetallesTarima.Refresh;
  txtUnidades.Clear;
  txtClaveArticulo.Clear;
  txtNombreArticulo.Clear;
  txtUnidades.SetFocus;
  txtPesoTotal.Text:=Conexion.ExecSQLScalar('select coalesce(sum(peso),0) from sic_maquila_articulos_tarima where maquila_det_id = :m ',[maquila_det_id]);
  txtSumaCajasTarima.Text := Conexion.ExecSQLScalar('select coalesce(sum(unidades),0) from sic_maquila_articulos_tarima where maquila_det_id = :m ',[maquila_det_id]);
  //ACTUALIZAR CAJAS
  Conexion.ExecSQL('update sic_maquila_det_salida set cajas=:c where maquila_det_id = :mid',[strtoint(txtSumaCajasTarima.Text),maquila_det_id]);
  //ACTUALIZAR PESO
   Conexion.ExecSQL('update sic_maquila_det_salida set peso=:c where maquila_det_id = :mid',[strtofloat(txtPesoTotal.Text),maquila_det_id]);
end;

procedure TTarima.btnCancelarClick(Sender: TObject);
begin
   ModalResult := mrCancel;
  Close;
end;

procedure TTarima.btnGuardarClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TTarima.FormShow(Sender: TObject);
begin
  txtSumaCajasTarima.Text := Conexion.ExecSQLScalar('select coalesce(sum(unidades),0) from sic_maquila_articulos_tarima where maquila_det_id = :m ',[maquila_det_id]);
  txtPesoTotal.Text:=Conexion.ExecSQLScalar('select coalesce(sum(peso),0) from sic_maquila_articulos_tarima where maquila_det_id = :m ',[maquila_det_id]);
  qryDetallesTarima.ParamByName('maquila_det_id').AsInteger := maquila_det_id;
  qryDetallesTarima.Open;
end;

procedure TTarima.txtClaveArticuloKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FormBusqueda : TBusquedaArticulos;
begin
  if key = VK_F4 then
  begin
    FormBusqueda := TBusquedaArticulos.Create(nil);
    FormBusqueda.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormBusqueda.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormBusqueda.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
    FormBusqueda.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormBusqueda.Conexion.Open;
    Formbusqueda.rb_clave.Checked := true;
    FormBusqueda.txtBusqueda.Text := txtClaveArticulo.Text;

    if FormBusqueda.ShowModal = mrOk then
    begin
      txtClaveArticulo.Text := FormBusqueda.articulo_clave;
      txtNombreArticulo.Text := FormBusqueda.articulo_nombre;
      art_id := IntToStr(FormBusqueda.articulo_id);
      btnAgregar.SetFocus;
    end;
  end;

  if Key = VK_RETURN then
  begin
    if Conexion.ExecSQLScalar('select count(*) from claves_articulos where clave_articulo = :ca',[txtClaveArticulo.Text]) > 0 then
    begin
      art_id := Conexion.ExecSQLScalar('select articulo_id from claves_articulos where clave_articulo = :ca',[txtClaveArticulo.Text]);
      txtNombreArticulo.Text := Conexion.ExecSQLScalar('select nombre from articulos where articulo_id = :aid',[art_id]);
      btnAgregar.SetFocus;
    end
    else
    begin
      ShowMessage('El articulo no existe. busque con la tecla F4');
      txtClaveArticulo.SetFocus;
      txtClaveArticulo.SelectAll;
    end;
  end;
end;

procedure TTarima.txtNombreArticuloKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FormBusqueda : TBusquedaArticulos;
begin
  if key = VK_F4 then
  begin
    FormBusqueda := TBusquedaArticulos.Create(nil);
    FormBusqueda.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormBusqueda.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormBusqueda.Conexion.Params.Values['UserName'] := Conexion.Params.Values['UserName'];
    FormBusqueda.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormBusqueda.Conexion.Open;
    FormBusqueda.txtBusqueda.Text := txtNombreArticulo.Text;

    if FormBusqueda.ShowModal = mrOk then
    begin
      txtClaveArticulo.Text := FormBusqueda.articulo_clave;
      txtNombreArticulo.Text := FormBusqueda.articulo_nombre;
      art_id := IntToStr(FormBusqueda.articulo_id);
      btnAgregar.SetFocus;
    end;
  end;
end;

end.
