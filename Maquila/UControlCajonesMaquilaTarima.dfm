object Tarima: TTarima
  Left = 0
  Top = 0
  Caption = 'Tarima'
  ClientHeight = 338
  ClientWidth = 834
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblUnidades: TLabel
    Left = 77
    Top = 49
    Width = 44
    Height = 13
    Caption = 'Unidades'
  end
  object lblArticulo: TLabel
    Left = 239
    Top = 49
    Width = 36
    Height = 13
    Caption = 'Articulo'
  end
  object lblClaveArticulo: TLabel
    Left = 127
    Top = 49
    Width = 27
    Height = 13
    Caption = 'Clave'
  end
  object lblCajas: TLabel
    Left = 136
    Top = 267
    Width = 27
    Height = 13
    Caption = 'Cajas'
  end
  object lblNumTarima: TLabel
    Left = 48
    Top = 16
    Width = 4
    Height = 18
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 697
    Top = 267
    Width = 50
    Height = 13
    Caption = 'Peso Total'
  end
  object btnGuardar: TButton
    Left = 158
    Top = 294
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 6
    OnClick = btnGuardarClick
  end
  object grdTarima: TDBGrid
    Left = 48
    Top = 104
    Width = 778
    Height = 154
    DataSource = dsDetallesTarima
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object txtUnidades: TEdit
    Left = 48
    Top = 68
    Width = 73
    Height = 21
    Alignment = taRightJustify
    TabOrder = 0
  end
  object txtNombreArticulo: TEdit
    Left = 239
    Top = 68
    Width = 162
    Height = 21
    TabOrder = 2
    OnKeyDown = txtNombreArticuloKeyDown
  end
  object txtClaveArticulo: TEdit
    Left = 127
    Top = 68
    Width = 106
    Height = 21
    TabOrder = 1
    OnKeyDown = txtClaveArticuloKeyDown
  end
  object btnAgregar: TButton
    Left = 407
    Top = 66
    Width = 50
    Height = 25
    Caption = 'Agregar'
    TabOrder = 3
    OnClick = btnAgregarClick
  end
  object txtSumaCajasTarima: TEdit
    Left = 48
    Top = 264
    Width = 73
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    TabOrder = 5
  end
  object btnCancelar: TButton
    Left = 248
    Top = 294
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 7
    OnClick = btnCancelarClick
  end
  object txtPesoTotal: TEdit
    Left = 753
    Top = 264
    Width = 73
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    TabOrder = 8
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=C:\Microsip datos 2020\SAGE 2020.FDB'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 48
    Top = 296
  end
  object qryDetallesTarima: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select sa.unidades, sa.clave_articulo, a.nombre as Articulo , sa' +
        '.peso'
      'from sic_maquila_Articulos_tarima sa join'
      'articulos a on a.articulo_id =sa.articulo_id'
      'where sa.maquila_det_id = :maquila_det_id')
    Left = 312
    Top = 272
    ParamData = <
      item
        Name = 'MAQUILA_DET_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryDetallesTarimaUNIDADES: TBCDField
      DisplayLabel = 'Unidades'
      DisplayWidth = 10
      FieldName = 'UNIDADES'
      Origin = 'UNIDADES'
      Precision = 18
      Size = 2
    end
    object qryDetallesTarimaCLAVE_ARTICULO: TStringField
      DisplayLabel = 'Clave Articulo'
      FieldName = 'CLAVE_ARTICULO'
      Origin = 'CLAVE_ARTICULO'
    end
    object qryDetallesTarimaARTICULO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nombre'
      DisplayWidth = 70
      FieldName = 'ARTICULO'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 200
    end
    object qryDetallesTarimaPESO: TBCDField
      DisplayLabel = 'Peso'
      DisplayWidth = 20
      FieldName = 'PESO'
      Origin = 'PESO'
      Precision = 18
      Size = 2
    end
  end
  object dsDetallesTarima: TDataSource
    DataSet = qryDetallesTarima
    Left = 408
    Top = 256
  end
  object qryAgregar: TFDQuery
    Connection = Conexion
    Left = 272
    Top = 16
  end
end
