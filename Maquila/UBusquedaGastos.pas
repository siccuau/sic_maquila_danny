unit UBusquedaGastos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TBusquedaGastos = class(TForm)
    txtBusqueda: TEdit;
    rb_nombre: TRadioButton;
    rb_clave: TRadioButton;
    db_articulos: TDBGrid;
    Button1: TButton;
    Button2: TButton;
    DataSource1: TDataSource;
    FDQuery1: TFDQuery;
    FDTransaction1: TFDTransaction;
    Conexion: TFDConnection;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure db_articulosDblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
        procedure buscar();
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
      articulo_id : Integer;
      costo_gasto:double;
  query : String;
  query_gasto:String;
  articulo_nombre :String;
  articulo_clave : string;

  end;

var
  BusquedaGastos: TBusquedaGastos;

implementation

{$R *.dfm}

procedure TBusquedaGastos.Button1Click(Sender: TObject);
begin
    articulo_nombre := db_articulos.DataSource.DataSet.FieldByName('nombre_gasto').AsString;
    query := 'select id_gasto from sic_gastos_conceptos where nombre_gasto= :articulo_nombre';
    articulo_id := Conexion.ExecSQLScalar(query,[articulo_nombre]);
    articulo_clave := db_articulos.DataSource.DataSet.FieldByName('clave').AsString;
    query_gasto := 'select costo from sic_gastos_conceptos where id_gasto= :id';
    costo_gasto:= Conexion.ExecSQLScalar(query_gasto,[articulo_id]);
    {query := 'select costo_ultima_compra from articulos where nombre= :articulo_nombre';
    costo_original := Conexion.ExecSQLScalar(query,[articulo_nombre]); }
    ModalResult := mrOK;
end;

procedure TBusquedaGastos.Button2Click(Sender: TObject);
begin
   ModalResult := mrCancel;
  Close;
end;

procedure TBusquedaGastos.db_articulosDblClick(Sender: TObject);
begin
    articulo_nombre := db_articulos.DataSource.DataSet.FieldByName('nombre_gasto').AsString;
    query := 'select id_gasto from sic_gastos_conceptos where nombre_gasto= :articulo_nombre';
    articulo_id := Conexion.ExecSQLScalar(query,[articulo_nombre]);
    articulo_clave := db_articulos.DataSource.DataSet.FieldByName('clave').AsString;
    query_gasto := 'select costo from sic_gastos_conceptos where id_gasto= :id';
    costo_gasto:= Conexion.ExecSQLScalar(query_gasto,[articulo_id]);
    {query := 'select costo_ultima_compra from articulos where nombre= :articulo_nombre';
    costo_original := Conexion.ExecSQLScalar(query,[articulo_nombre]);}
    ModalResult := mrOK;
end;

procedure TBusquedaGastos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Conexion.Close;
end;

procedure TBusquedaGastos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = VK_ESCAPE then
 begin
   Close;
 end;
end;

procedure TBusquedaGastos.FormShow(Sender: TObject);
begin
  txtBusqueda.Clear;
  txtBusqueda.SetFocus;
  buscar;
end;

procedure TBusquedaGastos.buscar();
begin
  FDQuery1.Close;
  if rb_nombre.Checked then
  begin
    FDQuery1.SQL.Text := 'select clave_gasto as clave, nombre_gasto from sic_gastos_conceptos'+
                         ' where upper(nombre_gasto) like Upper(''%'+txtBusqueda.Text+'%'');';
  end
  else
  begin
    FDQuery1.SQL.Text := 'select clave_gasto as clave, nombre_gasto from sic_gastos_conceptos'+
                         ' where upper(nombre_gasto) like Upper(''%'+txtBusqueda.Text+'%'');';
  end;
  FDQuery1.Open;
end;
end.
