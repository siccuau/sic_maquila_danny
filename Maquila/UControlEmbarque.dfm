object FormEmbarqueMaquilas: TFormEmbarqueMaquilas
  Left = 0
  Top = 0
  Caption = 'Maquilas Embarque'
  ClientHeight = 338
  ClientWidth = 929
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBMaquilas: TDBGrid
    Left = 8
    Top = 47
    Width = 713
    Height = 266
    DataSource = dsMaquilas
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBMaquilasDblClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 319
    Width = 929
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object btnNuevo: TButton
    Left = 16
    Top = 8
    Width = 81
    Height = 33
    Caption = 'Nuevo'
    TabOrder = 2
    OnClick = btnNuevoClick
  end
  object RadioGroup1: TRadioGroup
    Left = 736
    Top = 55
    Width = 185
    Height = 137
    Caption = 'Ordenar Por'
    ItemIndex = 0
    Items.Strings = (
      'Folio'
      'Fecha')
    TabOrder = 3
    OnClick = RadioGroup1Click
  end
  object RadioGroup2: TRadioGroup
    Left = 736
    Top = 8
    Width = 185
    Height = 33
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Asc'
      'Desc')
    TabOrder = 4
    OnClick = RadioGroup2Click
  end
  object GroupBox1: TGroupBox
    Left = 736
    Top = 198
    Width = 185
    Height = 105
    Caption = 'Buscador'
    TabOrder = 5
    object txtBuscarFolio: TEdit
      Left = 32
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object btnBuscarFolio: TButton
      Left = 56
      Top = 59
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 1
      OnClick = btnBuscarFolioClick
    end
  end
  object dsMaquilas: TDataSource
    DataSet = QueryMaquilas
    Left = 554
    Top = 168
  end
  object QueryMaquilas: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select embarque_id,folio,fecha,chofer,placas,poliza_de_seguro fr' +
        'om sic_embarque')
    Left = 509
    Top = 80
    object QueryMaquilasEMBARQUE_ID: TIntegerField
      FieldName = 'EMBARQUE_ID'
      Origin = 'EMBARQUE_ID'
      Required = True
      Visible = False
    end
    object QueryMaquilasFOLIO: TStringField
      DisplayWidth = 10
      FieldName = 'FOLIO'
      Origin = 'FOLIO'
    end
    object QueryMaquilasFECHA: TDateField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object QueryMaquilasCHOFER: TStringField
      DisplayWidth = 30
      FieldName = 'CHOFER'
      Origin = 'CHOFER'
      Size = 50
    end
    object QueryMaquilasPLACAS: TStringField
      FieldName = 'PLACAS'
      Origin = 'PLACAS'
    end
    object QueryMaquilasPOLIZA_DE_SEGURO: TStringField
      FieldName = 'POLIZA_DE_SEGURO'
      Origin = 'POLIZA_DE_SEGURO'
    end
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Server=localhost'
      'Protocol=TCPIP'
      'DriverID=FB')
    LoginPrompt = False
    Left = 509
    Top = 40
  end
  object qryLlenado: TFDQuery
    Connection = Conexion
    Left = 192
    Top = 8
  end
end
