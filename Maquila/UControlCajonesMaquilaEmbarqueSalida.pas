unit UControlCajonesMaquilaEmbarqueSalida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, Vcl.Grids,
  Vcl.StdCtrls;

type
  TControlCajonesMaquilaEmbarqueSalida = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    gridMaterial: TStringGrid;
    Button1: TButton;
    Button2: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    guardar:bool;
  end;

var
  ControlCajonesMaquilaEmbarqueSalida: TControlCajonesMaquilaEmbarqueSalida;

implementation

{$R *.dfm}

procedure TControlCajonesMaquilaEmbarqueSalida.Button1Click(Sender: TObject);
begin
guardar:=true;
self.Close;
end;

procedure TControlCajonesMaquilaEmbarqueSalida.Button2Click(Sender: TObject);
begin
guardar:=false;
self.Close;
end;

procedure TControlCajonesMaquilaEmbarqueSalida.FormShow(Sender: TObject);
begin
             //TITULOS DEL STRINGGRID
             gridMaterial.Cells[0,0]:='Articulo';
             gridMaterial.Cells[1,0]:='Unidades';
             gridMaterial.ColWidths[0]:=450;
             gridMaterial.ColWidths[1]:=70;
end;

end.
