unit UControlCajonesConfig;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client,
  FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.StdCtrls, Vcl.Menus, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs, Vcl.Bind.Editors,
  Data.Bind.Components, Data.Bind.DBScope;

type
  TControlCajonesConfig = class(TForm)
    Conexion: TFDConnection;
    cbAlmacen: TComboBox;
    Label1: TLabel;
    qryAlamcen: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    StatusBar1: TStatusBar;
    cbPoliza: TComboBox;
    Label2: TLabel;
    qryPoliza: TFDQuery;
    BindSourceDB2: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    btnGuardar: TButton;
    qryAlamcenALMACEN_ID: TIntegerField;
    qryPolizaTIPO_POLIZA_ID: TIntegerField;
    qryPolizaNOMBRE: TStringField;
    qryPolizaCLAVE: TStringField;
    qryAlamcenNOMBRE: TStringField;
    qryAlamcenSIC_POLIZA: TIntegerField;
    procedure Almacenes1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesConfig: TControlCajonesConfig;

implementation

{$R *.dfm}

procedure TControlCajonesConfig.Almacenes1Click(Sender: TObject);
begin
Label1.Visible:=True;
cbAlmacen.Visible:=True;
btnGuardar.Visible:=True;
Label2.Visible:=True;
cbPoliza.Visible:=True;
end;

procedure TControlCajonesConfig.btnGuardarClick(Sender: TObject);
begin
Conexion.ExecSQL('update almacenes set sic_poliza=:poliza where almacen_id=:aid',[qryPolizaTIPO_POLIZA_ID.Value,qryAlamcenALMACEN_ID.Value]);
ShowMessage('Guardado Correctamente.');
Conexion.Commit;
end;

procedure TControlCajonesConfig.FormShow(Sender: TObject);
begin
 qryAlamcen.Open();
 qryPoliza.Open();
end;

end.
