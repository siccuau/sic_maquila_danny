unit UControlCajonesMaquilaChofer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  Vcl.ComCtrls, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Vcl.StdCtrls,
  Vcl.DBCtrls, FireDAC.DApt, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.EngExt, Vcl.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.DBScope, FireDAC.Comp.DataSet, Vcl.Menus,UControlCajonesChofer;

type
  TControlCajonesMaquilaChofer = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    btnAceptar: TButton;
    btnCancelar: TButton;
    Label1: TLabel;
    mnuChoferes: TPopupMenu;
    mnuItemAbrir: TMenuItem;
    mnuItemNuevo: TMenuItem;
    dsChofer: TDataSource;
    cbChofer: TDBLookupComboBox;
    qryChofer: TFDQuery;
    qryChoferID: TIntegerField;
    qryChoferNOMBRE: TStringField;
    qryChoferPLACAS: TStringField;
    qryChoferMARCA: TStringField;
    qryChoferMODELO: TStringField;
    Label2: TLabel;
    cbCliente: TDBLookupComboBox;
    dsCliente: TDataSource;
    qryArticulosSalida: TFDQuery;
    qryCliente: TFDQuery;
    qryArticulosSalidaCAJAS: TBCDField;
    qryArticulosSalidaARTICULO: TStringField;
    qryArticulosSalidaARTICULO_ID: TIntegerField;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mnuItemAbrirClick(Sender: TObject);
    procedure mnuItemNuevoClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    proveedor_id,almacen_id,maquila_id:integer;
  end;

var
  ControlCajonesMaquilaChofer: TControlCajonesMaquilaChofer;

implementation

{$R *.dfm}

procedure TControlCajonesMaquilaChofer.btnAceptarClick(Sender: TObject);
var
serie,folio_str,query_str,clave_proveedor,fecha,placas,productor,clave_cliente,clave_articulo,folio_maquila:string;
detalle_id,articulo_id,consecutivo,folio_id,chofer_id,moneda_id,cond_pago_id,documento_id,docto_id,dir_cli:integer;
precio_unitario,unidades:double;
begin
  try
  /// SE CREA LA REMISION  ////
    serie := Conexion.ExecSQLScalar('select serie from folios_ventas where tipo_docto = ''R''');
    consecutivo := Conexion.ExecSQLScalar('select consecutivo from folios_ventas where tipo_docto = ''R''');
    folio_id := Conexion.ExecSQLScalar('select folio_ventas_id from folios_ventas where tipo_docto = ''R''');
    //********** OBTIENE EL SIGUIENTE FOLIO DEL DOCUMENTO DESTINO Y SE ACTUALIZA LA TABLA DE FOLIOS*******
    folio_str := Format('%.*d',[9,(consecutivo)]);
    Conexion.ExecSQL('update folios_ventas set consecutivo=consecutivo+1 where folio_ventas_id=:folio_id',[folio_id]);

    //************************* SE OBTIENEN LOS DEMAS DATOS PARA DAR DE ALTA EL ENCABEZADO***************
    chofer_id := cbChofer.KeyValue;
    clave_proveedor := Conexion.ExecSQLScalar('select coalesce(clave_prov,'''') from get_clave_prov(:id)',[proveedor_id]);
    moneda_id := 1;
    cond_pago_id := Conexion.ExecSQLScalar('select first 1 cond_pago_id from clientes where cliente_id = :cid',[cbCliente.KeyValue]);
   fecha := FormatDateTime('mm/dd/yyyy', Now);
    placas:=Conexion.ExecSQLScalar('select placas from sic_choferes where id=:id',[chofer_id]);
    productor:=Conexion.ExecSQLScalar('select id from sic_productores where proveedor_id=:pid',[proveedor_id]);
    dir_cli:=Conexion.ExecSQLScalar('select dir_cli_id from dirs_clientes where cliente_id=:cid',[inttostr(cbCliente.KeyValue)]);
    clave_cliente:=Conexion.ExecSQLScalar('select clave_cliente from claves_clientes where cliente_id=:cid',[cbCliente.KeyValue]);
    folio_maquila:=Conexion.ExecSQLScalar('select folio from sic_maquila where maquila_id=:mid',[maquila_id]);
    //****************************************DA DE ALTA EL ENCABEZADO***********************************

      query_str := 'insert into doctos_ve (docto_ve_id,tipo_docto,folio,fecha,clave_cliente,'+
                   ' cliente_id,dir_cli_id,dir_consig_id,almacen_id,moneda_id,tipo_cambio,tipo_dscto,estatus,'+
                   ' sistema_origen,cond_pago_id,descripcion) values (-1,''R'','''+folio_str+''','+
                   ' '''+fecha+''','''+clave_cliente+''','''+inttostr(cbCliente.KeyValue)+''','''+inttostr(dir_cli)+''','''+inttostr(dir_cli)+''','+
                   ' '+inttostr(almacen_id)+','+inttostr(moneda_id)+',1,''P'',''P'',''VE'','+inttostr(cond_pago_id)+',''Remision creada para la maquila: '+folio_maquila+''') returning docto_ve_id';
      documento_id := Conexion.ExecSQLScalar(query_str);
      docto_id := documento_id;

       //*******************************DETALLADO DEL DOCUMENTO*********************************************
       qryArticulosSalida.First;
       while not qryArticulosSalida.Eof do
           begin
           articulo_id:=qryArticulosSalidaARTICULO_ID.Value;
           unidades:=qryArticulosSalidaCAJAS.Value;
           precio_unitario:=Conexion.ExecSQLScalar('select precio from precios_articulos where articulo_id=:aid',[qryArticulosSalidaARTICULO_ID.Value]);
           clave_articulo:=Conexion.ExecSQLScalar('select clave_articulo from claves_articulos where articulo_id=:aid',[qryArticulosSalidaARTICULO_ID.Value]);
           query_str := 'insert into doctos_ve_det (docto_ve_det_id, docto_ve_id,'+
                         ' clave_articulo,articulo_id,unidades,unidades_comprom,unidades_surt_dev,unidades_a_surtir,precio_unitario,'+
                         'pctje_dscto,dscto_art,pctje_dscto_cli,dscto_extra,pctje_dscto_vol,pctje_dscto_promo,'+
                         'precio_total_neto,pctje_comis,notas,posicion)'+
                         ' values(-1,'+
                         ' '+inttostr(documento_id)+','''+clave_articulo+''','+inttostr(articulo_id)+','+FloatToStr(unidades)+',0,0,0,'+
                         ' '+FloatToStr(precio_unitario)+',0,0,0,0,0,0,'+FloatToStr(precio_unitario)+',0,''Remision creada para la maquila: '+folio_maquila+''',1) returning docto_ve_det_id ';
           detalle_id := Conexion.ExecSQLScalar(query_str);
           qryArticulosSalida.Next;
           end;
           Conexion.ExecSQL('execute procedure calc_totales_docto_ve('+inttostr(docto_id)+',''N'')');
            Conexion.ExecSQL('execute procedure aplica_docto_ve(:id)',[docto_id]);
     Conexion.Commit;

     ShowMessage('Remision creada correcamente con el folio: '+folio_str+'.');
     self.Close;
  except
      ShowMessage('No se pudo crear la remision.');
     Conexion.Rollback;
  end;
end;

procedure TControlCajonesMaquilaChofer.btnCancelarClick(Sender: TObject);
begin
self.Close;
end;

procedure TControlCajonesMaquilaChofer.FormShow(Sender: TObject);
begin
qryChofer.Open();
cbChofer.KeyValue := cbChofer.ListSource.DataSet.FieldByName(cbChofer.KeyField).Value;
qryCliente.Open();
cbCliente.KeyValue := cbCliente.ListSource.DataSet.FieldByName(cbCliente.KeyField).Value;
      qryArticulosSalida.ParamByName('maquila_id').Value:=maquila_id;
      qryArticulosSalida.Open();
end;

procedure TControlCajonesMaquilaChofer.mnuItemAbrirClick(Sender: TObject);
var
  FormChofer : TControlCajonesChofer;
begin
  FormChofer := TControlCajonesChofer.Create(nil);
  FormChofer.Conexion := Conexion;
  FormChofer.Caption := 'Chofer - '+cbChofer.Text;
  FormChofer.chofer_id := cbChofer.KeyValue;
  FormChofer.ShowModal;
  qryChofer.SQL.Text:='select * from sic_choferes';
  qryChofer.Open();

end;

procedure TControlCajonesMaquilaChofer.mnuItemNuevoClick(Sender: TObject);
 var
  FormChofer : TControlCajonesChofer;
begin
  FormChofer := TControlCajonesChofer.Create(nil);

  FormChofer.Conexion.Params := Conexion.Params;
  FormChofer.Caption := 'Chofer - Nuevo Chofer';
  FormChofer.chofer_id := 0;
  FormChofer.ShowModal;
    qryChofer.SQL.Text:='select * from sic_choferes';
  qryChofer.Open();
end;

end.
