object ControlCajonesChofer: TControlCajonesChofer
  Left = 0
  Top = 0
  Caption = 'Chofer'
  ClientHeight = 242
  ClientWidth = 397
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 19
    Width = 44
    Height = 13
    Caption = 'Nombre: '
  end
  object Label2: TLabel
    Left = 34
    Top = 52
    Width = 34
    Height = 13
    Caption = 'Placas:'
  end
  object txtNombre: TEdit
    Left = 74
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object txtPlacas: TEdit
    Left = 74
    Top = 49
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 24
    Top = 88
    Width = 241
    Height = 129
    Caption = 'Camion/Trailer'
    TabOrder = 2
    object Label3: TLabel
      Left = 24
      Top = 43
      Width = 33
      Height = 13
      Caption = 'Marca:'
    end
    object Label4: TLabel
      Left = 19
      Top = 80
      Width = 38
      Height = 13
      Caption = 'Modelo:'
    end
    object txtMarca: TEdit
      Left = 72
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object txtModelo: TEdit
      Left = 72
      Top = 77
      Width = 121
      Height = 21
      TabOrder = 1
    end
  end
  object btnGuardar: TButton
    Left = 296
    Top = 64
    Width = 75
    Height = 49
    Caption = 'Guardar'
    TabOrder = 3
    OnClick = btnGuardarClick
  end
  object btnCancelar: TButton
    Left = 296
    Top = 131
    Width = 75
    Height = 30
    Caption = 'Cancelar'
    TabOrder = 4
    OnClick = btnCancelarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Server=localhost'
      'Protocol=TCPIP'
      'DriverID=FB')
    LoginPrompt = False
    Left = 435
    Top = 150
  end
  object qryDatos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_choferes where id=:cid')
    Left = 256
    Top = 16
    ParamData = <
      item
        Name = 'CID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryDatosID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryDatosNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 50
    end
    object qryDatosPLACAS: TStringField
      FieldName = 'PLACAS'
      Origin = 'PLACAS'
      Size = 50
    end
    object qryDatosMARCA: TStringField
      FieldName = 'MARCA'
      Origin = 'MARCA'
      Size = 50
    end
    object qryDatosMODELO: TStringField
      FieldName = 'MODELO'
      Origin = 'MODELO'
      Size = 50
    end
  end
end
