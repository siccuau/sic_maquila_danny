object FormEmbarque: TFormEmbarque
  Left = 0
  Top = 0
  Caption = 'Embarque'
  ClientHeight = 577
  ClientWidth = 740
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 558
    Width = 740
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object GroupChofer: TGroupBox
    Left = 8
    Top = 146
    Width = 337
    Height = 119
    Caption = 'Datos del Chofer'
    TabOrder = 1
    object Label4: TLabel
      Left = 60
      Top = 57
      Width = 34
      Height = 13
      Caption = 'Placas:'
    end
    object Label5: TLabel
      Left = 57
      Top = 31
      Width = 37
      Height = 13
      Caption = 'Chofer:'
    end
    object Label7: TLabel
      Left = 11
      Top = 85
      Width = 83
      Height = 13
      Caption = 'Poliza de Seguro:'
    end
    object txtPlacas: TEdit
      Left = 100
      Top = 55
      Width = 97
      Height = 21
      TabOrder = 1
    end
    object txtChofer: TEdit
      Left = 100
      Top = 28
      Width = 208
      Height = 21
      TabOrder = 0
    end
    object txtPolizaSeguro: TEdit
      Left = 100
      Top = 82
      Width = 149
      Height = 21
      TabOrder = 2
    end
  end
  object GroupDatos: TGroupBox
    Left = 8
    Top = 8
    Width = 689
    Height = 121
    Caption = 'Datos'
    TabOrder = 2
    object Label2: TLabel
      Left = 60
      Top = 56
      Width = 44
      Height = 13
      Caption = 'Almacen:'
    end
    object Label3: TLabel
      Left = 16
      Top = 82
      Width = 88
      Height = 13
      Caption = 'Cliente Embarque:'
    end
    object Label1: TLabel
      Left = 32
      Top = 27
      Width = 72
      Height = 13
      Caption = 'No. Embarque:'
    end
    object Label6: TLabel
      Left = 497
      Top = 27
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object txtEmbarque: TEdit
      Left = 110
      Top = 27
      Width = 121
      Height = 21
      Enabled = False
      TabOrder = 0
    end
    object dtpFecha: TDateTimePicker
      Left = 536
      Top = 23
      Width = 121
      Height = 21
      Date = 43777.503023194450000000
      Time = 43777.503023194450000000
      TabOrder = 1
    end
    object cmbAlmacen: TDBLookupComboBox
      Left = 110
      Top = 53
      Width = 145
      Height = 21
      KeyField = 'ALMACEN_ID'
      ListField = 'NOMBRE'
      ListSource = DSAlmacenes
      TabOrder = 2
    end
    object cmbCliente: TDBLookupComboBox
      Left = 110
      Top = 80
      Width = 281
      Height = 21
      KeyField = 'CLIENTE_ID'
      ListField = 'NOMBRE'
      ListSource = DSClientes
      TabOrder = 3
    end
  end
  object gridEtiquetas: TStringGrid
    Left = 8
    Top = 271
    Width = 697
    Height = 241
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 3
    OnDblClick = gridEtiquetasDblClick
    OnKeyPress = gridEtiquetasKeyPress
    OnSelectCell = gridEtiquetasSelectCell
    ColWidths = (
      64
      64
      64
      64
      64)
  end
  object btnGenerarPDF: TButton
    Left = 398
    Top = 518
    Width = 75
    Height = 25
    Caption = 'Generar PDF'
    TabOrder = 4
    OnClick = btnGenerarPDFClick
  end
  object btnEnviarMicrosip: TButton
    Left = 247
    Top = 518
    Width = 114
    Height = 25
    Caption = 'Enviar a Microsip'
    TabOrder = 5
    OnClick = btnEnviarMicrosipClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Server=localhost'
      'Protocol=TCPIP'
      'Database=E:\Microsip Pruebas\SAGE 2020.FDB'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 11
    Top = 520
  end
  object DSAlmacenes: TDataSource
    DataSet = QueryAlmacenes
    Left = 360
    Top = 144
  end
  object QueryAlmacenes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from almacenes')
    Left = 416
    Top = 144
    object QueryAlmacenesALMACEN_ID: TIntegerField
      FieldName = 'ALMACEN_ID'
      Origin = 'ALMACEN_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QueryAlmacenesNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 50
    end
  end
  object DSClientes: TDataSource
    DataSet = QueryClientes
    Left = 360
    Top = 208
  end
  object QueryClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 416
    Top = 208
    object QueryClientesCLIENTE_ID: TIntegerField
      FieldName = 'CLIENTE_ID'
      Origin = 'CLIENTE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QueryClientesNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Required = True
      Size = 200
    end
  end
  object reporte: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42949.704375405100000000
    ReportOptions.LastChange = 43794.386916504630000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 512
    Top = 216
    Datasets = <
      item
        DataSet = DBReporte
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 52.913420000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object txtTitulo: TfrxMemoView
          Align = baWidth
          Width = 740.409927000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Embarque de Maquila')
          ParentFont = False
          Style = 'Title'
          VAlign = vaCenter
        end
        object DSReporteMAQUILA_ID: TfrxMemoView
          Top = 26.456710000000000000
          Width = 740.787880000000000000
          Height = 26.456710000000000000
          DataSet = ControlCajonesMaquilaMaterial.DSReporte
          DataSetName = 'DSReporte'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBDataset1."FOLIO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 40.480210000000000000
        Top = 94.488250000000000000
        Width = 740.409927000000000000
        object Shape1: TfrxShapeView
          Left = 1.000000000000000000
          Top = 6.582560000000000000
          Width = 739.488250000000000000
          Height = 33.897650000000000000
        end
        object Memo8: TfrxMemoView
          Left = 20.350487960000000000
          Top = 9.559060000000000000
          Width = 146.903792200000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'ETIQUETA')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 195.931460160000000000
          Top = 10.559060000000000000
          Width = 77.380401300000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'LOCALIZACION')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 189.826840000000000000
          Top = 7.582560000000000000
          Height = 32.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line2: TfrxLineView
          Left = 278.338590000000000000
          Top = 7.582560000000000000
          Height = 32.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Memo1: TfrxMemoView
          Left = 283.464750000000000000
          Top = 10.338590000000000000
          Width = 69.821341300000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'SURTIDAS')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 359.055350000000000000
          Top = 7.338590000000000000
          Height = 32.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 196.535560000000000000
        Width = 740.409927000000000000
        DataSet = DBReporte
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object frxDBDataset1ETIQUETA: TfrxMemoView
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          DataField = 'ETIQUETA'
          DataSet = DBReporte
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBDataset1."ETIQUETA"]')
          ParentFont = False
        end
        object frxDBDataset1LOCALIZACION: TfrxMemoView
          Left = 188.976500000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          DataField = 'LOCALIZACION'
          DataSet = DBReporte
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBDataset1."LOCALIZACION"]')
          ParentFont = False
        end
        object frxDBDataset1SURTIDAS: TfrxMemoView
          Left = 279.685220000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'SURTIDAS'
          DataSet = DBReporte
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBDataset1."SURTIDAS"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 597.165740000000000000
        Width = 740.409927000000000000
        object Memo31: TfrxMemoView
          Align = baWidth
          Width = 740.409927000000000000
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo32: TfrxMemoView
          Top = 1.000000000000000000
          Height = 22.677180000000000000
          AutoWidth = True
          Memo.UTF8W = (
            '[Date] [Time]')
        end
        object Memo33: TfrxMemoView
          Align = baRight
          Left = 664.819327000000000000
          Top = 1.000000000000000000
          Width = 75.590600000000000000
          Height = 22.677180000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Page [Page#]')
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 298.677180000000000000
        Top = 275.905690000000000000
        Width = 740.409927000000000000
        object Line10: TfrxLineView
          Left = 1.000000000000000000
          Top = 0.708410000000000000
          Width = 737.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 4.000000000000000000
        end
      end
    end
  end
  object qryImprimir: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select se.folio,se.embarque_id,localizacion,etiqueta,(SELECT sum' +
        '(surtidas) FROM sic_maquila_articulos_tarima WHERE maquila_det_i' +
        'd=(select maquila_det_id from sic_maquila_det_salida where etiqu' +
        'eta=sde.etiqueta)) as surtidas from sic_det_embarque sde join'
      'sic_embarque se on sde.embarque_id=se.embarque_id'
      'where se.embarque_id=:mid'
      'order by localizacion')
    Left = 584
    Top = 168
    ParamData = <
      item
        Name = 'MID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryImprimirEMBARQUE_ID: TIntegerField
      FieldName = 'EMBARQUE_ID'
      Origin = 'EMBARQUE_ID'
      Required = True
    end
    object qryImprimirLOCALIZACION: TStringField
      FieldName = 'LOCALIZACION'
      Origin = 'LOCALIZACION'
      Size = 11
    end
    object qryImprimirETIQUETA: TStringField
      FieldName = 'ETIQUETA'
      Origin = 'ETIQUETA'
      Size = 100
    end
    object qryImprimirSURTIDAS: TLargeintField
      FieldName = 'SURTIDAS'
      Origin = 'SURTIDAS'
    end
    object qryImprimirFOLIO: TStringField
      FieldName = 'FOLIO'
      Origin = 'FOLIO'
    end
  end
  object DBReporte: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'EMBARQUE_ID=EMBARQUE_ID'
      'LOCALIZACION=LOCALIZACION'
      'ETIQUETA=ETIQUETA'
      'SURTIDAS=SURTIDAS'
      'FOLIO=FOLIO')
    DataSet = qryImprimir
    BCDToCurrency = False
    Left = 504
    Top = 160
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 584
    Top = 224
  end
  object qryEnviar: TFDQuery
    Connection = Conexion
    Left = 176
    Top = 520
  end
  object qryArticulos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_maquila_articulos_tarima where maquila_det_id=' +
        ':mid')
    Left = 544
    Top = 136
    ParamData = <
      item
        Name = 'MID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryArticulosID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object qryArticulosMAQUILA_DET_ID: TIntegerField
      FieldName = 'MAQUILA_DET_ID'
      Origin = 'MAQUILA_DET_ID'
    end
    object qryArticulosARTICULO_ID: TIntegerField
      FieldName = 'ARTICULO_ID'
      Origin = 'ARTICULO_ID'
    end
    object qryArticulosUNIDADES: TBCDField
      FieldName = 'UNIDADES'
      Origin = 'UNIDADES'
      Precision = 18
      Size = 2
    end
    object qryArticulosCLAVE_ARTICULO: TStringField
      FieldName = 'CLAVE_ARTICULO'
      Origin = 'CLAVE_ARTICULO'
    end
    object qryArticulosSURTIDAS: TIntegerField
      FieldName = 'SURTIDAS'
      Origin = 'SURTIDAS'
    end
  end
  object DSArticulos: TDataSource
    DataSet = qryArticulos
    Left = 656
    Top = 200
  end
end
