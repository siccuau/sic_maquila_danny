object ControlCajonesMaquilaChofer: TControlCajonesMaquilaChofer
  Left = 0
  Top = 0
  Caption = 'Chofer'
  ClientHeight = 219
  ClientWidth = 282
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 32
    Width = 50
    Height = 13
    Caption = 'Operador:'
  end
  object Label2: TLabel
    Left = 26
    Top = 72
    Width = 40
    Height = 13
    Caption = 'Cliente: '
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 204
    Width = 282
    Height = 15
    Panels = <
      item
        Width = 50
      end>
  end
  object btnAceptar: TButton
    Left = 32
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 132
    Top = 104
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object cbChofer: TDBLookupComboBox
    Left = 72
    Top = 29
    Width = 145
    Height = 21
    KeyField = 'ID'
    ListField = 'NOMBRE'
    ListSource = dsChofer
    PopupMenu = mnuChoferes
    TabOrder = 3
  end
  object cbCliente: TDBLookupComboBox
    Left = 72
    Top = 66
    Width = 145
    Height = 21
    KeyField = 'CLIENTE_ID'
    ListField = 'NOMBRE'
    ListSource = dsCliente
    TabOrder = 4
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Server=localhost'
      'Protocol=TCPIP'
      'DriverID=FB')
    LoginPrompt = False
    Left = 211
    Top = 62
  end
  object mnuChoferes: TPopupMenu
    Left = 4
    Top = 96
    object mnuItemAbrir: TMenuItem
      Bitmap.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000064000000640000000000000000000000000000070000
        000B0000000F0000001100000011000000110000001100000011000000110000
        00110000000F0000000C000000080000000400000001000000000000000B0000
        0055000000580000005B0000005B0000005B0000005B0000005B0000005B0000
        005B00000059000000440000002100000007000000020000000029ADD6FF29AD
        D6FF29ADD6FF29ADD6FF29ADD6FF29ADD6FF29ADD6FF29ADD6FF29ADD6FF29AD
        D6FF2398BDD8165F758E000000430000000C000000050000000129ADD6FF29AD
        D6FF60EAFEFF8EF0FEFF95F1FEFF9AF1FEFF9CF2FEFF9AF1FEFF95F1FEFF8EF0
        FEFF83EEFEFF2398BCD90000005A00000024000000080000000229ADD6FF29AD
        D6FF60EAFEFF86EFFEFF8EF0FEFF92F0FEFF94F1FEFF92F0FEFF8EF0FEFF86EF
        FEFF7CEEFEFF29ADD6FF165D7390000000450000000D0000000529ADD6FF68EB
        FEFF29ADD6FF60EAFEFF81EEFEFF86EFFEFF87EFFEFF86EFFEFF81EEFEFF7BED
        FEFF72ECFEFF68EBFEFF2398BCD90000005A000000260000000929ADD6FF5BE9
        FEFF29ADD6FF60EAFEFF71ECFEFF75EDFEFF76EDFEFF75EDFEFF71ECFEFF6BEB
        FEFF64EAFEFF5BE9FEFF29ADD6FF165F758E000000440000000C29ADD6FF4BE7
        FEFF52E8FEFF29ADD6FF60EAFEFF60EAFEFF61EAFEFF60EAFEFF5DEAFEFF59E9
        FEFF52E8FEFF4BE7FEFF4BE7FEFF249BC0D5000000510000002029ADD6FF60E9
        FDFF65EAFDFF29ADD6FF29ADD6FF29ADD6FF29ADD6FF29ADD6FF29ADD6FF29AD
        D6FF29ADD6FF29ADD6FF29ADD6FF29ADD6FF259EC4550000000729ADD6FF7FED
        FDFF81EEFEFF83EEFDFF85EEFDFF85EEFEFF86EEFDFF85EEFEFF85EEFDFF83EE
        FDFF29ADD6FF000000500000000E00000009000000070000000629ADD6FFA4F2
        FDFFA5F2FEFFA6F2FDFFA7F3FEFFA7F3FEFF29ADD6FF29ADD6FF29ADD6FF29AD
        D6FF29ADD6FF0000000B0000000A0000000A0000000B0000000A0000000329AD
        D6FFD0F8FEFFD0F8FEFFD0F8FEFF29ADD6FF0000000F0000000A000000090000
        000900000009000000090000000A0000004C0000005200000059000000010000
        000329ADD6FF29ADD6FF29ADD6FF0000000A0000000700000004000000040000
        0045000000090000000C00A200FF00A300FF009E00FF0000005B000000000000
        0001000000020000000300000003000000030000000200000002007F00FF0000
        000700000047000000490000004A00A700FF00A200FF00000057000000000000
        000000000000000000000000000000000000000000000000000100000002008B
        00FF009500FF00A100FF00A400FF0000000A00A100FF00000007000000000000
        0000000000000000000000000000000000000000000000000000000000010000
        0002000000030000000400000004000000040000000300000002}
      Caption = 'Abrir Chofer'
      OnClick = mnuItemAbrirClick
    end
    object mnuItemNuevo: TMenuItem
      Bitmap.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000064000000640000000000000000000000FFFFFF000000
        001C0000002E0000002E0000002E313440FF2C3959C70F182B740101025B0000
        0058000000520000004A000000420000002E0000001CFFFFFF00FFFFFF00FFFF
        FF00E2B899FFE2B998FFE2B899FF5E6D9DFF566692FF526192FF41649EFFCAAC
        98FFE2B898FFE2B999FFE2B998FFE2B898FFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00E3BB9BFFF8E0BEFFF8E0BEFF9B9FAEFF6A7CA9FF5874A0FF066BB0FF0374
        BFFFF8E1BEFFF8E0BFFFF8E1BEFFE3BA9AFFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00E4BD9CFFF8E1BFFFF3D6B2FFC2B8AEFF5777ACFF29C0F8FF0297D7FF0076
        D4FF0376C1FFF3D6B2FFF8E1BFFFE4BD9DFFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00E6C09FFFF8E2C1FFD9BE9DFFD9BE9EFF96A7ABFF0672C4FF24D3FFFF09B3
        EEFF0076D4FF0478C2FFF8E1C0FFE6C09FFFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00E7C3A2FFF8E3C2FFF6DDBAFFF8E3C3FFF8E3C2FFB6C5C3FF0672C4FF24D3
        FFFF25C6F6FF0076D4FF037CCAFFE8C3A2FFA9A7A801FFFFFF00FFFFFF00FFFF
        FF00EAC6A4FFF8E4C4FFDCC2A4FFDCC4A6FFDCC4A6FFDCC5A6FFA1AEAFFF0672
        C4FF98ECFFFF25C6F6FF0076D4FF7E7E75FF3D45A33CFFFFFF00FFFFFF00FFFF
        FF00ECCAA8FFF8E5C6FFF9E4C6FFF9E5C5FFF9E4C6FFF8E4C6FFF7E3C5FFBCC8
        C6FF0672C4FF96EAFEFFB7B7ACFF6E6F61FF7E7E75FFFFFFFF00FFFFFF00FFFF
        FF00EECDABFFF8E6C7FFDFC8AAFFDEC8AAFFDFC8ABFFDFC7ABFFDBC3A6FFDAC2
        A4FFABB0ABFF8E8F84FFE0E2D3FF89897FFF1765DEFF1765DEEAFFFFFF00FFFF
        FF00F0D0ADFFF9E6C9FFF8E6C9FFF9E6C9FFF9E6C9FFF7E5C9FFF0D9BDFFE9CF
        B3FFE8CEB2FFCCB9AFFF89897FFF5496EAFF4285E0FF1765DED3FFFFFF00FFFF
        FF00F1D3B1FFF9E7CBFFE2CCB0FFE2CBAFFFE1CCAFFFE1CAAFFFD7BEA1FFD5BA
        9CFFD4B99DFFD4B99DFFB6AFB4FF7492C4FF2C6FD286FFFFFF00FFFFFF00FFFF
        FF00F3D7B4FFF9E7CCFFF8E7CCFFF9E8CCFFF9E8CCFFF7E5CAFFE7CFB5FFF2DE
        C6FFF3E0C8FFF7E8D3FFF4E1C9C5EFD0AEFFF7EFF002F6EDEE01FFFFFF00FFFF
        FF00F4D9B5FFF9E8CDFFE4CFB4FFE4CFB4FFE4CFB4FFE3CEB3FFDDC6A9FFF9E8
        CEFFF9E8CEFFF9E8CDCCF1D3B1FFEFD0AE38FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F6DBB8FFF9E9CFFFF9E9CEFFF9E8CEFFF8E8CEFFF6E5CCFFEEDABFFFF9E8
        CEFFF9E8CEDEF4D9B5FFEFD0AE38FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F7DDBAFFF9E9CFFFF9E9CFFFF9E9CFFFF9E9CFFFF7E6CDFFEDD8BDFFF9E8
        CEFFF7DDB8FFEFD0AE38FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8DFBBFFF8DFBBFFF8DFBBFFF8DFBBFFF8DFBBFFF8DFBBFFF8DFBBFFF8DF
        BBFFEFD0AE38FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      Caption = 'Nuevo Chofer'
      OnClick = mnuItemNuevoClick
    end
  end
  object dsChofer: TDataSource
    DataSet = qryChofer
    Left = 120
    Top = 96
  end
  object qryChofer: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_choferes')
    Left = 232
    Top = 16
    object qryChoferID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryChoferNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 50
    end
    object qryChoferPLACAS: TStringField
      FieldName = 'PLACAS'
      Origin = 'PLACAS'
      Size = 50
    end
    object qryChoferMARCA: TStringField
      FieldName = 'MARCA'
      Origin = 'MARCA'
      Size = 50
    end
    object qryChoferMODELO: TStringField
      FieldName = 'MODELO'
      Origin = 'MODELO'
      Size = 50
    end
  end
  object dsCliente: TDataSource
    DataSet = qryCliente
    Left = 88
    Top = 144
  end
  object qryArticulosSalida: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select sum(unidades) as cajas, a.nombre as articulo,a.ARTICULO_I' +
        'D'
      'from sic_maquila_articulos_tarima sat join'
      
        'sic_maquila_det_salida ds on ds.maquila_det_id = sat.maquila_det' +
        '_id join'
      'articulos a on a.articulo_id = sat.articulo_id'
      
        'where ds.maquila_id = :maquila_id group by a.nombre,a.ARTICULO_I' +
        'D')
    Left = 232
    Top = 136
    ParamData = <
      item
        Name = 'MAQUILA_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryArticulosSalidaCAJAS: TBCDField
      FieldName = 'CAJAS'
      Origin = 'CAJAS'
      Precision = 18
      Size = 2
    end
    object qryArticulosSalidaARTICULO: TStringField
      FieldName = 'ARTICULO'
      Origin = 'ARTICULO'
      Required = True
      Size = 200
    end
    object qryArticulosSalidaARTICULO_ID: TIntegerField
      FieldName = 'ARTICULO_ID'
      Origin = 'ARTICULO_ID'
      Required = True
    end
  end
  object qryCliente: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from clientes')
    Left = 144
    Top = 152
  end
end
