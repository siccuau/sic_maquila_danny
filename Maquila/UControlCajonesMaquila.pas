unit UControlCajonesMaquila;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, Vcl.ComCtrls, Data.DB, FireDAC.Comp.Client, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, UControlCajonesEmpacadorasLista, UControlCajonesMaquilaEtiquetas, UControlCajonesMaquilaRegistro,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,System.Character,
  FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait, Vcl.ExtCtrls, frxClass, frxDBSet,
  frxExportPDF;

type
  TControlCajonesMaquila = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    DBMaquilas: TDBGrid;
    btnEditarMaquila: TButton;
    btnMaquilaNueva: TButton;
    btnEmpacadoras: TButton;
    btnEtiquetas: TButton;
    QueryMaquilas: TFDQuery;
    dsMaquilas: TDataSource;
    btnEliminar: TButton;
    RadioGroup1: TRadioGroup;
    QueryMaquilasFOLIO: TStringField;
    QueryMaquilasPROVEEDOR: TStringField;
    QueryMaquilasFECHA: TDateField;
    GroupBox1: TGroupBox;
    txtBuscarFolio: TEdit;
    btnBuscarFolio: TButton;
    RadioGroup2: TRadioGroup;
    QueryMaquilasMAQUILA_ID: TIntegerField;
    qryImprimir: TFDQuery;
    Reporte: TfrxReport;
    DSReporte: TfrxDBDataset;
    cbReporte: TComboBox;
    btnImprimir: TButton;
    GroupBox2: TGroupBox;
    QueryImprimir: TFDQuery;
    DSReporteCajones: TfrxDBDataset;
    ReporteCajones: TfrxReport;
    QueryEtiquetas: TFDQuery;
    DSEtiquetas: TfrxDBDataset;
    Etiquetas: TfrxReport;
    QueryProductos: TFDQuery;
    frxPDFExport1: TfrxPDFExport;
    QueryEntrada: TFDQuery;
    DSEntrada: TfrxDBDataset;
    ReporteMaquila: TfrxReport;
    FDQuery1: TFDQuery;
    FDQuery1COD_BARRAS: TStringField;
    FDQuery1Articulo: TStringField;
    FDQuery1PESO: TBCDField;
    FDQuery1NUM_TARIMA: TStringField;
    FDQuery1CAJAS: TIntegerField;
    FDQuery1ETIQUETA: TStringField;
    ReporteEmbarque: TfrxReport;
    DSReporteMaquila: TfrxDBDataset;
    frxDBDataset1: TfrxDBDataset;
    DBReporte: TfrxDBDataset;
    qryImprimirEmbarque: TFDQuery;
    qryImprimirEMBARQUE_ID: TIntegerField;
    qryImprimirETIQUETA: TStringField;
    qryImprimirLOCALIZACION: TStringField;
    qryImprimirSURTIDAS: TIntegerField;
    qryImprimirEMBARQUE_ID_1: TIntegerField;
    qryImprimirFOLIO: TStringField;
    qryImprimirALMACEN_ID: TIntegerField;
    qryImprimirCLIENTE_ID: TIntegerField;
    qryImprimirFECHA: TDateField;
    qryImprimirCHOFER: TStringField;
    qryImprimirPLACAS: TStringField;
    qryImprimirPOLIZA_DE_SEGURO: TStringField;
    procedure btnEmpacadorasClick(Sender: TObject);
    procedure btnEtiquetasClick(Sender: TObject);
    procedure btnMaquilaNuevaClick(Sender: TObject);
    procedure btnEditarMaquilaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure btnBuscarFolioClick(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesMaquila: TControlCajonesMaquila;

implementation

{$R *.dfm}

function GetSerie(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsLetter(folio[i]))
    then Result := Result+folio[i]
    else exit;
  end;
end;

function GetNumFolio(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsNumber(folio[i]))
    then Result := Result+folio[i];
  end;
end;


procedure TControlCajonesMaquila.btnBuscarFolioClick(Sender: TObject);
begin
txtBuscarFolio.Text:=AnsiUpperCase(txtBuscarFolio.Text);
 QueryMaquilas.SQL.Text:='select sm.maquila_id,sm.folio, p.nombre as proveedor, sm.fecha, sm.caracteristica'+
' from sic_maquila sm join'+
' proveedores p on sm.proveedor_id=p.proveedor_id'+
' where (upper(sm.folio) like ''%'+txtBuscarFolio.Text+'%'' or upper(p.nombre) like ''%'+txtBuscarFolio.Text+'%'' or upper(sm.fecha) like ''%'+txtBuscarFolio.Text+'%'' or upper(sm.caracteristica) like ''%'+txtBuscarFolio.Text+'%'')';
 QueryMaquilas.Open();
end;

procedure TControlCajonesMaquila.btnEditarMaquilaClick(Sender: TObject);
var
  FormMaquila : TControlCajonesMaquilaRegistro;
begin
  //********SI NO ESTA SELECCIONADA ALGUNA MAQUILA********
  if QueryMaquilas.FieldByName('maquila_id').Asstring = '' then
  begin
    showmessage('Seleccione una maquila primero por favor');
  end
  else
  //******SI YA ESTA SELECCIONADA LA MAQUILA************
  begin
    FormMaquila := TControlCajonesMaquilaRegistro.Create(nil);
    FormMaquila.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
    FormMaquila.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
    FormMaquila.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
    FormMaquila.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
    FormMaquila.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
    FormMaquila.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
    FormMaquila.operacion := 1;
    FormMaquila.btnDetalleSalida.Enabled := True;
    FormMaquila.btnDetalleEntrada.Enabled := True;
    FormMaquila.maquila_id := QueryMaquilas.FieldByName('maquila_id').AsInteger;
    FormMaquila.Caption := 'Editar Maquila';
    FormMaquila.txtFolioMaquila.Text := QueryMaquilas.FieldByName('folio').AsString;
    FormMaquila.cbAlmacenesEntrada.KeyValue:=Conexion.ExecSQLScalar('select almacen_entrada_id from sic_maquila where maquila_id=:mid',[QueryMaquilas.FieldByName('maquila_id').AsInteger]);
    FormMaquila.cbAlmacenesSalida.KeyValue:=Conexion.ExecSQLScalar('select almacen_salida_id from sic_maquila where maquila_id=:mid',[QueryMaquilas.FieldByName('maquila_id').AsInteger]);
                //VERIFICAR SI LA MAQUILA ESTA CERRADA
         if conexion.ExecSQLScalar('select estatus from sic_maquila where maquila_id='+QueryMaquilas.FieldByName('maquila_id').Asstring+'')='C' then
    begin
       FormMaquila.btnLiberar.Enabled:=false;
       FormMaquila.btnGuardar.Enabled:=false;
       FormMaquila.cbProveedores.Enabled:=false;
       FormMaquila.dtpFecha.Enabled:=false;
       FormMaquila.cbAlmacenesEntrada.Enabled:=false;
       FormMaquila.cbAlmacenesSalida.Enabled:=false;
    end;
    FormMaquila.ShowModal;
  end;

end;

procedure TControlCajonesMaquila.btnEliminarClick(Sender: TObject);
begin
conexion.ExecSQL('delete from sic_maquila where maquila_id='+DBMaquilas.SelectedField.Text+'');
DBMaquilas.DataSource.DataSet.Refresh;
end;

procedure TControlCajonesMaquila.btnEmpacadorasClick(Sender: TObject);
var
  FormEmpacadoras : TControlCajonesEmpacadorasLista;
begin
  //*************PANTALLA DE EMPACADORES***************
  FormEmpacadoras := TControlCajonesEmpacadorasLista.Create(nil);
  FormEmpacadoras.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormEmpacadoras.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormEmpacadoras.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormEmpacadoras.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormEmpacadoras.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';


  FormEmpacadoras.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormEmpacadoras.ShowModal;
end;

procedure TControlCajonesMaquila.btnMaquilaNuevaClick(Sender: TObject);
var
  FormMaquila : TControlCajonesMaquilaRegistro;
begin
  //********MAQUILA NUEVA********************
  FormMaquila := TControlCajonesMaquilaRegistro.Create(nil);
  FormMaquila.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormMaquila.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormMaquila.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormMaquila.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormMaquila.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormMaquila.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormMaquila.operacion := 0;
  FormMaquila.btnDetalleSalida.Enabled := False;
  FormMaquila.btnDetalleEntrada.Enabled := False;
  FormMaquila.btnLiberar.Enabled:=False;
  //FormMaquila.txtTarimasSalida.Enabled:=true;
  FormMaquila.Caption := 'Nueva Maquila';
  FormMaquila.ShowModal;
  DBMaquilas.DataSource.DataSet.Refresh;
end;

procedure TControlCajonesMaquila.FormShow(Sender: TObject);
begin
  //*****************SE CREA LA TABLA DE EMPACADORAS AL INGRESAR A LA PANTALLA PRINCIPAL*************
  QueryMaquilas.Open;
  Conexion.ExecSQL( 'execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_EMPACADORAS'')) then'+
    ' BEGIN'+
        ' execute statement ''create table SIC_EMPACADORAS ( id integer,nombre VARCHAR(20), pagoxunidad double precision, consec integer);'';'+
        ' execute statement ''CREATE GENERATOR sic_id_empacadora;'';'+
        ' execute statement ''set GENERATOR sic_id_empacadora TO 0;'';'+
        ' execute statement ''CREATE TRIGGER SIC_empacadoras_BI0 FOR SIC_empacadoras'+
                                ' ACTIVE BEFORE INSERT POSITION 0'+
                                ' AS'+
                                ' BEGIN'+
                                    ' if (NEW.ID is NULL) then NEW.ID = GEN_ID(sic_id_empacadora, 1);'+
                                ' END'';'+
    ' END end');
 dsMaquilas.DataSet.Refresh;
 DBMaquilas.Refresh;

end;

procedure TControlCajonesMaquila.RadioGroup1Click(Sender: TObject);
var
orden:string;
begin
if RadioGroup2.ItemIndex=0 then
  begin
  orden:='ASC';
  end
   else
   orden:='DESC';
  case RadioGroup1.ItemIndex of
  0:QueryMaquilas.SQL.Text:='select sm.maquila_id,sm.folio, p.nombre as proveedor, sm.fecha, sm.caracteristica'+
                            ' from sic_maquila sm join'+
                            ' proveedores p on sm.proveedor_id=p.proveedor_id'+
                            ' order by sm.folio '+orden;

  1:QueryMaquilas.SQL.Text:='select sm.maquila_id,sm.folio, p.nombre as proveedor, sm.fecha, sm.caracteristica'+
                            ' from sic_maquila sm join'+
                            ' proveedores p on sm.proveedor_id=p.proveedor_id'+
                            ' order by p.nombre '+orden;

  2:QueryMaquilas.SQL.Text:='select sm.maquila_id,sm.folio, p.nombre as proveedor, sm.fecha, sm.caracteristica'+
                            ' from sic_maquila sm join'+
                            ' proveedores p on sm.proveedor_id=p.proveedor_id'+
                            ' order by sm.fecha '+orden;
  end;
 QueryMaquilas.Open();
end;

procedure TControlCajonesMaquila.RadioGroup2Click(Sender: TObject);
begin
RadioGroup1Click(Sender);
end;

procedure TControlCajonesMaquila.btnEtiquetasClick(Sender: TObject);
var
  FormEtiquetas:TControlCajonesMaquilaEtiquetas;
begin
  //****************SE ABRE LA PANTALLA DE ETIQUETAS*************
  FormEtiquetas := TControlCajonesMaquilaEtiquetas.Create(nil);
  FormEtiquetas.Conexion.Params.Values['Database'] := Conexion.Params.Values['Database'];
  FormEtiquetas.Conexion.Params.Values['Server'] := Conexion.Params.Values['Server'];
  FormEtiquetas.Conexion.Params.Values['User_Name'] := Conexion.Params.Values['User_Name'];
  FormEtiquetas.Conexion.Params.Values['Password'] := Conexion.Params.Values['Password'];
  FormEtiquetas.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';

  FormEtiquetas.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormEtiquetas.ShowModal;
end;

procedure TControlCajonesMaquila.btnImprimirClick(Sender: TObject);
var
  dialog:TSaveDialog;
begin
case cbReporte.ItemIndex of

0:begin //Maquila
  QueryImprimir.SQL.Text:='select sm.folio,sm.fecha,'+
          ' case when upper(sm.estatus)=''C'' Then ''Cerrada'''+
          ' when upper(sm.estatus)=''A'' Then ''Abierta'''+
          ' end as Estatus,sm.caracteristica,'+
          ' (select nombre from almacenes where almacen_id='+
          ' (select almacen_entrada_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Entrada",'+
          ' (select nombre from almacenes where almacen_id='+
          ' (select almacen_salida_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Salida",'+
          ' (select nombre from proveedores where proveedor_id='+
          ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Proveedor",'+
          ' (select nombre from sic_productores where proveedor_id='+
          ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Productor"'+
          ' from sic_maquila sm where maquila_id=:folio';
          QueryImprimir.ParamByName('folio').Value:=QueryMaquilas.FieldByName('maquila_id').AsInteger;
          QueryImprimir.Open;
          QueryEntrada.SQL.Text:='select cod_barras,(select nombre from articulos where articulo_id=cc.articulo_id) as "Articulo",'+
            ' peso from claves_cajones cc join sic_maquila sm on cc.maquila_id=sm.maquila_id'+
            ' where sm.maquila_id=:folio';
          fdQuery1.ParamByName('folio').Value:=QueryMaquilas.FieldByName('maquila_id').AsInteger;
          fdQuery1.Open;
          ReporteMaquila.PrepareReport(True);
          ReporteMaquila.ShowReport(true);
  end;

1:begin //MATERIAL DE EMPAQUE
qryImprimir.SQL.Text:='select (select folio from sic_maquila where maquila_id=:mid),nombre as articulo,'+
                      ' sum(unidades) as unidades,'+
                              ' articulo_id'+
                      ' from (select  sat.unidades as cajas,'+
                              ' com.nombre,'+
                              ' (sat.unidades*jd.unidades) as unidades,'+
                             ' com.articulo_id'+
                      ' from sic_maquila_articulos_tarima sat join'+
                      ' sic_maquila_det_salida ds on ds.maquila_det_id = sat.maquila_det_id join'+
                      ' articulos a on a.articulo_id = sat.articulo_id  join'+
                      ' juegos_det jd on jd.articulo_id = a.articulo_id join'+
                      ' articulos com on com.articulo_id=jd.componente_id'+
                      ' where ds.maquila_id = :mid )'+
                      ' group by nombre,articulo_id'+
                      ' order by nombre';
    qryImprimir.ParamByName('mid').Value:=QueryMaquilas.FieldByName('maquila_id').AsInteger;
    qryImprimir.Open;
    reporte.PrepareReport(True);
    Reporte.Print;
    Reporte.ShowReport(true);
  end;
end;


end;

end.
