unit UControlCajonesReportes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ExtCtrls, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, frxClass, frxDBSet,
  FireDAC.Comp.DataSet;

type
  TControlCajonesReportes = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    rgReportes: TRadioGroup;
    txtFolio: TEdit;
    DateTimePicker1: TDateTimePicker;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    DateTimePicker2: TDateTimePicker;
    btnVistaPrevia: TButton;
    btnImprimir: TButton;
    QueryImprimir: TFDQuery;
    DSReporteMaquila: TfrxDBDataset;
    ReporteMaquila: TfrxReport;
    QueryImprimirFOLIO: TStringField;
    QueryImprimirFECHA: TDateField;
    QueryImprimirESTATUS: TStringField;
    QueryImprimirCARACTERISTICA: TStringField;
    QueryImprimirAlmacenEntrada: TStringField;
    QueryImprimirAlmacenSalida: TStringField;
    QueryImprimirProveedor: TStringField;
    QueryImprimirProductor: TStringField;
    QueryEntrada: TFDQuery;
    DSEntrada: TfrxDBDataset;
    DSSalida: TfrxDBDataset;
    QuerySalida: TFDQuery;
    GroupBox1: TGroupBox;
    txtFolioMaterial: TEdit;
    Button1: TButton;
    Button2: TButton;
    StaticText3: TStaticText;
    DSMaterial: TfrxDBDataset;
    ReporteMaterial: TfrxReport;
    FDQuery1: TFDQuery;
    FDQuery1COD_BARRAS: TStringField;
    FDQuery1Articulo: TStringField;
    FDQuery1PESO: TBCDField;
    FDQuery1NUM_TARIMA: TStringField;
    FDQuery1CAJAS: TIntegerField;
    FDQuery1ETIQUETA: TStringField;
    frxDBDataset1: TfrxDBDataset;
    ReporteMaquilaFecha: TfrxReport;
    QueryMaterial: TFDQuery;
    QueryMaterialNOMBRE: TStringField;
    QueryMaterialUNIDADES: TIntegerField;
    QueryMaterialFOLIO: TStringField;
    QueryMaterialFECHA: TDateField;
    procedure rgReportesClick(Sender: TObject);
    procedure btnVistaPreviaClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesReportes: TControlCajonesReportes;

implementation

{$R *.dfm}

procedure TControlCajonesReportes.btnImprimirClick(Sender: TObject);
begin
 case rgReportes.ItemIndex of
  0:begin
     if txtFolio.Text<>'' then
      begin
        if Conexion.ExecSQLScalar('select count(folio) from sic_maquila where folio=:folio',[txtFolio.Text])=0 then
        begin
        ShowMessage('La maquila con el folio '+txtFolio.Text+' no existe.');
        end
        else
        begin
         QueryImprimir.SQL.Text:='select sm.folio,sm.fecha,'+
          ' case when upper(sm.estatus)=''C'' Then ''Cerrada'''+
          ' when upper(sm.estatus)=''A'' Then ''Abierta'''+
          ' end as Estatus,sm.caracteristica,'+
          ' (select nombre from almacenes where almacen_id='+
          ' (select almacen_entrada_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Entrada",'+
          ' (select nombre from almacenes where almacen_id='+
          ' (select almacen_salida_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Salida",'+
          ' (select nombre from proveedores where proveedor_id='+
          ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Proveedor",'+
          ' (select nombre from sic_productores where proveedor_id='+
          ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Productor"'+
          ' from sic_maquila sm where folio=:folio';
          QueryImprimir.ParamByName('folio').Value:=txtFolio.Text;
          QueryImprimir.Open;
                    fdQuery1.ParamByName('folio').Value:=txtFolio.Text;
          fdQuery1.Open;
          ReporteMaquila.PrepareReport(True);
          ReporteMaquila.Print;
          ReporteMaquila.ShowReport(true);
          end;
       end
       else
       begin
        ShowMessage('El Folio no puede estar vacio.');
       end;
     end;

  1:begin
    QueryImprimir.SQL.Text:='select sm.folio,sm.fecha,'+
                ' case when upper(sm.estatus)=''C'' Then ''Cerrada'''+
                ' when upper(sm.estatus)=''A'' Then ''Abierta'''+
                ' end as Estatus,sm.caracteristica,'+
                ' (select nombre from almacenes where almacen_id='+
                ' (select almacen_entrada_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Entrada",'+
                ' (select nombre from almacenes where almacen_id='+
                ' (select almacen_salida_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Salida",'+
                ' (select nombre from proveedores where proveedor_id='+
                ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Proveedor",'+
                ' (select nombre from sic_productores where proveedor_id='+
                ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Productor"'+
                ' from sic_maquila sm where fecha between :fecha_ini and :fecha_fin';
          QueryImprimir.ParamByName('fecha_ini').Value:=(formatdatetime('mm/dd/yyyy',DateTimePicker1.Date));
          QueryImprimir.ParamByName('fecha_fin').Value:=(formatdatetime('mm/dd/yyyy',DateTimePicker2.Date));
          QueryImprimir.Open;
          ReporteMaquilaFecha.PrepareReport(True);
          ReporteMaquilaFecha.Print;
          ReporteMaquilaFecha.ShowReport(true);
    end;
  end;
end;

procedure TControlCajonesReportes.btnVistaPreviaClick(Sender: TObject);
begin
  case rgReportes.ItemIndex of
  0:begin
     if txtFolio.Text<>'' then
      begin
        if Conexion.ExecSQLScalar('select count(folio) from sic_maquila where folio=:folio',[txtFolio.Text])=0 then
        begin
        ShowMessage('La maquila con el folio '+txtFolio.Text+' no existe.');
        end
        else
        begin
         QueryImprimir.SQL.Text:='select sm.folio,sm.fecha,'+
          ' case when upper(sm.estatus)=''C'' Then ''Cerrada'''+
          ' when upper(sm.estatus)=''A'' Then ''Abierta'''+
          ' end as Estatus,sm.caracteristica,'+
          ' (select nombre from almacenes where almacen_id='+
          ' (select almacen_entrada_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Entrada",'+
          ' (select nombre from almacenes where almacen_id='+
          ' (select almacen_salida_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Salida",'+
          ' (select nombre from proveedores where proveedor_id='+
          ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Proveedor",'+
          ' (select nombre from sic_productores where proveedor_id='+
          ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Productor"'+
          ' from sic_maquila sm where folio=:folio';
          QueryImprimir.ParamByName('folio').Value:=txtFolio.Text;
          QueryImprimir.Open;
          QueryEntrada.SQL.Text:='select cod_barras,(select nombre from articulos where articulo_id=cc.articulo_id) as "Articulo",'+
            ' peso from claves_cajones cc join sic_maquila sm on cc.maquila_id=sm.maquila_id'+
            ' where sm.folio=:folio';
          fdQuery1.ParamByName('folio').Value:=txtFolio.Text;
          fdQuery1.Open;
       //   QuerySalida.SQL.Text:='select smds.num_tarima,smds.cajas,smds.etiqueta from sic_maquila_det_salida smds'+
       //   ' join sic_maquila sm on smds.maquila_id=sm.maquila_id where sm.folio=:folio';
       //   QuerySalida.ParamByName('folio').Value:=txtFolio.Text;
       //   QuerySalida.Open;
          ReporteMaquila.PrepareReport(True);
          //Reporte.Print;
          ReporteMaquila.ShowReport(true);
          end;
       end
       else
       begin
        ShowMessage('El Folio no puede estar vacio.');
       end;
     end;

  1:begin
    QueryImprimir.SQL.Text:='select sm.folio,sm.fecha,'+
                ' case when upper(sm.estatus)=''C'' Then ''Cerrada'''+
                ' when upper(sm.estatus)=''A'' Then ''Abierta'''+
                ' end as Estatus,sm.caracteristica,'+
                ' (select nombre from almacenes where almacen_id='+
                ' (select almacen_entrada_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Entrada",'+
                ' (select nombre from almacenes where almacen_id='+
                ' (select almacen_salida_id from sic_maquila where maquila_id=sm.maquila_id)) as "Almacen Salida",'+
                ' (select nombre from proveedores where proveedor_id='+
                ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Proveedor",'+
                ' (select nombre from sic_productores where proveedor_id='+
                ' (select proveedor_id from sic_maquila where maquila_id=sm.maquila_id)) as "Productor"'+
                ' from sic_maquila sm where fecha between :fecha_ini and :fecha_fin';
          QueryImprimir.ParamByName('fecha_ini').Value:=(formatdatetime('mm/dd/yyyy',DateTimePicker1.Date));
          QueryImprimir.ParamByName('fecha_fin').Value:=(formatdatetime('mm/dd/yyyy',DateTimePicker2.Date));
          QueryImprimir.Open;
          ReporteMaquilaFecha.PrepareReport(True);
          //ReporteMaquila.Print;
          ReporteMaquilaFecha.ShowReport(true);
    end;
  end;
end;

procedure TControlCajonesReportes.Button1Click(Sender: TObject);
begin
    QueryMaterial.SQL.Text:='select (select nombre from articulos where articulo_id=sme.articulo_id),'+
                            ' sme.unidades,sm.folio,sm.fecha from sic_material_empaque sme'+
                            ' join sic_maquila sm on sme.maquila_id=sm.maquila_id'+
                            ' where sm.folio=:folio';
    QueryMaterial.ParamByName('folio').Value:=txtFolioMaterial.Text;
    QueryMaterial.Open;
    ReporteMaterial.PrepareReport(True);
    ReporteMaterial.ShowReport(true);
end;

procedure TControlCajonesReportes.Button2Click(Sender: TObject);
begin
    QueryMaterial.SQL.Text:='select (select nombre from articulos where articulo_id=sme.articulo_id),'+
                            ' sme.unidades,sm.folio,sm.fecha from sic_material_empaque sme'+
                            ' join sic_maquila sm on sme.maquila_id=sm.maquila_id'+
                            ' where sm.folio=:folio';
    QueryMaterial.ParamByName('folio').Value:=txtFolioMaterial.Text;
    QueryMaterial.Open;
    ReporteMaterial.PrepareReport(True);
    ReporteMaterial.Print;
    ReporteMaterial.ShowReport(true);
end;

procedure TControlCajonesReportes.rgReportesClick(Sender: TObject);
begin
btnVistaPrevia.Visible:=true;
btnImprimir.Visible:=true;
  if rgReportes.ItemIndex=0 then
  begin
    txtFolio.Visible:=true;
    StaticText1.Visible:=false;
    StaticText2.Visible:=false;
    DateTimePicker1.Visible:=false;
    DateTimePicker2.Visible:=false;
  end
  else
  begin
    txtFolio.Visible:=false;
    StaticText1.Visible:=true;
    StaticText2.Visible:=true;
    DateTimePicker1.Visible:=true;
    DateTimePicker2.Visible:=true;
  end;
end;

end.
