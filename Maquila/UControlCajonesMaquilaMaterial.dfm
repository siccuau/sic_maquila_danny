object ControlCajonesMaquilaMaterial: TControlCajonesMaquilaMaterial
  Left = 0
  Top = 0
  Caption = 'Liberaci'#243'n de Maquila'
  ClientHeight = 671
  ClientWidth = 960
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 160
    Top = 32
    Width = 114
    Height = 35
    Caption = 'Entrada'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 656
    Top = 32
    Width = 88
    Height = 35
    Caption = 'Salida'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 312
    Top = 253
    Width = 308
    Height = 35
    Caption = 'Material de Empaque'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 95
    Top = 236
    Width = 59
    Height = 16
    Caption = 'Total KGs'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 549
    Top = 232
    Width = 71
    Height = 16
    Caption = 'Total Cajas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 537
    Top = 448
    Width = 73
    Height = 16
    Caption = 'Total Costo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 652
    Width = 960
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object gridEntrada: TDBGrid
    Left = 8
    Top = 73
    Width = 457
    Height = 152
    DataSource = DSLlenadoEntrada
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object gridSalida: TDBGrid
    Left = 495
    Top = 74
    Width = 457
    Height = 152
    DataSource = DSLlenadoSalida
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object btnImprimir: TButton
    Left = 688
    Top = 320
    Width = 75
    Height = 42
    Caption = 'Imprimir'
    Enabled = False
    TabOrder = 3
    OnClick = btnImprimirClick
  end
  object btnCerrar: TButton
    Left = 688
    Top = 376
    Width = 75
    Height = 41
    Caption = 'Liberar'
    TabOrder = 4
    OnClick = btnCerrarClick
  end
  object gridMaterial: TStringGrid
    Left = 184
    Top = 294
    Width = 489
    Height = 148
    ColCount = 2
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    TabOrder = 5
  end
  object txtTotal: TEdit
    Left = 24
    Top = 231
    Width = 65
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    ReadOnly = True
    TabOrder = 6
  end
  object txtTotalCajas: TEdit
    Left = 495
    Top = 231
    Width = 49
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    ReadOnly = True
    TabOrder = 7
  end
  object DBGrid3: TDBGrid
    Left = 184
    Top = 294
    Width = 489
    Height = 145
    DataSource = DSMaterial
    TabOrder = 8
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object txtTotalCosto: TEdit
    Left = 616
    Top = 445
    Width = 49
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    ReadOnly = True
    TabOrder = 9
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Server=localhost'
      'Protocol=TCPIP'
      'Database=E:\Microsip Pruebas\SIC 2019.FDB'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 781
    Top = 254
  end
  object DSLlenadoEntrada: TDataSource
    DataSet = qryLlenadoEntrada
    Left = 40
    Top = 344
  end
  object qryLlenadoSalida: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select sum(unidades) as cajas, a.nombre as articulo,a.ARTICULO_I' +
        'D'
      'from sic_maquila_articulos_tarima sat join'
      
        'sic_maquila_det_salida ds on ds.maquila_det_id = sat.maquila_det' +
        '_id join'
      'articulos a on a.articulo_id = sat.articulo_id'
      
        'where ds.maquila_id = :maquila_id group by a.nombre,a.ARTICULO_I' +
        'D')
    Left = 832
    Top = 248
    ParamData = <
      item
        Name = 'MAQUILA_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryLlenadoSalidaARTICULO: TStringField
      DisplayWidth = 55
      FieldName = 'ARTICULO'
      Origin = 'ARTICULO'
      Required = True
      Size = 200
    end
    object qryLlenadoSalidaCAJAS: TBCDField
      DisplayWidth = 10
      FieldName = 'CAJAS'
      Origin = 'CAJAS'
      Precision = 18
      Size = 2
    end
    object qryLlenadoSalidaARTICULO_ID: TIntegerField
      FieldName = 'ARTICULO_ID'
      Origin = 'ARTICULO_ID'
      Required = True
      Visible = False
    end
  end
  object DSLlenadoSalida: TDataSource
    DataSet = qryLlenadoSalida
    Left = 896
    Top = 240
  end
  object qryMaterial: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select nombre as articulo,'
      'sum(unidades) as unidades,'
      '        articulo_id,costo_ultima_compra'
      
        'from (select (select costo_ultima_compra from get_ultcom_art(com' +
        '.articulo_id)),sat.unidades as cajas,'
      '        com.nombre,'
      '        (sat.unidades*jd.unidades) as unidades,'
      '       com.articulo_id'
      'from sic_maquila_articulos_tarima sat join'
      
        'sic_maquila_det_salida ds on ds.maquila_det_id = sat.maquila_det' +
        '_id join'
      'articulos a on a.articulo_id = sat.articulo_id  join'
      'juegos_det jd on jd.articulo_id = a.articulo_id join'
      'articulos com on com.articulo_id=jd.componente_id'
      'where ds.maquila_id = :maquila_id )'
      'group by nombre,articulo_id,costo_ultima_compra'
      'order by nombre')
    Left = 216
    Top = 248
    ParamData = <
      item
        Name = 'MAQUILA_ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryMaterialARTICULO: TStringField
      DisplayLabel = 'Articulo'
      DisplayWidth = 50
      FieldName = 'ARTICULO'
      Origin = 'ARTICULO'
      Required = True
      Size = 200
    end
    object qryMaterialUNIDADES: TFMTBCDField
      DisplayLabel = 'Unidades'
      DisplayWidth = 5
      FieldName = 'UNIDADES'
      Origin = 'UNIDADES'
      Precision = 18
      Size = 7
    end
    object qryMaterialARTICULO_ID: TIntegerField
      FieldName = 'ARTICULO_ID'
      Origin = 'ARTICULO_ID'
      Required = True
      Visible = False
    end
    object qryMaterialCOSTO_ULTIMA_COMPRA: TFMTBCDField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Costo'
      DisplayWidth = 5
      FieldName = 'COSTO_ULTIMA_COMPRA'
      Origin = 'COSTO_ULTIMA_COMPRA'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '#.00'
      Precision = 18
      Size = 6
    end
  end
  object DSReporte: TfrxDBDataset
    UserName = 'DSReporte'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ARTICULO=ARTICULO'
      'UNIDADES=UNIDADES'
      'ARTICULO_ID=ARTICULO_ID'
      'FOLIO=FOLIO'
      'COSTO_ULTIMA_COMPRA=COSTO_ULTIMA_COMPRA')
    DataSet = qryImprimir
    BCDToCurrency = False
    Left = 832
    Top = 200
  end
  object Reporte: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42949.704375405100000000
    ReportOptions.LastChange = 43754.447120937500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 712
    Top = 224
    Datasets = <
      item
        DataSet = DSReporte
        DataSetName = 'DSReporte'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftTop]
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object txtTitulo: TfrxMemoView
          Align = baWidth
          Width = 740.409927000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Material de Empaque')
          ParentFont = False
          Style = 'Title'
          VAlign = vaCenter
        end
        object DSReporteFOLIO: TfrxMemoView
          Align = baWidth
          Top = 22.677180000000000000
          Width = 740.409927000000000000
          Height = 18.897650000000000000
          DataField = 'FOLIO'
          DataSet = DSReporte
          DataSetName = 'DSReporte'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[DSReporte."FOLIO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 40.480210000000000000
        Top = 83.149660000000000000
        Width = 740.409927000000000000
        object Shape1: TfrxShapeView
          Left = 1.000000000000000000
          Top = 6.582560000000000000
          Width = 739.488250000000000000
          Height = 33.897650000000000000
        end
        object Memo8: TfrxMemoView
          Left = 20.350487960000000000
          Top = 9.559060000000000000
          Width = 146.903792200000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'ARTICULO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 192.151930160000000000
          Top = 9.559060000000000000
          Width = 69.821341300000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'UNIDADES')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 189.826840000000000000
          Top = 7.582560000000000000
          Height = 32.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line2: TfrxLineView
          Left = 267.000000000000000000
          Top = 7.582560000000000000
          Height = 32.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Memo1: TfrxMemoView
          Left = 272.126160000000000000
          Top = 9.535560000000000000
          Width = 69.821341300000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'COSTO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 346.974229840000000000
          Top = 7.559060000000000000
          Height = 32.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 185.196970000000000000
        Width = 740.409927000000000000
        DataSet = DSReporte
        DataSetName = 'DSReporte'
        RowCount = 0
        object DSReporteUNIDADES: TfrxMemoView
          Left = 188.976500000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'UNIDADES'
          DataSet = DSReporte
          DataSetName = 'DSReporte'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[DSReporte."UNIDADES"]')
          ParentFont = False
        end
        object DSReporteNOMBRE: TfrxMemoView
          Left = 3.779530000000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          DataSet = DSReporte
          DataSetName = 'DSReporte'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DSReporte."ARTICULO"]')
          ParentFont = False
        end
        object DSReporteCOSTO_ULTIMA_COMPRA: TfrxMemoView
          Left = 268.346630000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'COSTO_ULTIMA_COMPRA'
          DataSet = DSReporte
          DataSetName = 'DSReporte'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[DSReporte."COSTO_ULTIMA_COMPRA"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 585.827150000000000000
        Width = 740.409927000000000000
        object Memo31: TfrxMemoView
          Align = baWidth
          Width = 740.409927000000000000
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo32: TfrxMemoView
          Top = 1.000000000000000000
          Height = 22.677180000000000000
          AutoWidth = True
          Memo.UTF8W = (
            '[Date] [Time]')
        end
        object Memo33: TfrxMemoView
          Align = baRight
          Left = 664.819327000000000000
          Top = 1.000000000000000000
          Width = 75.590600000000000000
          Height = 22.677180000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Page [Page#]')
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 298.677180000000000000
        Top = 264.567100000000000000
        Width = 740.409927000000000000
        object Line10: TfrxLineView
          Left = 1.000000000000000000
          Top = 0.708410000000000000
          Width = 737.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 4.000000000000000000
        end
      end
    end
  end
  object qryImprimir: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select (select folio from sic_maquila where maquila_id=:mid),nom' +
        'bre as articulo,'
      'sum(unidades) as unidades,'
      '        articulo_id,costo_ultima_compra'
      'from (select  sat.unidades as cajas,'
      
        '        (select costo_ultima_compra from get_ultcom_art(com.arti' +
        'culo_id)),com.nombre,'
      '        (sat.unidades*jd.unidades) as unidades,'
      '       com.articulo_id'
      'from sic_maquila_articulos_tarima sat join'
      
        'sic_maquila_det_salida ds on ds.maquila_det_id = sat.maquila_det' +
        '_id join'
      'articulos a on a.articulo_id = sat.articulo_id  join'
      'juegos_det jd on jd.articulo_id = a.articulo_id join'
      'articulos com on com.articulo_id=jd.componente_id'
      'where ds.maquila_id = :mid )'
      'group by nombre,articulo_id,costo_ultima_compra'
      'order by nombre')
    Left = 888
    Top = 184
    ParamData = <
      item
        Name = 'MID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryImprimirARTICULO: TStringField
      FieldName = 'ARTICULO'
      Origin = 'ARTICULO'
      Required = True
      Size = 200
    end
    object qryImprimirUNIDADES: TFMTBCDField
      FieldName = 'UNIDADES'
      Origin = 'UNIDADES'
      Precision = 18
      Size = 7
    end
    object qryImprimirARTICULO_ID: TIntegerField
      FieldName = 'ARTICULO_ID'
      Origin = 'ARTICULO_ID'
      Required = True
    end
    object qryImprimirFOLIO: TStringField
      FieldName = 'FOLIO'
      Origin = 'FOLIO'
      Size = 6
    end
    object qryImprimirCOSTO_ULTIMA_COMPRA: TFMTBCDField
      FieldName = 'COSTO_ULTIMA_COMPRA'
      Origin = 'COSTO_ULTIMA_COMPRA'
      Precision = 18
      Size = 6
    end
  end
  object DSMaterial: TDataSource
    DataSet = qryMaterial
    Left = 160
    Top = 248
  end
  object qryLlenadoEntrada: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        ' select sum(peso) as KG,(select nombre from articulos where arti' +
        'culo_id=cc.articulo_id),articulo_id'
      
        ' from claves_cajones cc where cc.maquila_id=:maquila_id and tipo' +
        '='#39'E'#39
      ' group by articulo_id,peso')
    Left = 72
    Top = 304
    ParamData = <
      item
        Name = 'MAQUILA_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryLlenadoEntradaKG: TBCDField
      FieldName = 'KG'
      Origin = 'KG'
      Precision = 18
      Size = 2
    end
    object qryLlenadoEntradaNOMBRE: TStringField
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      Size = 200
    end
    object qryLlenadoEntradaARTICULO_ID: TIntegerField
      FieldName = 'ARTICULO_ID'
      Origin = 'ARTICULO_ID'
      Required = True
      Visible = False
    end
  end
end
