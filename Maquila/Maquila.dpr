program Maquila;

uses
  Vcl.Forms,
  UControlCajonesEmpacadorasAdmin in 'UControlCajonesEmpacadorasAdmin.pas' {ControlCajonesEmpacadorasAdmin},
  UControlCajonesEmpacadorasLista in 'UControlCajonesEmpacadorasLista.pas' {ControlCajonesEmpacadorasLista},
  UControlCajonesEntradaSalida in 'UControlCajonesEntradaSalida.pas' {ControlCajonesEntradaSalida},
  UControlCajonesLista in 'UControlCajonesLista.pas' {ControlCajonesLista},
  UControlCajonesMaquila in 'UControlCajonesMaquila.pas' {ControlCajonesMaquila},
  UControlCajonesMaquilaDetalleEntrada in 'UControlCajonesMaquilaDetalleEntrada.pas' {ControlCajonesMaquilaDetalleEntrada},
  UControlCajonesMaquilaDetalleSalida in 'UControlCajonesMaquilaDetalleSalida.pas' {ControlCajonesMaquilaDetalleSalida},
  UControlCajonesMaquilaEtiquetas in 'UControlCajonesMaquilaEtiquetas.pas' {ControlCajonesMaquilaEtiquetas},
  UControlCajonesMaquilaRegistro in 'UControlCajonesMaquilaRegistro.pas' {ControlCajonesMaquilaRegistro},
  UControlCajonesPrincipal in 'UControlCajonesPrincipal.pas' {ControlCajonesPrincipal},
  UControlCajonesProductores in 'UControlCajonesProductores.pas' {ControlCajonesProductores},
  ULogin in 'ULogin.pas' {Login},
  Uselempresa in 'Uselempresa.pas' {SeleccionaEmpresa},
  UBusqueda in 'UBusqueda.pas' {BusquedaArticulos},
  UConexiones in 'UConexiones.pas' {Conexiones},
  ULicencia in 'ULicencia.pas',
  WbemScripting_TLB in 'WbemScripting_TLB.pas',
  UEnvVars in 'UEnvVars.pas',
  UControlCajonesMaquilaTarima in 'UControlCajonesMaquilaTarima.pas' {Tarima},
  UControlCajonesConfig in 'UControlCajonesConfig.pas' {ControlCajonesConfig},
  UControlCajonesMaquilaMaterial in 'UControlCajonesMaquilaMaterial.pas' {ControlCajonesMaquilaMaterial},
  UControlCajonesMaquilaEmbarque in 'UControlCajonesMaquilaEmbarque.pas' {ControlCajonesMaquilaEmbarque},
  UControlCajonesMaquilaEmbarqueSalida in 'UControlCajonesMaquilaEmbarqueSalida.pas' {ControlCajonesMaquilaEmbarqueSalida},
  UControlCajonesMaquilaEmbarqueTarima in 'UControlCajonesMaquilaEmbarqueTarima.pas' {ControlCajonesMaquilaEmbarqueTarima},
  UControlCajonesMaquilaChofer in 'UControlCajonesMaquilaChofer.pas' {ControlCajonesMaquilaChofer},
  UControlCajonesChofer in 'UControlCajonesChofer.pas' {ControlCajonesChofer},
  UControlCajonesGastosExtras in 'UControlCajonesGastosExtras.pas' {ControlCajonesGastosExtra},
  UBusquedaGastos in 'UBusquedaGastos.pas' {BusquedaGastos},
  UGastosExtra in 'UGastosExtra.pas' {GastosExtra},
  UControlCajonesEmbarque in 'UControlCajonesEmbarque.pas' {FormEmbarque},
  UControlEmbarque in 'UControlEmbarque.pas' {FormEmbarqueMaquilas},
  UControlEmbarqueArticulos in 'UControlEmbarqueArticulos.pas' {FormEmbarqueArticulos},
  UControlCajonesReportes in 'UControlCajonesReportes.pas' {ControlCajonesReportes};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TLogin, Login);
  Application.CreateForm(TControlCajonesGastosExtra, ControlCajonesGastosExtra);
  Application.CreateForm(TBusquedaGastos, BusquedaGastos);
  Application.CreateForm(TGastosExtra, GastosExtra);
  Application.CreateForm(TFormEmbarque, FormEmbarque);
  Application.CreateForm(TFormEmbarqueMaquilas, FormEmbarqueMaquilas);
  Application.CreateForm(TFormEmbarqueArticulos, FormEmbarqueArticulos);
  Application.CreateForm(TControlCajonesReportes, ControlCajonesReportes);
  Application.Run;
end.
