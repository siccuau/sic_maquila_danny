unit UControlCajonesMaquilaEmbarqueTarima;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Vcl.Grids, Vcl.ComCtrls,
  Data.DB, FireDAC.Comp.Client;

type
  TControlCajonesMaquilaEmbarqueTarima = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    gridTarima: TStringGrid;
    btnGuardar: TButton;
    procedure btnGuardarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gridTarimaKeyPress(Sender: TObject; var Key: Char);
    procedure gridTarimaSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ControlCajonesMaquilaEmbarqueTarima: TControlCajonesMaquilaEmbarqueTarima;

implementation

{$R *.dfm}

procedure TControlCajonesMaquilaEmbarqueTarima.btnGuardarClick(Sender: TObject);
begin
self.Close;
end;

procedure TControlCajonesMaquilaEmbarqueTarima.FormShow(Sender: TObject);
begin
              //TITULOS DEL STRINGGRID
             gridTarima.Cells[0,0]:='Articulo';
             gridTarima.Cells[1,0]:='Cajas Total';
             gridTarima.Cells[2,0]:='Cajas Tarima';
             gridTarima.ColWidths[0]:=250;
             gridTarima.ColWidths[1]:=70;
              gridTarima.ColWidths[2]:=70;
              gridTarima.ColWidths[3]:=0;
              gridTarima.ColWidths[4]:=0;
end;

procedure TControlCajonesMaquilaEmbarqueTarima.gridTarimaKeyPress(
  Sender: TObject; var Key: Char);
var
  i: Integer;
begin
      if (Key=#13) then begin
        for i := 1 to gridTarima.RowCount-1 do
            begin
                 if gridTarima.Cells[2,i]>gridTarima.Cells[1,i] then
                     begin
                       ShowMessage('Error cajas de tarima no puede ser mayor a las cajas totales.');
                       gridTarima.Cells[2,i]:=inttostr(0);
                     end;
            end;
      end;
end;

procedure TControlCajonesMaquilaEmbarqueTarima.gridTarimaSelectCell(
  Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
 if (aCol = 0) or (aCol = 1) then begin
         CanSelect := FALSE;
         Exit;
 end;
end;

end.
